-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Mar 16, 2019 at 06:30 AM
-- Server version: 5.5.62-cll
-- PHP Version: 7.2.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ambikagr_etsdb`
--

-- --------------------------------------------------------

--
-- Table structure for table `album`
--

CREATE TABLE `album` (
  `a_id` int(15) NOT NULL,
  `album_type_id` varchar(20) NOT NULL,
  `a_title` varchar(100) NOT NULL DEFAULT '',
  `a_year` year(4) DEFAULT '0000',
  `a_description` text,
  `position` char(1) NOT NULL DEFAULT 'I',
  `username` varchar(50) NOT NULL,
  `createdate` date NOT NULL,
  `modifieddate` date NOT NULL,
  `status` char(1) NOT NULL DEFAULT 'E',
  `sortorder` int(3) NOT NULL DEFAULT '1',
  `remote_ip` varchar(50) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `album`
--

INSERT INTO `album` (`a_id`, `album_type_id`, `a_title`, `a_year`, `a_description`, `position`, `username`, `createdate`, `modifieddate`, `status`, `sortorder`, `remote_ip`) VALUES
(8, '8', 'Ambika Group Insight', 0000, 'Description', 'I', 'Keyur', '2015-10-27', '2016-01-01', 'E', 1, '127.0.0.1'),
(9, '8', 'Festival Gallery', 0000, 'description', 'I', 'Keyur', '2015-10-28', '2016-10-01', 'E', 2, '103.69.200.47'),
(10, '8', 'Site Updates', 0000, 'Site Updates', 'I', 'Keyur', '2016-04-12', '2016-01-01', 'E', 3, '127.0.0.1'),
(11, '8', 'Press Ad & Hoarding ', 0000, 'Press Ad & Hoarding ', 'I', 'Keyur', '2016-08-09', '2016-08-09', 'E', 4, '::1');

-- --------------------------------------------------------

--
-- Table structure for table `album_type`
--

CREATE TABLE `album_type` (
  `type_id` int(11) NOT NULL,
  `album_title` varchar(50) NOT NULL,
  `username` varchar(50) NOT NULL,
  `createdate` date NOT NULL,
  `modifieddate` date NOT NULL,
  `sortorder` varchar(20) NOT NULL,
  `status` char(1) NOT NULL DEFAULT 'E',
  `remote_ip` varchar(50) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `album_type`
--

INSERT INTO `album_type` (`type_id`, `album_title`, `username`, `createdate`, `modifieddate`, `sortorder`, `status`, `remote_ip`) VALUES
(8, 'Album Type 1', 'Keyur', '2015-10-27', '0000-00-00', '1', 'E', '192.168.2.106');

-- --------------------------------------------------------

--
-- Table structure for table `contact_master`
--

CREATE TABLE `contact_master` (
  `cid` int(11) NOT NULL,
  `cname` varchar(100) NOT NULL,
  `cemail` varchar(100) NOT NULL,
  `csubject` varchar(100) NOT NULL,
  `cmessage` varchar(256) NOT NULL,
  `ccontact` varchar(30) DEFAULT '0',
  `cdate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `new` int(1) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `contact_master`
--

INSERT INTO `contact_master` (`cid`, `cname`, `cemail`, `csubject`, `cmessage`, `ccontact`, `cdate`, `new`) VALUES
(28, 'Zixz', 'abc@xyz.com', 'xxz', 'xzzxxz', 'xzxz', '2016-08-31 22:00:00', 1),
(29, 'ssasa', 'aesha9794@gmail.com', 'dssa', 'sasasas', 'saas', '2016-08-31 22:00:00', 1),
(30, 'da', 'aesha@gmail.com', 'dsds', 'dsdssdds', 'dsds', '2016-08-31 22:00:00', 1),
(27, 'dssd', 'ds@dhjshj.com', 'ehggsgd', 'sddsfdffdfd', 'hjfdhjdhj', '2016-08-31 22:00:00', 1),
(31, 'kinjal', 'kinjal.poathak@gmail.com', 'tsst', 'jgjbdcjsdcvsvs', '6756576', '2016-08-31 22:00:00', 1),
(32, 'dssddssd', 'aesha9794@gmail.com', 'ssasa', 'sasasasa', 'ssa', '2016-08-31 22:00:00', 1),
(33, 'aesha', 'aesha9794@gmail.com', 'test', 'test', 'test', '2016-08-31 22:00:00', 1),
(34, 'Saurabh', 'sau.agg10@gmail.com', 'Ambika Pinnacle Residencies', 'I am interested in your project for investment.\r\nPlease share Price list and payment plan , brochure, possession and other project details.\r\n', '9599934899', '2016-10-24 22:00:00', 1),
(35, 'Julian Cooper', 'juliancooper.mkt@gmail.com', 'Want more clients and customers?', 'Do you wish you could increase your online leads? We have helped a lot of businesses thrive in this market and we can help you! Simply hit reply and Iâ€™ll share with you the cost and the benefits.', '844-287-6371', '2017-01-18 23:00:00', 1),
(36, 'nitesh', 'rakesh724500@gmail.com', 'flat rate', 'Give me flat price detail', '7021213039', '2017-04-11 22:00:00', 1),
(37, 'apnarera', 'go@apnarera.com', 'enquiry', 'Kind Attention, \r\nManagement and staff members.\r\nWe are really excited to introduce apnarera.com to you. \r\nwww.apnarera.com is an initiative to help realtors know about the Indian Real Estate (Regulation and development) Act, 2016 also known as RERA. The A', '7676763319', '2017-05-18 22:00:00', 1),
(38, 'BASANT SOMANY', 'somanybasant@gmail.com', 'PROJECT FINANCE', 'We are 25 years old company having matured experience in PROJECT FINANCE. We provide best services with hassle free approval from all the leading banks. Request you to spare your valuable time, so that i can give you detailed report about our services.Than', '7003779607 / 9804222382', '2017-07-11 22:00:00', 1),
(39, 'FastFundingAdvisors', '', '', 'Hi, letting you know that http://FastFundingAdvisors.com can find your business a SBA or private loan for $2,000 - $350K Without high credit or collateral. \r\n \r\nFind Out how much you qualify for here: \r\n \r\nhttp://FastFundingAdvisors.com/i.php?url=ambikagro', '', '2017-07-20 22:00:00', 1),
(40, 'GetaBusinessLoan365', '', '', 'Hi, letting you know that http://GetaBusinessLoan365.com can find your business a SBA or private loan for $2,000 - $350K Without high credit or collateral. \r\n \r\nFind Out how much you qualify for here: \r\n \r\nhttp://GetaBusinessLoan365.com/i.php?url=ambikagro', '', '2017-08-23 22:00:00', 1),
(41, 'Gopal', 'gopalraj7777730@gmail.com', 'Job Regarding ', 'I am a experience person in this field so please contact me sir ', '7405353334', '2017-09-05 22:00:00', 1),
(42, 'GetaBusinessFunded', '', '', 'Hi, letting you know that http://GetaBusinessFunded.com can find your business a SBA or private loan for $2,000 - $350K Without high credit or collateral. \r\n \r\nFind Out how much you qualify for here: \r\n \r\nhttp://GetaBusinessFunded.com/i.php?url=ambikagroup', '', '2017-09-18 22:00:00', 1),
(43, 'GetaBusinessLoan365', '', '', 'Hi, letting you know that http://GetaBusinessLoan365.com can find your business a SBA or private loan for $2,000 - $350K Without high credit or collateral. \r\n \r\nFind Out how much you qualify for here: \r\n \r\nhttp://GetaBusinessLoan365.com/i.php?url=ambikagro', '', '2017-10-06 22:00:00', 1),
(44, 'GetaBusinessFunded', '', '', 'Hi, letting you know that http://GetaBusinessFunded.com can find your business a SBA or private loan for $2,000 - $350K Without high credit or collateral. \r\n \r\nFind Out how much you qualify for here: \r\n \r\nhttp://GetaBusinessFunded.com/i.php?url=ambikagroup', '', '2017-10-18 22:00:00', 1),
(45, 'Pankaj', 'info@airwingsacademy.com', 'Buy', 'Want to buy 3bhk', '9501450467', '2017-10-20 22:00:00', 1),
(46, 'Pankaj', 'info@airwingsacademy.com', 'Buy', 'Buy 3 bhk', '9501450467', '2017-10-21 22:00:00', 1),
(47, 'Alex', 'newslentarus@mail.ru', 'Russia created a crypto currency to replace bitcoin - SibCoin (Siberian chervonets)', 'Russia created a crypto currency to replace bitcoin - SibCoin (Siberian chervonets)', '86711965845', '2017-10-24 22:00:00', 1),
(48, 'Keval Patel ', 'kevalpatel@hotmail.ca', 'Interesting in ambika solitaire scheme', 'Kindly know the area of flat as well as rate per sq.ft. And timing of possession....!!!!', '9638373700', '2017-11-06 23:00:00', 1),
(49, 'NataliRus', 'natali@mail.ru', 'Russia they released their crypto currency to replace bitcoin', 'In Russia released the national crypto currency SibCoin (Siberian chervonets) \r\nSibCoin can repeat the success of Bitcoin. \r\n \r\nAs a thanks that I informed you please send a money of this crypto currency \r\nMy SibCoin wallet: SRVJfVgYieAEBWJAsX2okBxrM3knYaY', '82111573672', '2017-11-08 23:00:00', 1),
(50, 'FindFastBusinessFunds', '', '', 'Hi, letting you know that http://FindFastBusinessFunds.com can find your business a SBA or private loan for $2,000 - $350K Without high credit or collateral. \r\n \r\nFind Out how much you qualify for here: \r\n \r\nhttp://FindFastBusinessFunds.com/i.php?url=ambik', '', '2017-11-09 23:00:00', 1),
(51, 'BusinessLoansFunded', '', '', 'Hi, letting you know that http://BusinessLoansFunded.com can find your business a SBA or private loan for $2,000 - $350K Without high credit or collateral. \r\n \r\nFind Out how much you qualify for here: \r\n \r\nhttp://BusinessLoansFunded.com/i.php?url=ambikagro', '', '2017-11-22 23:00:00', 1),
(52, 'PanMen', 'panamened@mail.ru', 'Crypto currency SibCoin was included in the list of Forbes.', 'Crypto currency SibCoin (Siberian chervonets) was included in the list of Forbes. SibCoin can replace Bitcoin on Russian territory', '81141444565', '2017-11-25 23:00:00', 1),
(53, 'GetaBusinessFunded365', '', '', 'Hi, letting you know that http://GetaBusinessFunded365.com can find your business a SBA or private loan for $2,000 - $350K Without high credit or collateral. \r\n \r\nFind Out how much you qualify for here: \r\n \r\nhttp://GetaBusinessFunded365.com/i.php?url=ambik', '', '2017-11-30 23:00:00', 1),
(54, 'FastFundingAdvisors', '', '', 'Hi, letting you know that http://FastFundingAdvisors.com can find your business a SBA or private loan for $2,000 - $350K Without high credit or collateral. \r\n \r\nFind Out how much you qualify for here: \r\n \r\nhttp://FastFundingAdvisors.com/i.php?url=ambikagro', '', '2018-01-04 23:00:00', 1),
(55, 'Ivan', 'ivan@mlcrosoft.com', 'Microsoft will support the SibCoin crypto currency', 'Microsoft will support the SibCoin (Siberian chervonets) crypto currency. \r\nMore information will be announced shortly.', '88615921612', '2018-01-08 23:00:00', 1),
(56, 'ProFunder247', '', '', 'Hi, letting you know that http://ProFunder-247.com can find your business a SBA or private loan for $2,000 - $350K Without high credit or collateral. \r\n \r\nFind Out how much you qualify for here: \r\n \r\nhttp://ProFunder-247.com/i.php?url=ambikagroupsurat.com&', '', '2018-01-18 23:00:00', 1),
(57, 'GetaBusinessFunded', '', '', 'Hi, letting you know that http://GetaBusinessFunded.com can find your business a SBA or private loan for $2,000 - $350K Without high credit or collateral. \r\n \r\nFind Out how much you qualify for here: \r\n \r\nhttp://GetaBusinessFunded.com/i.php?url=ambikagroup', '', '2018-01-30 23:00:00', 1),
(58, 'Dhananjay', 'Moredhananjay18@gmail.com', 'I need 2 bhk flate in ambika group construction adajan', 'I need 2 bhk flate in ambika group construction in adajan area is this available???', '7567797682', '2018-02-06 23:00:00', 1),
(59, 'ProFunder365', '', '', 'Hi, letting you know that http://ProFunder365.com/i.php?url=ambikagroupsurat.com&id=e95 can find your business a SBA or private loan for $2,000 - $350K Without high credit or collateral. \r\n \r\nFind Out how much you qualify for here: \r\n \r\nhttp://ProFunder365', '', '2018-02-20 23:00:00', 1),
(60, 'PADAM DEV LAW', 'lawpadam@yahoo.com', '3BHK or 4 BHK Apartment', 'Dear Sir,........ I am presently residing at Noida in NCR and wish to purchase a spacious 3 BHK or  a 4 BHK apartment in Surat for self occupation. I would be obliged if you could send me details of various completed projects or projects expected top eb co', '+919717036837', '2018-03-02 23:00:00', 1),
(61, 'ProFunding365', 'noreply@profunding365.com', 'Additional Capital to Grow Your Business', 'Hi, letting you know that http://ProFunding365.com can find your business a SBA or private loan for $2,000 - $350K Without high credit or collateral. \r\n \r\nFind Out how much you qualify for by clicking here: \r\n \r\nhttp://ProFunding365.com \r\n \r\nMinimum requir', '', '2018-03-21 23:00:00', 1),
(62, 'Jeramybic', 'tlcg01@yahoo.com', '$10,000 in just fivÐµ dÐ°Ñƒs', 'EÑ…tremely gÐ¾od news. \r\n \r\nI downloadÐµd this yÐµstÐµrday and it madÐµ mÐµ $2,421.28 \r\n \r\nThÐµse guys guarÐ°nteÐµ thÐ°t it will makÐµ yÐ¾u $2,000 within 24 hÐ¾urs. \r\n \r\nDo nÐ¾t miss Ð¾ut Ð¾n this. \r\n \r\nPleÐ°sÐµ follÐ¾w thÐµ link below to claim yÐ¾ur cÐ°sh', '82382331479', '2018-04-16 22:00:00', 1),
(63, 'Kennethvah', 'rv291275@mail.ru', '100$ Ð² Ð´ÐµÐ½ÑŒ Ð¸ Ð±oÐ»ÑŒÑˆe - Ð·aÑ€aÐ±oÑ‚oÐº Ð² Ð¸Ð½Ñ‚ÐµÑ€Ð½eÑ‚Ðµ', '10 000 Ð´oÐ»Ð»aÑ€Ð¾Ð² Ð²ÑÐµÐ³o Ð·a Ð¿ÑÑ‚ÑŒ Ð´Ð½eÐ¹: http://forum.bestusamakingmoneyways.org/?p=69742', '88694438975', '2018-04-21 22:00:00', 1),
(64, 'ArthurExcit', 'michael.herrod@itn.co.uk', '1000000+ unique high-ranking visitors from the US, AU, CA, UK per month to your site', 'Hello. \r\nIf you need to have so much quality traffic without only a few body movements, you can find the funniness here: http://make.ultimate-link-building.info/', '88647987555', '2018-04-24 22:00:00', 1);

-- --------------------------------------------------------

--
-- Table structure for table `download_brochure`
--

CREATE TABLE `download_brochure` (
  `b_id` int(11) NOT NULL,
  `bname` varchar(100) NOT NULL,
  `bemail` varchar(100) NOT NULL,
  `bphone` varchar(20) NOT NULL,
  `bdate` date NOT NULL,
  `bproject` varchar(100) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `download_brochure`
--

INSERT INTO `download_brochure` (`b_id`, `bname`, `bemail`, `bphone`, `bdate`, `bproject`) VALUES
(278, 'gjgj', 'fyjyf', 'ghjkgh', '2016-04-06', 'Galaxy Home'),
(279, 'Name 1', 'dafsdf@gmail.com', '34234234', '2016-04-11', 'Vrundavan Villa'),
(280, 'aesha', 'a@gmail.com', '12233', '2016-07-30', 'Ambika Dreams'),
(281, 'sdaas', 'a@gmail.com', '32323', '2016-07-30', 'Ambika Dreams'),
(282, 'aaA', 'a@gmail.com', '2133', '2016-07-30', 'Ambika Dreams'),
(283, 'asasas', 'a@gmail.com', '122332', '2016-07-30', 'Ambika Dreams'),
(284, 'aSAS', 'a@gmail.com', '211232', '2016-07-30', 'Ambika Dreams'),
(285, 'aes', 'asa@hjddg.com', 'asaasas', '2016-08-02', 'Ambika Solitaire'),
(286, 'Zil Mehtaa', 'zilmehta@gmail.com', '9904990399', '2016-08-09', 'Ambika Dreams'),
(287, 'Zil Mehtaa', 'zilmehta@gmail.com', '9904990399', '2016-08-09', 'Ambika Dreams'),
(288, 'Zil Mehtaa', 'zilmehta@gmail.com', '9904990399', '2016-08-09', 'Ambika Dreams'),
(289, 'Zil Mehtaa', 'zilmehta@gmail.com', '9904990399', '2016-08-09', 'Ambika Dreams'),
(290, 'Zil Mehtaa', 'zilmehta@gmail.com', '9904990399', '2016-08-09', 'Ambika Dreams'),
(291, 'Zil Mehtaa', 'zilmehta@gmail.com', '9904990399', '2016-08-09', 'Ambika Solitaire'),
(292, 'Zil Mehtaa', 'zilmehta@gmail.com', '9904990399', '2016-08-20', 'Ambika Pinnacle Residency'),
(293, 'z', 'z@gmail.com', '9904990399', '2016-08-20', 'Ambika Pinnacle Residency'),
(294, 'chirag', 'work.chiragkevadiya@gmail.com', '9099852919', '2016-09-04', 'Ambika Solitaire'),
(295, 'Mahendra Daga', 'mahendra.daga@redstone-group.in', '9782287640', '2016-09-26', 'Ambika Solitaire'),
(296, 'Mahendra Daga', 'mahendra.daga@redstone-group.in', '9782287640', '2016-09-26', 'Ambika Solitaire'),
(297, '', '', '', '2016-10-13', ''),
(298, '', '', '', '2016-10-13', ''),
(299, 'dsds', 'a@hhd.com', 'dsdds', '2016-10-15', 'Ambika Pinnacle Business Hub'),
(300, 'sandip bavishiya', 'bavishiyasandip@gmail.com', '9939373317', '2016-10-22', 'Ambika Solitaire'),
(301, 'owais shah', 'vishaalkapoor23@gmail.com', '9888939467', '2016-10-25', 'Ambika Solitaire'),
(302, 'owais shah', 'vishaalkapoor23@gmail.com', ' 919888939467', '2016-10-25', 'Ambika Solitaire'),
(303, 'Siddharth Sinha', 'vishaalkapoor23@gmail.com', '09899996677', '2016-10-25', 'Ambika Solitaire'),
(304, 'Siddharth', 'siddharth.eyecon@gmail.com', '9904709505', '2016-11-17', 'Ambika Solitaire'),
(305, '', '', '', '2016-11-21', ''),
(306, '', '', '', '2016-11-21', ''),
(307, 'pradip patel', 'pradippatel404@gmail.com', '99778800404', '2016-12-30', 'Ambika Solitaire'),
(308, 'jadav', 'jadav_kerai@yahoo.co.in', '9099070618', '2017-01-16', 'Ambika Solitaire'),
(309, '', '', '', '2017-02-17', ''),
(310, '', '', '', '2017-02-17', ''),
(311, 'chintan goyani', 'chintangoyani@gmail.com', '7405674727', '2017-04-04', 'Ambika Solitaire'),
(312, 'Mahendra Daga', 'mahendra.daga@redstone-group.in', '8209585319', '2017-04-08', 'Ambika Pinnacle Residency'),
(313, 'mahendra', 'mahendradaga007@gmail.com', '8209585319', '2017-04-08', 'Ambika Pinnacle Residency'),
(314, '', '', '', '2017-04-27', ''),
(315, '', '', '', '2017-04-27', ''),
(316, 'Oob Automation', 'oobautomation@gmail.com', '09714090305', '2017-05-05', 'Ambika Solitaire'),
(317, 'parash', 'parashkherala@gmail.com', '9328615103', '2017-05-06', 'Ambika Solitaire'),
(318, 'sfdst', 'sddstgfd@gmail.comn', '9745298432', '2017-05-15', 'Ambika Solitaire'),
(319, 'Nikesh', 'nick.gajera9@gmail.com', '9909104500', '2017-05-17', 'Ambika Solitaire'),
(320, 'sagar', 'onlymrs.k@gmail.com', '9924846289', '2017-05-26', 'Ambika Solitaire'),
(321, 'Rakesh Haridasan', 'rakesh.haridasan@edelweissfin.com', '9819350758', '2017-05-26', 'Ambika Pinnacle Residency'),
(322, 'sid', 'sidpatel2u@gmail.com', '0123456789', '2017-06-07', 'Ambika Solitaire'),
(323, 'Manish', 'manishbhatia304@yahoo.com', '968705955', '2017-06-25', 'Ambika Solitaire'),
(324, 'sagar', 'panchanisagar422@gmail.com', '9574544981', '2017-07-13', 'Ambika Solitaire'),
(325, 'sagar', 'panchanisagar422@gmail.com', '9574544981', '2017-07-13', 'Ambika Solitaire'),
(326, 'dsfa', 'asdf@asdf.coim', '345456456456', '2017-07-13', 'Ambika Solitaire'),
(327, 'ersd', 'asdf@asdfc.com', '987654654', '2017-07-13', 'Ambika Solitaire'),
(328, 'Bhavini', 'bhavini@ncx.com', '546456456', '2017-07-13', 'Ambika Solitaire'),
(329, 'Dishant', 'Dishant@dsgf.com', '123456', '2017-07-15', 'Ambika Pinnacle Residency'),
(330, 'Dishant', 'bhavini.easternts@gmail.com', '123456', '2017-07-17', 'Ambika Pinnacle Business Hub'),
(331, '', '', '', '2018-02-18', '');

-- --------------------------------------------------------

--
-- Table structure for table `epanel_aes`
--

CREATE TABLE `epanel_aes` (
  `content_id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `modifieddate` date NOT NULL,
  `content` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `epanel_master`
--

CREATE TABLE `epanel_master` (
  `id` int(11) NOT NULL,
  `home_content` text NOT NULL,
  `copyright` varchar(100) NOT NULL,
  `powered_by` varchar(255) NOT NULL,
  `ga_view_id` int(11) NOT NULL,
  `username` varchar(100) NOT NULL,
  `create_date` date NOT NULL,
  `modified_date` date NOT NULL,
  `remote_ip` varchar(70) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `epanel_master`
--

INSERT INTO `epanel_master` (`id`, `home_content`, `copyright`, `powered_by`, `ga_view_id`, `username`, `create_date`, `modified_date`, `remote_ip`) VALUES
(1, '<p><span>Eastern Techno Solution (ETS) is headquartered in Surat, Gujarat. An IT Company established by ex- employees of COGNIZANT and INFOSYS.</span><br /><br /><span>It principally focused on</span><span>&nbsp;Customized Software Development, Web Applications</span><span>&nbsp;(which includes Web Designing and Ecommerce Websites),&nbsp;</span><span>Mobile Applications and Training Institute.</span><span>&nbsp;Within 3 years from its inception, ETS has been able to make a mark in Gujarat.</span><br /><br /><span>We are a company with leading specialized professionals whose striking experiences reveal a comprehensible indulgent that industries today are faced with the confrontation of harnessing the eternally changing landscape of a complicated and spirited business world.</span><br /><br /><span>It\'s essential for businesses today to stay on the callous edge of technology to build and implement the tools necessary to compete and succeed taking care of innovation. ETS has showed that with apparent understanding of your business attached to our analytical proficiency; a policy can be forged heartening your company to a whole new plateau of triumph.</span><br /><br /><span>Our all-embracing case of services includes Customized software&rsquo;s</span><span>, Web</span><span>&nbsp;application development and Mobile application development (Android development and iPhone development). ETS sets it a challenge to make finest use of the resources, and the precious business intelligence entrenched in a range of verticals - Insurance, Finance, Hotels &amp; Travel, Healthcare, Retail, Distribution, Government, and Manufacturing etc.</span><br /><span><br /><strong>We as Training Institute</strong></span><br /><span>ETS also offers a wide range of training programs for school as well as college students and IT professionals. It provides high class principles and top priority to deliver best learning know-how by following best business practices projected to help the students to expand valuable newest knowledge and experience in grounding for an innovative, pleasing career in an ever-changing marketplace.</span></p>\r\n<p><span><br /><strong>We as Consultancy Firm</strong><br /><span>ETS is also a consultancy firm that helps college students and IT professionals to be placed in the best IT companies. We have tie- ups with many companies. So the students/ IT Professionals, after attending the corporate training program, maximize their chances of getting placed in the best IT companies. Thus, we are helping the industry by providing IT technocrats.</span></span></p>', 'Eastern Techno Solutions', 'a:2:{s:5:\"title\";s:24:\"Eastern Techno Solutions\";s:4:\"link\";s:25:\"http://www.easternts.com/\";}', 128779048, 'Keyur', '2015-10-26', '2016-09-01', '123.201.2.173');

-- --------------------------------------------------------

--
-- Table structure for table `gallery`
--

CREATE TABLE `gallery` (
  `galleryID` int(11) NOT NULL,
  `productID` int(11) NOT NULL DEFAULT '0',
  `galleryTitle` varchar(100) NOT NULL DEFAULT '',
  `galleryImage` varchar(100) NOT NULL,
  `isFront` char(1) NOT NULL DEFAULT 'E',
  `sortorder` int(3) DEFAULT '0',
  `status` char(1) NOT NULL DEFAULT 'E',
  `username` varchar(50) NOT NULL,
  `createdate` date DEFAULT '2016-01-01',
  `modifieddate` date DEFAULT '2016-01-01',
  `remote_ip` varchar(15) DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `gallery`
--

INSERT INTO `gallery` (`galleryID`, `productID`, `galleryTitle`, `galleryImage`, `isFront`, `sortorder`, `status`, `username`, `createdate`, `modifieddate`, `remote_ip`) VALUES
(1, 1, 'Galaxy Villa', 'slider1.jpg', 'E', 0, 'E', '', '2016-01-01', '2016-01-01', ''),
(2, 1, 'Galaxy Villa', 'slider2.jpg', 'E', 0, 'E', '', '2016-01-01', '2016-01-01', ''),
(3, 1, 'one bedroom maple', 'one-bedroom-maple.jpg', 'D', 0, 'E', '', '2016-01-01', '2016-01-01', ''),
(4, 1, 'one bedroom maple', 'two-bedroom-maple.jpg', 'D', 0, 'E', '', '2016-01-01', '2016-01-01', ''),
(5, 1, 'one bedroom maple', 'three-bedroom-maple.jpg', 'D', 0, 'E', '', '2016-01-01', '2016-01-01', ''),
(6, 1, 'Galaxy Villa', 'one-thumb-maple1.jpg', 'G', 0, 'E', '', '2016-01-01', '2016-01-01', ''),
(7, 1, 'Galaxy Villa', 'maple-betel-home.jpg', 'G', 0, 'E', '', '2016-01-01', '2016-01-01', ''),
(8, 1, 'Galaxy Villa', 'three-thumb-maple1.jpg', 'G', 0, 'E', '', '2016-01-01', '2016-01-01', ''),
(9, 1, 'Galaxy Villa', 'two-thumb-maple1.jpg', 'G', 0, 'E', '', '2016-01-01', '2016-01-01', ''),
(10, 2, '', 'slider1.jpg', 'E', 0, 'E', '', '2016-01-01', '2016-01-01', ''),
(11, 2, '', 'slider2.jpg', 'E', 0, 'E', '', '2016-01-01', '2016-01-01', ''),
(12, 2, 'Ground Floor Plan', 'three-bedroom-maple.jpg', 'D', 0, 'E', '', '2016-01-01', '2016-01-01', ''),
(13, 2, 'Layout Plan', 'two-bedroom-maple.jpg', 'D', 0, 'E', '', '2016-01-01', '2016-01-01', ''),
(14, 2, 'Layout Plan', 'layout-plan-betel.jpg', 'D', 0, 'E', '', '2016-01-01', '2016-01-01', ''),
(15, 2, 'First Floor Plan', 'one-bedroom-maple.jpg', 'D', 0, 'E', '', '2016-01-01', '2016-01-01', ''),
(16, 2, 'Galaxy Villa', 'gallery1.jpg', 'G', 0, 'E', '', '2016-01-01', '2016-01-01', ''),
(17, 2, 'Galaxy Villa', 'gallery2.jpg', 'G', 0, 'E', '', '2016-01-01', '2016-01-01', ''),
(18, 3, '', 'slider1.jpg', 'E', 0, 'E', '', '2016-01-01', '2016-01-01', ''),
(19, 3, '', 'slider2.jpg', 'E', 0, 'E', '', '2016-01-01', '2016-01-01', ''),
(20, 3, '', 'slider3.jpg', 'E', 0, 'E', '', '2016-01-01', '2016-01-01', ''),
(21, 3, 'one bedroom maple', 'layout-plan-blossom (1).jpg', 'D', 0, 'E', '', '2016-01-01', '2016-01-01', ''),
(22, 3, 'one bedroom maple', 'one-bedroom-maple.jpg', 'D', 0, 'E', '', '2016-01-01', '2016-01-01', ''),
(23, 3, 'First Floor Plan', 'two-bedroom-maple.jpg', 'D', 0, 'E', '', '2016-01-01', '2016-01-01', ''),
(24, 3, 'Layout Plan', 'three-bedroom-maple.jpg', 'D', 0, 'E', '', '2016-01-01', '2016-01-01', ''),
(25, 3, 'Galaxy Villa', 'gallery1.jpg', 'G', 0, 'E', '', '2016-01-01', '2016-01-01', ''),
(26, 3, 'Galaxy Villa', 'gallery4.jpg', 'G', 0, 'E', '', '2016-01-01', '2016-01-01', ''),
(27, 3, 'Galaxy Villa', 'gallery5.jpg', 'G', 0, 'E', '', '2016-01-01', '2016-01-01', ''),
(28, 3, 'Galaxy Villa', 'gallery3.jpg', 'G', 0, 'E', '', '2016-01-01', '2016-01-01', ''),
(29, 3, 'Galaxy Villa', 'gallery2.jpg', 'G', 0, 'E', '', '2016-01-01', '2016-01-01', ''),
(30, 4, '', 'slider2.jpg', 'E', 2, 'E', 'Keyur', '2016-01-01', '2016-01-01', '127.0.0.1'),
(31, 4, '', 'slider1.jpg', 'E', 1, 'E', 'Keyur', '2016-01-01', '2016-01-01', '127.0.0.1'),
(32, 4, 'Layout Plan', 'layoutplan.jpg', 'D', 1, 'E', 'Keyur', '2016-01-01', '2016-01-01', '127.0.0.1'),
(33, 4, 'Club House Layout', 'club-house-layout-plan.jpg', 'D', 2, 'E', 'Keyur', '2016-01-01', '2016-01-01', '127.0.0.1'),
(34, 4, '', '9.jpg', 'G', 9, 'E', '', '2016-01-01', '2016-01-01', ''),
(35, 4, '', '8.jpg', 'G', 10, 'E', '', '2016-01-01', '2016-01-01', ''),
(36, 4, '', '10.jpg', 'G', 4, 'E', '', '2016-01-01', '2016-01-01', ''),
(37, 4, '', '4.jpg', 'G', 5, 'E', '', '2016-01-01', '2016-01-01', ''),
(38, 4, '', '6.jpg', 'G', 6, 'E', '', '2016-01-01', '2016-01-01', ''),
(39, 4, '', '3.jpg', 'G', 2, 'E', '', '2016-01-01', '2016-01-01', ''),
(40, 4, '', '5.jpg', 'G', 7, 'E', '', '2016-01-01', '2016-01-01', ''),
(41, 4, '', '1.jpg', 'G', 1, 'E', 'Keyur', '2016-01-01', '2016-01-01', '127.0.0.1'),
(42, 4, '', '2.jpg', 'G', 8, 'E', 'Keyur', '2016-01-01', '2016-01-01', '127.0.0.1'),
(43, 4, '', '24.jpg', 'G', 11, 'E', '', '2016-01-01', '2016-01-01', ''),
(44, 4, '', '23.jpg', 'G', 3, 'E', '', '2016-01-01', '2016-01-01', ''),
(45, 4, '', '17.jpg', 'G', 12, 'E', '', '2016-01-01', '2016-01-01', ''),
(46, 4, '', '16.jpg', 'G', 13, 'E', '', '2016-01-01', '2016-01-01', ''),
(47, 4, '', '15.jpg', 'G', 14, 'E', '', '2016-01-01', '2016-01-01', ''),
(48, 4, '', '13.jpg', 'G', 15, 'E', '', '2016-01-01', '2016-01-01', ''),
(49, 4, '', '14.jpg', 'G', 16, 'E', '', '2016-01-01', '2016-01-01', ''),
(50, 5, '', 'slider1.jpg', 'E', 0, 'E', '', '2016-01-01', '2016-01-01', ''),
(51, 5, 'Upper Ground Floor', 'upper-ground-floor.jpg', 'D', 0, 'E', 'Keyur', '2016-01-01', '2016-01-01', '127.0.0.1'),
(52, 5, 'Third Floor Plan', 'thirdfloorplan.jpg', 'D', 0, 'E', 'Keyur', '2016-01-01', '2016-01-01', '127.0.0.1'),
(53, 5, 'Upper Basement Plan', 'upper-basement-plan.jpg', 'D', 0, 'E', 'Keyur', '2016-01-01', '2016-01-01', '127.0.0.1'),
(54, 5, 'Tenth Floor Plan', 'tenth-floorplan.jpg', 'D', 0, 'E', 'Keyur', '2016-01-01', '2016-01-01', '127.0.0.1'),
(55, 5, 'Sixth Floor Plan', 'sixthfloorplan.jpg', 'D', 0, 'E', 'Keyur', '2016-01-01', '2016-01-01', '127.0.0.1'),
(56, 5, 'Seventh Floor Plan', 'seventh-floor-plan.jpg', 'D', 0, 'E', 'Keyur', '2016-01-01', '2016-01-01', '127.0.0.1'),
(57, 5, 'Ninth Floor Plan', 'nineth-floor-plan.jpg', 'D', 0, 'E', 'Keyur', '2016-01-01', '2016-01-01', '127.0.0.1'),
(58, 5, 'Second Floor Plan', 'secondfloorplan.jpg', 'D', 0, 'E', 'Keyur', '2016-01-01', '2016-01-01', '127.0.0.1'),
(59, 5, 'Lower Basement Plan', 'lower-basement-plan.jpg', 'D', 0, 'E', 'Keyur', '2016-01-01', '2016-01-01', '127.0.0.1'),
(60, 5, 'Ground Floor Plan', 'groundfloorplan.jpg', 'D', 0, 'E', 'Keyur', '2016-01-01', '2016-01-01', '127.0.0.1'),
(61, 5, 'Fourth Floor Plan', 'fourthfloorplan.jpg', 'D', 0, 'E', 'Keyur', '2016-01-01', '2016-01-01', '127.0.0.1'),
(62, 5, 'Fifth Floor Plan', 'fivthfloorplan.jpg', 'D', 0, 'E', 'Keyur', '2016-01-01', '2016-01-01', '127.0.0.1'),
(63, 5, 'First Floor Plan', 'firstfloorplan.jpg', 'D', 0, 'E', 'Keyur', '2016-01-01', '2016-01-01', '127.0.0.1'),
(64, 5, 'Eleventh Floor Plan', 'eleventhfloorplan.jpg', 'D', 0, 'E', 'Keyur', '2016-01-01', '2016-01-01', '127.0.0.1'),
(65, 5, 'Eight Floor Plan', 'eighthfloorplan.jpg', 'D', 0, 'E', 'Keyur', '2016-01-01', '2016-01-01', '127.0.0.1'),
(66, 5, '', '6.jpg', 'G', 0, 'E', '', '2016-01-01', '2016-01-01', ''),
(67, 5, '', '5.jpg', 'G', 0, 'E', '', '2016-01-01', '2016-01-01', ''),
(68, 5, '', '3.jpg', 'G', 0, 'E', '', '2016-01-01', '2016-01-01', ''),
(69, 5, '', '4.jpg', 'G', 0, 'E', '', '2016-01-01', '2016-01-01', ''),
(70, 5, '', '2.jpg', 'G', 0, 'E', '', '2016-01-01', '2016-01-01', ''),
(71, 5, '', '1.jpg', 'G', 0, 'E', '', '2016-01-01', '2016-01-01', ''),
(72, 6, '', 'slider2.jpg', 'E', 2, 'E', '', '2016-01-01', '2016-01-01', ''),
(73, 6, '', 'slider1.jpg', 'E', 1, 'E', 'Keyur', '2016-01-01', '2016-01-01', '127.0.0.1'),
(74, 6, '', 'tarrace-garden.jpg', 'G', 0, 'E', '', '2016-01-01', '2016-01-01', ''),
(75, 6, '', 'table-tennis.jpg', 'G', 0, 'E', '', '2016-01-01', '2016-01-01', ''),
(76, 6, '', 'suit-room.jpg', 'G', 0, 'E', '', '2016-01-01', '2016-01-01', ''),
(77, 6, '', 'swimming02.jpg', 'G', 0, 'E', '', '2016-01-01', '2016-01-01', ''),
(78, 6, '', 'hotel-reception.jpg', 'G', 0, 'E', '', '2016-01-01', '2016-01-01', ''),
(79, 6, '', 'swimming01.jpg', 'G', 0, 'E', '', '2016-01-01', '2016-01-01', ''),
(80, 6, '', 'hotel-lobby.jpg', 'G', 0, 'E', '', '2016-01-01', '2016-01-01', ''),
(81, 6, '', 'spa.jpg', 'G', 0, 'E', '', '2016-01-01', '2016-01-01', ''),
(82, 6, '', 'gym-reception.jpg', 'G', 0, 'E', '', '2016-01-01', '2016-01-01', ''),
(83, 6, '', 'golf.jpg', 'G', 0, 'E', '', '2016-01-01', '2016-01-01', ''),
(84, 6, '', 'gym.jpg', 'G', 0, 'E', '', '2016-01-01', '2016-01-01', ''),
(85, 6, '', 'cricket.jpg', 'G', 0, 'E', '', '2016-01-01', '2016-01-01', ''),
(86, 6, '', 'billiards.jpg', 'G', 0, 'E', '', '2016-01-01', '2016-01-01', ''),
(87, 6, '', 'banquet-hall.jpg', 'G', 0, 'E', '', '2016-01-01', '2016-01-01', ''),
(88, 6, '', 'delux-room.jpg', 'G', 0, 'E', '', '2016-01-01', '2016-01-01', ''),
(89, 6, '', 'aerobics.jpg', 'G', 0, 'E', '', '2016-01-01', '2016-01-01', ''),
(90, 7, '', 'slider2.jpg', 'E', 0, 'E', '', '2016-01-01', '2016-01-01', ''),
(91, 7, '', 'slider1.jpg', 'E', 0, 'E', '', '2016-01-01', '2016-01-01', ''),
(92, 7, 'Upper Floor Plan', 'upperfloorplan.jpg', 'D', 0, 'E', 'Keyur', '2016-01-01', '2016-01-01', '127.0.0.1'),
(93, 7, 'Layout Plan', 'layout-plan.jpg', 'D', 0, 'E', 'Keyur', '2016-01-01', '2016-01-01', '127.0.0.1'),
(94, 7, 'Ground Floor Plan', 'groundfloorplan.jpg', 'D', 0, 'E', 'Keyur', '2016-01-01', '2016-01-01', '127.0.0.1'),
(95, 7, 'Mezzanine Floor Plan', 'mazzaninefloorplan.jpg', 'D', 0, 'E', 'Keyur', '2016-01-01', '2016-01-01', '127.0.0.1'),
(96, 7, 'First Floor Plan', 'firstfloorplan.jpg', 'D', 0, 'E', 'Keyur', '2016-01-01', '2016-01-01', '127.0.0.1'),
(97, 7, 'Second Floor Plan', 'secondfloorplan.jpg', 'D', 0, 'E', 'Keyur', '2016-01-01', '2016-01-01', '127.0.0.1'),
(98, 7, '', '18.jpg', 'G', 0, 'E', '', '2016-01-01', '2016-01-01', ''),
(99, 7, '', '16.jpg', 'G', 0, 'E', '', '2016-01-01', '2016-01-01', ''),
(100, 7, '', '17.jpg', 'G', 0, 'E', '', '2016-01-01', '2016-01-01', ''),
(101, 7, '', '15.jpg', 'G', 0, 'E', '', '2016-01-01', '2016-01-01', ''),
(102, 7, '', '14.jpg', 'G', 0, 'E', '', '2016-01-01', '2016-01-01', ''),
(103, 7, '', '10.jpg', 'G', 0, 'E', '', '2016-01-01', '2016-01-01', ''),
(104, 7, '', '13.jpg', 'G', 0, 'E', '', '2016-01-01', '2016-01-01', ''),
(105, 7, '', '9.jpg', 'G', 0, 'E', '', '2016-01-01', '2016-01-01', ''),
(106, 7, '', '11.jpg', 'G', 0, 'E', '', '2016-01-01', '2016-01-01', ''),
(107, 7, '', '8.jpg', 'G', 0, 'E', '', '2016-01-01', '2016-01-01', ''),
(108, 7, '', '6.jpg', 'G', 0, 'E', '', '2016-01-01', '2016-01-01', ''),
(109, 7, '', '7.jpg', 'G', 0, 'E', '', '2016-01-01', '2016-01-01', ''),
(110, 7, '', '5.jpg', 'G', 0, 'E', '', '2016-01-01', '2016-01-01', ''),
(111, 7, '', '12.jpg', 'G', 0, 'E', '', '2016-01-01', '2016-01-01', ''),
(112, 7, '', '2.jpg', 'G', 0, 'E', '', '2016-01-01', '2016-01-01', ''),
(113, 7, '', '3.jpg', 'G', 0, 'E', '', '2016-01-01', '2016-01-01', ''),
(114, 7, '', '1.jpg', 'G', 0, 'E', '', '2016-01-01', '2016-01-01', ''),
(115, 7, '', '4.jpg', 'G', 0, 'E', '', '2016-01-01', '2016-01-01', ''),
(116, 8, '', 'slider2.jpg', 'E', 0, 'E', '', '2016-01-01', '2016-01-01', ''),
(117, 8, '', 'slider1.jpg', 'E', 0, 'E', '', '2016-01-01', '2016-01-01', ''),
(118, 8, 'Upper Ground Floor', 'upperground-floor.jpg', 'D', 0, 'E', 'Keyur', '2016-01-01', '2016-01-01', '127.0.0.1'),
(119, 8, 'Second Floor Plan', 'secondfloorplan.jpg', 'D', 0, 'E', 'Keyur', '2016-01-01', '2016-01-01', '127.0.0.1'),
(120, 8, 'Mezzanine Floor Plan', 'mazzaninefloor-plan.jpg', 'D', 0, 'E', 'Keyur', '2016-01-01', '2016-01-01', '127.0.0.1'),
(121, 8, 'Ground Floor Plan', 'ground-floor.jpg', 'D', 0, 'E', 'Keyur', '2016-01-01', '2016-01-01', '127.0.0.1'),
(122, 8, 'Layout Plan', 'layout-plan.jpg', 'D', 0, 'E', 'Keyur', '2016-01-01', '2016-01-01', '127.0.0.1'),
(123, 8, 'First Floor Plan', 'firstfloorplan.jpg', 'D', 0, 'E', 'Keyur', '2016-01-01', '2016-01-01', '127.0.0.1'),
(124, 8, 'Basement Parking Layout', 'basement-parking.jpg', 'D', 0, 'E', 'Keyur', '2016-01-01', '2016-01-01', '127.0.0.1'),
(125, 8, '', '16.jpg', 'G', 0, 'E', '', '2016-01-01', '2016-01-01', ''),
(126, 8, '', '15.jpg', 'G', 0, 'E', '', '2016-01-01', '2016-01-01', ''),
(127, 8, '', '11.jpg', 'G', 0, 'E', '', '2016-01-01', '2016-01-01', ''),
(128, 8, '', '10.jpg', 'G', 0, 'E', '', '2016-01-01', '2016-01-01', ''),
(129, 8, '', '8.jpg', 'G', 0, 'E', '', '2016-01-01', '2016-01-01', ''),
(130, 8, '', '12.jpg', 'G', 0, 'E', '', '2016-01-01', '2016-01-01', ''),
(131, 8, '', '13.jpg', 'G', 0, 'E', '', '2016-01-01', '2016-01-01', ''),
(132, 8, '', '9.jpg', 'G', 0, 'E', '', '2016-01-01', '2016-01-01', ''),
(133, 8, '', '5.jpg', 'G', 0, 'E', '', '2016-01-01', '2016-01-01', ''),
(134, 8, '', '6.jpg', 'G', 0, 'E', '', '2016-01-01', '2016-01-01', ''),
(135, 8, '', '7.jpg', 'G', 0, 'E', '', '2016-01-01', '2016-01-01', ''),
(136, 8, '', '4.jpg', 'G', 0, 'E', '', '2016-01-01', '2016-01-01', ''),
(137, 8, '', '2.jpg', 'G', 0, 'E', '', '2016-01-01', '2016-01-01', ''),
(138, 8, '', '3.jpg', 'G', 0, 'E', '', '2016-01-01', '2016-01-01', ''),
(139, 8, '', '1.jpg', 'G', 0, 'E', '', '2016-01-01', '2016-01-01', ''),
(140, 9, '', 'slider1.jpg', 'E', 0, 'E', '', '2016-01-01', '2016-01-01', ''),
(141, 9, '', 'slider2.jpg', 'E', 0, 'E', '', '2016-01-01', '2016-01-01', ''),
(142, 9, 'First Floor Plan', 'first-floor-plan.jpg', 'D', 1, 'E', 'Keyur', '2016-01-01', '2016-01-01', '127.0.0.1'),
(143, 9, 'Fourth Floor Plan', 'fourth-floor-plan.jpg', 'D', 2, 'E', 'Keyur', '2016-01-01', '2016-01-01', '127.0.0.1'),
(144, 9, 'Second Basement Plan', 'second-basementplan.jpg', 'D', 3, 'E', 'Keyur', '2016-01-01', '2016-01-01', '127.0.0.1'),
(145, 9, 'First Basement Plan', 'first-basement-plan.jpg', 'D', 4, 'E', 'Keyur', '2016-01-01', '2016-01-01', '127.0.0.1'),
(146, 9, 'Ground Floor Plan', 'gound-floor-plan.jpg', 'D', 5, 'E', 'Keyur', '2016-01-01', '2016-01-01', '127.0.0.1'),
(147, 9, 'Fifth Floor Plan', 'fifth-floor-plan.jpg', 'D', 6, 'E', 'Keyur', '2016-01-01', '2016-01-01', '127.0.0.1'),
(148, 9, 'Third Floor Plan', 'third-floor-plan.jpg', 'D', 8, 'E', 'Keyur', '2016-01-01', '2016-01-01', '127.0.0.1'),
(149, 9, 'Sixth Floor Plan', 'sixth-floor-plan.jpg', 'D', 9, 'E', 'Keyur', '2016-01-01', '2016-01-01', '127.0.0.1'),
(150, 9, 'Second Floor Plan', 'second-floor-plan.jpg', 'D', 10, 'E', 'Keyur', '2016-01-01', '2016-01-01', '127.0.0.1'),
(151, 9, '', 'final-wwm-small-book-9.jpg', 'G', 0, 'E', '', '2016-01-01', '2016-01-01', ''),
(152, 9, '', 'final-wwm-small-book-5.jpg', 'G', 0, 'E', '', '2016-01-01', '2016-01-01', ''),
(153, 9, '', 'final-wwm-small-book-8.jpg', 'G', 0, 'E', '', '2016-01-01', '2016-01-01', ''),
(154, 9, '', 'final-wwm-small-book-3.jpg', 'G', 0, 'E', '', '2016-01-01', '2016-01-01', ''),
(155, 9, '', 'final-wwm-small-book-6.jpg', 'G', 0, 'E', '', '2016-01-01', '2016-01-01', ''),
(156, 9, '', 'final-wwm-small-book-10.jpg', 'G', 0, 'E', '', '2016-01-01', '2016-01-01', ''),
(157, 9, '', 'final-wwm-small-book-71.jpg', 'G', 0, 'E', '', '2016-01-01', '2016-01-01', ''),
(158, 9, '', 'final-wwm-small-book-101.jpg', 'G', 0, 'E', '', '2016-01-01', '2016-01-01', ''),
(159, 9, '', 'final-wwm-small-book-7.jpg', 'G', 0, 'E', '', '2016-01-01', '2016-01-01', ''),
(160, 9, '', 'final-wwm-small-book-16.jpg', 'G', 0, 'E', '', '2016-01-01', '2016-01-01', ''),
(161, 9, '', 'final-wwm-small-book-102.jpg', 'G', 0, 'E', '', '2016-01-01', '2016-01-01', ''),
(162, 10, '', 'slider3.jpg', 'E', 0, 'E', '', '2016-01-01', '2016-01-01', ''),
(163, 10, '', 'slider1.jpg', 'E', 0, 'E', '', '2016-01-01', '2016-01-01', ''),
(164, 10, '', 'slider2.jpg', 'E', 0, 'E', '', '2016-01-01', '2016-01-01', ''),
(171, 10, '', '9.jpg', 'G', 0, 'E', '', '2016-01-01', '2016-01-01', ''),
(170, 10, '', '8.jpg', 'G', 0, 'E', '', '2016-01-01', '2016-01-01', ''),
(169, 10, '', '10.jpg', 'G', 0, 'E', '', '2016-01-01', '2016-01-01', ''),
(168, 11, '', 'slider1.jpg', 'E', 0, 'E', '', '2016-01-01', '2016-01-01', ''),
(172, 10, '', '7.jpg', 'G', 0, 'E', '', '2016-01-01', '2016-01-01', ''),
(173, 10, '', '6.jpg', 'G', 0, 'E', '', '2016-01-01', '2016-01-01', ''),
(174, 10, '', '5.jpg', 'G', 0, 'E', '', '2016-01-01', '2016-01-01', ''),
(175, 10, '', '4.jpg', 'G', 0, 'E', '', '2016-01-01', '2016-01-01', ''),
(176, 10, '', '3.jpg', 'G', 0, 'E', '', '2016-01-01', '2016-01-01', ''),
(177, 10, '', '2.jpg', 'G', 0, 'E', '', '2016-01-01', '2016-01-01', ''),
(178, 10, '', '1.jpg', 'G', 0, 'E', '', '2016-01-01', '2016-01-01', ''),
(179, 12, '', 'slider3.jpg', 'E', 0, 'E', '', '2016-01-01', '2016-01-01', ''),
(180, 12, '', 'slider2.jpg', 'E', 0, 'E', '', '2016-01-01', '2016-01-01', ''),
(181, 12, '', 'slider1.jpg', 'E', 0, 'E', '', '2016-01-01', '2016-01-01', ''),
(182, 12, '', '17.jpg', 'G', 0, 'E', '', '2016-01-01', '2016-01-01', ''),
(183, 12, '', '16.jpg', 'G', 0, 'E', '', '2016-01-01', '2016-01-01', ''),
(188, 12, '', '11.jpg', 'G', 0, 'E', '', '2016-01-01', '2016-01-01', ''),
(189, 12, '', '10.jpg', 'G', 0, 'E', '', '2016-01-01', '2016-01-01', ''),
(190, 12, '', '9.jpg', 'G', 0, 'E', '', '2016-01-01', '2016-01-01', ''),
(191, 12, '', '8.jpg', 'G', 0, 'E', '', '2016-01-01', '2016-01-01', ''),
(192, 12, '', '7.jpg', 'G', 0, 'E', '', '2016-01-01', '2016-01-01', ''),
(193, 12, '', '6.jpg', 'G', 0, 'E', '', '2016-01-01', '2016-01-01', ''),
(194, 12, '', '5.jpg', 'G', 0, 'E', '', '2016-01-01', '2016-01-01', ''),
(196, 12, '', '3.jpg', 'G', 0, 'E', '', '2016-01-01', '2016-01-01', ''),
(197, 12, '', '2.jpg', 'G', 0, 'E', '', '2016-01-01', '2016-01-01', ''),
(198, 12, '', '1.jpg', 'G', 0, 'E', '', '2016-01-01', '2016-01-01', ''),
(199, 13, '', 'slider3.jpg', 'E', 0, 'E', '', '2016-01-01', '2016-01-01', ''),
(200, 13, '', 'slider2.jpg', 'E', 0, 'E', '', '2016-01-01', '2016-01-01', ''),
(201, 13, '', 'slider1.jpg', 'E', 0, 'E', '', '2016-01-01', '2016-01-01', ''),
(202, 13, '', '9.jpg', 'G', 0, 'E', '', '2016-01-01', '2016-01-01', ''),
(203, 13, '', '8.jpg', 'G', 0, 'E', '', '2016-01-01', '2016-01-01', ''),
(204, 13, '', '7.jpg', 'G', 0, 'E', '', '2016-01-01', '2016-01-01', ''),
(205, 13, '', '6.jpg', 'G', 0, 'E', '', '2016-01-01', '2016-01-01', ''),
(206, 13, '', '5.jpg', 'G', 0, 'E', '', '2016-01-01', '2016-01-01', ''),
(207, 13, '', '4.jpg', 'G', 0, 'E', '', '2016-01-01', '2016-01-01', ''),
(208, 13, '', '3.jpg', 'G', 0, 'E', '', '2016-01-01', '2016-01-01', ''),
(209, 13, '', '2.jpg', 'G', 0, 'E', '', '2016-01-01', '2016-01-01', ''),
(210, 13, '', '1.jpg', 'G', 0, 'E', '', '2016-01-01', '2016-01-01', ''),
(212, 14, '', 'slider1.jpg', 'E', 0, 'E', 'Keyur', '2016-01-01', '2016-08-09', '::1'),
(214, 14, '1st Floor Plan', '1stfloor.jpg', 'D', 3, 'E', 'Keyur', '2016-01-01', '2016-01-01', '192.168.1.122'),
(215, 14, 'Ground Floor', 'ground-floor.jpg', 'D', 1, 'E', 'Keyur', '2016-01-01', '2016-01-01', '192.168.1.122'),
(216, 14, '4BHK Typical Floor', '4bhk_typical_floor.jpg', 'D', 7, 'E', 'Keyur', '2016-01-01', '2016-01-01', '192.168.1.122'),
(217, 14, '3rd & 4th Floor Plan ', '3rd-4thfloor-plan.jpg', 'D', 6, 'E', 'Keyur', '2016-01-01', '2016-01-01', '192.168.1.122'),
(218, 14, '3BHK Floor Paln', '3bhk_typical_floor.jpg', 'D', 5, 'E', 'Keyur', '2016-01-01', '2016-01-01', '192.168.1.122'),
(219, 14, '2nd Floor Plan', '2nd-floor.jpg', 'D', 4, 'E', 'Keyur', '2016-01-01', '2016-01-01', '192.168.1.122'),
(220, 14, 'Ground Floor Plan', 'Ground-floorplan.jpg', 'D', 2, 'E', 'Keyur', '2016-01-01', '2016-01-01', '192.168.1.122'),
(227, 14, '', '12.jpg', 'G', 0, 'E', '', '2016-01-01', '2016-01-01', ''),
(230, 14, '', '9.jpg', 'G', 0, 'E', '', '2016-01-01', '2016-01-01', ''),
(231, 14, '', '8.jpg', 'G', 0, 'E', '', '2016-01-01', '2016-01-01', ''),
(232, 14, '', '7.jpg', 'G', 0, 'E', '', '2016-01-01', '2016-01-01', ''),
(234, 14, '', '5.jpg', 'G', 0, 'E', '', '2016-01-01', '2016-01-01', ''),
(235, 14, '', '2.jpg', 'G', 0, 'E', '', '2016-01-01', '2016-01-01', ''),
(239, 15, '', 'slider3.jpg', 'E', 0, 'E', '', '2016-01-01', '2016-01-01', ''),
(241, 15, '', 'slider1.jpg', 'E', 0, 'E', '', '2016-01-01', '2016-01-01', ''),
(242, 15, '1st Floor Plan', 'ground-floorplan.jpg', 'D', 3, 'E', 'Keyur', '2016-01-01', '2016-01-01', '192.168.1.122'),
(243, 15, 'Ground Floor', 'ground-floor.jpg', 'D', 1, 'E', 'Keyur', '2016-01-01', '2016-01-01', '192.168.1.122'),
(244, 15, '4BHK Typical Floor', '4bhk_typical_floor.jpg', 'D', 7, 'E', 'Keyur', '2016-01-01', '2016-01-01', '192.168.1.122'),
(245, 15, '3rd & 4th Floor Plan ', '3rd-4thfloor-plan.jpg', 'D', 6, 'E', 'Keyur', '2016-01-01', '2016-01-01', '192.168.1.122'),
(246, 15, '3BHK Floor Paln', '3bhk_typical_floor.jpg', 'D', 5, 'E', 'Keyur', '2016-01-01', '2016-01-01', '192.168.1.122'),
(247, 15, '2nd Floor Plan', '2nd-floor.jpg', 'D', 4, 'E', 'Keyur', '2016-01-01', '2016-01-01', '127.0.0.1'),
(248, 15, 'Ground Floor Plan', 'Ground-floorplan.jpg', 'D', 2, 'E', 'Keyur', '2016-01-01', '2016-01-01', '192.168.1.122'),
(257, 15, '', '10.jpg', 'G', 0, 'E', '', '2016-01-01', '2016-01-01', ''),
(258, 15, '', '11.jpg', 'G', 0, 'E', '', '2016-01-01', '2016-01-01', ''),
(267, 16, '', 'slider3.jpg', 'E', 2, 'E', '', '2016-01-01', '2016-01-01', ''),
(268, 16, '', 'slider2.jpg', 'E', 0, 'E', '', '2016-01-01', '2016-01-01', ''),
(269, 16, '', 'slider1.jpg', 'E', 0, 'E', '', '2016-01-01', '2016-01-01', ''),
(272, 16, '', '32.jpg', 'G', 0, 'E', '', '2016-01-01', '2016-01-01', ''),
(273, 16, '', '31.jpg', 'G', 0, 'E', '', '2016-01-01', '2016-01-01', ''),
(275, 16, '', '29.jpg', 'G', 0, 'E', '', '2016-01-01', '2016-01-01', ''),
(276, 16, '', '28.jpg', 'G', 0, 'E', '', '2016-01-01', '2016-01-01', ''),
(278, 16, '', '26.jpg', 'G', 0, 'E', '', '2016-01-01', '2016-01-01', ''),
(280, 16, '', '24.jpg', 'G', 0, 'E', '', '2016-01-01', '2016-01-01', ''),
(281, 16, '', '23.jpg', 'G', 0, 'E', '', '2016-01-01', '2016-01-01', ''),
(282, 16, '', '22.jpg', 'G', 0, 'E', '', '2016-01-01', '2016-01-01', ''),
(283, 16, '', '21.jpg', 'G', 0, 'E', '', '2016-01-01', '2016-01-01', ''),
(284, 16, '', '20.jpg', 'G', 0, 'E', '', '2016-01-01', '2016-01-01', ''),
(295, 16, '', '9.jpg', 'G', 0, 'E', '', '2016-01-01', '2016-01-01', ''),
(296, 16, '', '8.jpg', 'G', 0, 'E', '', '2016-01-01', '2016-01-01', ''),
(304, 16, 'Upper & Lower - Basement', 'upper-lower-Basement.jpg', 'D', 1, 'E', 'Keyur', '2016-01-01', '2016-01-01', '127.0.0.1'),
(305, 16, 'Typical 1st-18th Floor Plan', 'typical-1st-to-18th-floor-layout-plan.jpg', 'D', 2, 'E', 'Keyur', '2016-01-01', '2016-01-01', '192.168.1.122'),
(306, 16, 'Building A Floor Plan', 'Building-A-Typical1st-to-18th-floor-plan.jpg', 'D', 3, 'E', 'Keyur', '2016-01-01', '2016-01-01', '192.168.1.122'),
(307, 16, 'Building B, C, D, E Plan', 'building-bcde-1st-to-18th-floorplan.jpg', 'D', 4, 'E', 'Keyur', '2016-01-01', '2016-01-01', '192.168.1.122'),
(308, 16, 'Terrace Floor Plan', 'Terrace-Floor-Layout-Plan.jpg', 'D', 5, 'E', 'Keyur', '2016-01-01', '2016-01-01', '192.168.1.122'),
(309, 17, '', 'slider3.jpg', 'E', 0, 'E', '', '2016-01-01', '2016-01-01', ''),
(310, 17, '', 'slider2.jpg', 'E', 0, 'E', '', '2016-01-01', '2016-01-01', ''),
(311, 17, '', 'slider1.jpg', 'E', 0, 'E', '', '2016-01-01', '2016-01-01', ''),
(312, 17, '', '4.jpg', 'G', 0, 'E', '', '2016-01-01', '2016-01-01', ''),
(313, 17, '', '3.jpg', 'G', 0, 'E', '', '2016-01-01', '2016-01-01', ''),
(314, 17, '', '2.jpg', 'G', 0, 'E', '', '2016-01-01', '2016-01-01', ''),
(315, 17, '', '1.jpg', 'G', 0, 'E', '', '2016-01-01', '2016-01-01', ''),
(317, 10, 'Shopping Floor Plan', 'shopping-floor.jpg', 'D', 2, 'E', 'Keyur', '2016-01-01', '2016-01-01', '127.0.0.1'),
(318, 10, 'Layout Plan', 'layoutplan.jpg', 'D', 1, 'E', 'Keyur', '2016-01-01', '2016-01-01', '127.0.0.1'),
(319, 10, '', '8.jpg', 'S', 0, 'E', 'Keyur', '2016-01-01', '2016-01-01', '127.0.0.1'),
(320, 10, '', '5.jpg', 'S', 0, 'E', 'Keyur', '2016-01-01', '2016-01-01', '127.0.0.1'),
(321, 10, '', '1.jpg', 'S', 0, 'E', 'Keyur', '2016-01-01', '2016-01-01', '127.0.0.1'),
(325, 13, '', '1 (1).jpg', 'S', 0, 'E', '', '2016-01-01', '2016-01-01', ''),
(326, 13, '', '2 (1).jpg', 'S', 0, 'E', '', '2016-01-01', '2016-01-01', ''),
(327, 13, '', '3 (1).jpg', 'S', 0, 'E', '', '2016-01-01', '2016-01-01', ''),
(333, 17, '', '3 (2).jpg', 'S', 0, 'E', '', '2016-01-01', '2016-01-01', ''),
(332, 17, '', '2 (2).jpg', 'S', 0, 'E', '', '2016-01-01', '2016-01-01', ''),
(331, 17, '', '1 (2).jpg', 'S', 0, 'E', '', '2016-01-01', '2016-01-01', ''),
(334, 14, '', '1 (1).jpg', 'S', 0, 'E', '', '2016-01-01', '2016-01-01', ''),
(335, 14, '', '2 (1).jpg', 'S', 0, 'E', '', '2016-01-01', '2016-01-01', ''),
(336, 14, '', '3 (1).jpg', 'S', 0, 'E', '', '2016-01-01', '2016-01-01', ''),
(337, 16, '', '2 (1).jpg', 'S', 0, 'E', '', '2016-01-01', '2016-01-01', ''),
(338, 16, '', '1 (1).jpg', 'S', 0, 'E', '', '2016-01-01', '2016-01-01', ''),
(339, 16, '', '3 (1).jpg', 'S', 0, 'E', '', '2016-01-01', '2016-01-01', ''),
(340, 15, '', '1 (1).jpg', 'S', 0, 'D', '', '2016-01-01', '2016-01-01', ''),
(341, 15, '', '2 (1).jpg', 'S', 0, 'D', '', '2016-01-01', '2016-01-01', ''),
(342, 15, '', '3 (1).jpg', 'S', 0, 'D', '', '2016-01-01', '2016-01-01', ''),
(353, 14, '', '13.jpg', 'C', 0, 'E', '', '2016-01-01', '2016-01-01', '0'),
(344, 15, '', '13 (5).jpg', 'G', 0, 'E', '', '2016-01-01', '2016-01-01', '0'),
(345, 14, '', '3 (2).jpg', 'G', 0, 'E', '', '2016-01-01', '2016-01-01', '0'),
(346, 14, '', '4 (1).jpg', 'G', 0, 'E', '', '2016-01-01', '2016-01-01', '0'),
(347, 14, '', '6 (1).jpg', 'G', 0, 'E', '', '2016-01-01', '2016-01-01', '0'),
(348, 14, '', '1 (2).jpg', 'G', 0, 'E', '', '2016-01-01', '2016-01-01', '0'),
(350, 16, '', '4.jpg', 'C', 0, 'E', '', '2016-01-01', '2016-01-01', '0'),
(351, 16, '', '9.jpg', 'C', 0, 'E', '', '2016-01-01', '2016-01-01', '0'),
(352, 16, '', '3.jpg', 'C', 0, 'E', '', '2016-01-01', '2016-01-01', '0'),
(354, 14, '', '5.jpg', 'C', 0, 'E', '', '2016-01-01', '2016-01-01', '0'),
(355, 14, '', '8.jpg', 'C', 0, 'E', '', '2016-01-01', '2016-01-01', '0'),
(356, 14, '', '11.jpg', 'C', 0, 'E', '', '2016-01-01', '2016-01-01', '0'),
(357, 15, '', '1.jpg', 'C', 0, 'E', '', '2016-01-01', '2016-01-01', '0'),
(358, 15, '', '10.jpg', 'C', 0, 'E', '', '2016-01-01', '2016-01-01', '0'),
(359, 15, '', '12.jpg', 'C', 0, 'E', '', '2016-01-01', '2016-01-01', '0'),
(360, 14, '', 'slider2.jpg', 'E', 0, 'E', '', '2016-01-01', '2016-01-01', '0'),
(361, 14, '', 'slider3 (1).jpg', 'E', 0, 'E', '', '2016-01-01', '2016-01-01', '0');

-- --------------------------------------------------------

--
-- Table structure for table `gallery_master`
--

CREATE TABLE `gallery_master` (
  `image_id` int(11) NOT NULL,
  `username` varchar(100) NOT NULL DEFAULT '',
  `createdate` date NOT NULL DEFAULT '0000-00-00',
  `modifieddate` date NOT NULL DEFAULT '0000-00-00',
  `a_id` int(11) NOT NULL,
  `image_title` varchar(255) NOT NULL DEFAULT '',
  `gallery_image` text NOT NULL,
  `sortorder` int(11) NOT NULL,
  `isFront` varchar(100) NOT NULL DEFAULT '',
  `status` char(1) NOT NULL DEFAULT 'E',
  `remote_ip` varchar(50) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `gallery_master`
--

INSERT INTO `gallery_master` (`image_id`, `username`, `createdate`, `modifieddate`, `a_id`, `image_title`, `gallery_image`, `sortorder`, `isFront`, `status`, `remote_ip`) VALUES
(12, 'Keyur', '2016-04-04', '2016-07-30', 8, 'Image 1', '12-2.jpg', 1, '', 'E', '127.0.0.1'),
(18, 'Keyur', '2016-04-12', '0000-00-00', 10, 'Image 1', '18-3.jpg', 7, '', 'D', '127.0.0.1'),
(14, 'Keyur', '2016-04-12', '2016-07-30', 8, 'Image 2', '14-4.jpg', 3, '', 'E', '127.0.0.1'),
(15, 'Keyur', '2016-04-12', '2016-07-30', 8, 'Image 3', '15-9.jpg', 4, '', 'E', '127.0.0.1'),
(16, 'Keyur', '2016-04-12', '2016-07-30', 8, 'Image 4', '16-5.jpg', 5, '', 'E', '127.0.0.1'),
(17, 'Keyur', '2016-04-12', '2016-07-30', 8, 'Image 5', '17-9.jpg', 6, '', 'E', '127.0.0.1'),
(19, 'Keyur', '2016-04-12', '2016-10-01', 9, 'Image 8', '19-Ambica-Group-Navratri-01.jpg', 8, '', 'E', '103.69.200.47'),
(20, 'Keyur', '2016-08-09', '2016-08-09', 11, 'Ambika Pinnacle', '20-1.jpg', 9, '', 'E', '::1'),
(21, 'Keyur', '2016-08-09', '2016-08-09', 11, 'Ambika Solitaire', '21-3.jpg', 10, '', 'E', '::1'),
(22, 'Keyur', '2016-08-09', '2016-08-09', 11, 'Ambika Pinnacle', '22-12.jpg', 11, '', 'E', '::1'),
(23, 'Keyur', '2016-08-09', '2016-08-09', 11, 'Ambika Solitaire', '23-9.jpg', 12, '', 'E', '::1'),
(24, 'Keyur', '2016-08-09', '2016-08-09', 11, 'Ambika Pinnacle', '24-5.jpg', 13, '', 'E', '::1'),
(25, 'Keyur', '2016-08-09', '2016-08-09', 11, 'Ambika Solitaire', '25-3.jpg', 14, '', 'E', '::1');

-- --------------------------------------------------------

--
-- Table structure for table `group_master`
--

CREATE TABLE `group_master` (
  `group_id` int(11) NOT NULL,
  `group_name` varchar(50) NOT NULL DEFAULT '',
  `group_status` char(1) NOT NULL DEFAULT ''
) ENGINE=MyISAM DEFAULT CHARSET=latin1 PACK_KEYS=0;

--
-- Dumping data for table `group_master`
--

INSERT INTO `group_master` (`group_id`, `group_name`, `group_status`) VALUES
(1, 'Administrator', 'E'),
(2, 'Developer', 'T');

-- --------------------------------------------------------

--
-- Table structure for table `homecontent`
--

CREATE TABLE `homecontent` (
  `content_id` int(11) NOT NULL,
  `username` varchar(100) NOT NULL,
  `createdate` date NOT NULL,
  `modifieddate` date NOT NULL,
  `home_content` text,
  `cover_image` text NOT NULL,
  `status` char(1) NOT NULL,
  `sortorder` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `homecontent`
--

INSERT INTO `homecontent` (`content_id`, `username`, `createdate`, `modifieddate`, `home_content`, `cover_image`, `status`, `sortorder`) VALUES
(1, 'Keyur', '2016-02-20', '2016-09-01', '', '1-Chrysanthemum.jpg', 'E', 1);

-- --------------------------------------------------------

--
-- Table structure for table `item`
--

CREATE TABLE `item` (
  `item_id` int(11) NOT NULL,
  `title` varchar(50) NOT NULL,
  `subtitle` text NOT NULL,
  `short_desc` text NOT NULL,
  `price` varchar(70) NOT NULL,
  `image` text,
  `category_id` int(11) NOT NULL,
  `username` varchar(30) DEFAULT NULL,
  `createdate` date DEFAULT NULL,
  `modifieddate` date DEFAULT NULL,
  `sortorder` int(8) DEFAULT NULL,
  `status` char(1) NOT NULL DEFAULT 'E',
  `remote_ip` varchar(100) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `item`
--

INSERT INTO `item` (`item_id`, `title`, `subtitle`, `short_desc`, `price`, `image`, `category_id`, `username`, `createdate`, `modifieddate`, `sortorder`, `status`, `remote_ip`) VALUES
(1, 'Veg 65', 'Sub Title', '<p>Short Description</p>', '30.00', '1-sunil.jpg', 1, 'Keyur', '2016-02-20', '2016-02-20', 1, 'E', '127.0.0.1'),
(2, 'veg 65', 'veg 65', '<p>veg 65</p>', '30.00', '2-2.jpg', 2, 'Keyur', '2016-02-22', NULL, 2, 'E', '127.0.0.1'),
(3, 'veg 65', 'veg 65', '<p>veg 65</p>', '65.00', '3-2.jpg', 3, 'Keyur', '2016-02-22', NULL, 3, 'E', '127.0.0.1'),
(4, 'veg 65', 'veg 65', '<p>veg 65</p>', '45.00', '4-Penguins.jpg', 4, 'Keyur', '2016-02-22', NULL, 4, 'E', '127.0.0.1');

-- --------------------------------------------------------

--
-- Table structure for table `job_master`
--

CREATE TABLE `job_master` (
  `job_id` int(11) NOT NULL,
  `j_name` varchar(150) NOT NULL,
  `j_email` varchar(50) NOT NULL,
  `j_message` varchar(255) DEFAULT NULL,
  `j_resume` varchar(255) NOT NULL,
  `j_contact` varchar(100) DEFAULT NULL,
  `j_date` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `job_master`
--

INSERT INTO `job_master` (`job_id`, `j_name`, `j_email`, `j_message`, `j_resume`, `j_contact`, `j_date`) VALUES
(569, 'Zil Mehta', 'zilmehta@gmail.com', 'Test Message ', '', '9904990399', '2016-04-09 10:25:46'),
(570, 'Zil Mehta', 'zilmehta@gmail.com', 'sdfsdfsdf', '', '9904995093', '2016-04-09 10:26:54'),
(571, 'Zil Mehta', 'zilmehta@gmail.com', 'vxcvxcv', '', '5435345345', '2016-04-09 10:38:04'),
(572, 'Zil Mehta', 'zilmehta@gmail.com', 'fgdfg', '', 's45345345', '2016-04-09 10:44:55'),
(574, 'Zil Mehta', 'zilmehta@gmail.com', 'fsdfsdf', '574-sample.pdf', '3234234', '2016-04-09 10:53:09'),
(575, 'xzx', 'abc@xyz.com', 'saxzxzxz', '', '2121212', '2016-07-29 13:00:39'),
(576, 'saasas', 'abc@xyz.com', 'wdsdsdsd', '', '2123323', '2016-07-29 13:02:08'),
(577, 'saaas', 'abc@xyz.com', 'saassa', '', 'sasa', '2016-07-29 13:02:45'),
(578, 'aesa', 'aesha.patel@easternts.com', 'sdsdsdsd', '', 'dds', '2016-07-29 13:03:16'),
(579, 'zXzzx', 'abc@xyz.com', 'xzxzxzx', '', '3223', '2016-07-29 13:03:49'),
(580, 'sdsa', 'aesha9794@gmail.com', 'sasasaas', '', 'sasaas', '2016-07-29 13:06:59'),
(581, 'saasas', 'aesha.patel@easternts.com', 'xzxzxz', '', '22', '2016-07-29 13:07:22'),
(582, 'zxz', 'abc@xyz.com', 'xzzxzx', '', 'xzxz', '2016-07-29 13:07:51'),
(583, 'weqw', 'aesha9794@gmail.com', 'sddsdsddasa', '', 'sasasda', '2016-07-29 13:08:17'),
(584, 'AS', 'aesha.patel@easternts.com', 'SDSDSD', '', 'DSSD', '2016-07-29 13:09:06'),
(585, 'saas', 'aes123@gmail.com', 'saas', '', 'sasa', '2016-07-29 13:11:15'),
(586, 'sdsasa', 'aesha.patel@easternts.com', 'xxzcxz', '', '22121', '2016-07-29 13:13:30'),
(587, 'aesha', 'aes123@gmail.com', 'ssaassa', '587-mission.jpg', '21333', '2016-07-29 13:14:55'),
(588, 'saS', 'aes123@gmail.com', 'TEST', '588-p-1.jpg', '221332', '2016-07-30 18:48:50'),
(589, 'jatin adiyecha', 'jatinadiyecha18@gmail.com', 'i have a four years experience in related field.', '589-Adiyecha.docx', '9998805936', '2016-09-06 17:11:42'),
(590, 'Sagar', 'sagarkakadia@gmail.com', 'i am currently working with Happy Home Corporation as a Account cum Purchase Manager ', '590-Resume.doc', '9723482050', '2016-09-09 15:25:09'),
(591, 'Diyora Pratik', 'diyorapratik@gmail.com', 'I am interested work with your company, I have 2 year Experience in Real Estate. So when you Start  the  Ambika Solitaire at time i would like to give interview. So Please Call at that Time.\r\nThank You', '591-RESUME.pdf', '7567914603', '2016-09-10 13:44:37'),
(592, 'Bhavdip B. Bagsariya', 'bhavdipbagsariya@gmail.com', 'NA', '592-resume.0.pdf', '8866988913', '2016-09-30 05:40:17'),
(593, 'Divyesh R. Dobariya ', 'Er.divyeshdobariya@gmail.com', 'Job applying for the post of civil engineer.', '593-Divyesh Updated.pdf', '+91-8000052220', '2016-10-06 18:21:43'),
(594, 'Divyesh R. Dobariya ', 'Er.divyeshdobariya@gmail.com', 'Job applying for the post of civil engineer.', '594-Divyesh Updated.pdf', '+91-8000052220', '2016-10-06 18:23:15'),
(595, 'PETHANI BHAVESHKUMAR KISHORBHAI', '13bcl083@nirmauni.ac.in', 'Looking for an Opportunity to use my knowledge, skill and talent to prove my ability for achieving height and helping the organization to achieve its long term goals.', '595-13BCL083-Bhavesh Pethani.pdf', '9737498637', '2016-10-12 17:41:55'),
(596, '', '', '', '', '', '2016-10-13 02:30:52'),
(597, '', '', '', '', '', '2016-10-13 02:32:43'),
(598, 'shailesh sheladiya', 'sheladiya.shailesh@yahoo.in', 'for account job', '598-SHAILESH_CV.doc', '9714218304', '2016-10-25 08:02:38'),
(599, '', '', '', '', '', '2016-11-21 21:25:53'),
(600, '', '', '', '', '', '2016-11-21 21:26:53'),
(601, 'patel ankit rameshbhai', 'p.ankit32@ymail.com', 'Sir i have my BE civil engineering in 2013.i have 2 years of experiance in construction field.', '601-ANKIT.doc', '9586019456', '2016-11-23 06:22:11'),
(602, 'Dhaval Vispute', 'dhavalvispute95@gmail.com', 'I have completed my bachelorâ€™s from GIDC Degree College, Navsari. I looking for a civil engineering job. I am attaching my resume', '602-Dhaval Vispute Resume.pdf', '7990345954', '2016-12-10 14:24:20'),
(603, 'padsala amitbhai bhikhalal', 'piyushpadsala@yahoo.in', 'Draftsman Civil & Technical Assistant Job Requirement', '603-Amit padsala Resume.docx', '9537311865', '2017-01-21 09:29:16'),
(604, 'Jatin Chauhan', 'jatin.jpc@gmail.com', 'To,\r\nThe Manager (HR),\r\nAmbika Group - VIP circle,\r\nNear Kapodara Bridge,\r\nUtran, Mota Varachha\r\n\r\nRespected sir,\r\n\r\n\r\nRefer to the above mentioned subject, I wish to apply for the post of\r\na Head- Projects Development. The position seems as ideal opportu', '604-CURRICULUM VITAE JATIN CHAUHAN.PDF.pdf', '9727257082', '2017-02-13 07:54:10'),
(605, '', '', '', '', '', '2017-02-17 05:24:07'),
(606, '', '', '', '', '', '2017-02-17 05:25:07'),
(607, 'Keval', 'savanikeval23@gmail.com', 'Civil engineering', '607-KEVAL CV 2017.pdf', '9510044075', '2017-03-28 07:14:12'),
(608, 'DrashtiRamoliya', 'drashtiramoliya12@gmail.com', 'I have attached my cv over here. If you have any opening than please contact me.', '608-Dasu Ramoliya.doc', '8866793635', '2017-03-30 12:09:31'),
(609, 'Gadiwala Mohammadyahya A', 'gadiwalayahya@gmail.com', 'I m finding a job for site Engg or Project Engg\r\nI m Completed my BE Civil Engg back in 2016\r\nRight Now I m Working as a Trainee At Millennium 4 in Surat', '609-MOHAMMED YAHYA A GADIWALA.docx', '9825274557', '2017-04-04 11:00:17'),
(610, 'Nilesh J Dhorajiya', 'neeljdhorajiya@gmail.com', 'For The Post of Legal Advocate', '610-Nilesh Dhorajiya-LLB BCOM CV.pdf', '+919712372401', '2017-04-14 15:26:31'),
(611, 'Viralkumar P Navadiya', 'viralkumarn@yahoo.com', 'For Sr engineer in best performance', '611-viral resume.pdf', '9377121087', '2017-04-21 13:12:26'),
(612, '', '', '', '', '', '2017-04-27 18:25:48'),
(613, '', '', '', '', '', '2017-04-27 18:26:53'),
(614, 'vipul padsala', 'vipul.padsala009@gmail.com', 'Apply for job', '614-vipul padsala.pdf', '9537570515', '2017-05-04 20:04:44'),
(615, 'Rudra Deepak', 'deepakrudra9@gmail.com', 'I am searching for site engineer job', '615-Resume.docx', '9724437795', '2017-05-27 14:58:54'),
(616, 'VADALIYA PARASKUMAR RAMESHBHAI', 'vadaliyaparas055@gmail.com', 'I want to work as a civil engineer in your company.', '616-paras resume.docx', '9586148559', '2017-06-12 10:54:13'),
(617, 'Hitesh tank', 'tankhitesh53@gmail.com', 'â€ŒI am fresher civil engineer. I complete BE in civil engineering at Vishwakarma government engineering college on may 2017. I work as site engineer or trainee with ambika group.', '617-Resume_2017-01-25-11-42-48.pdf', '9537726146', '2017-06-15 12:31:15'),
(618, 'Maulik Savaliya ', 'mauliksavaliya8@gmail.com', 'I am interested to work for you\r\nSo please give me a chnace', '618-resume.pdf', '9725926501', '2017-07-03 11:00:13'),
(619, 'Dishant ', 'bhavini.easternts@gmail.com', 'Dishant ', '619-pdf-sample.pdf', '123456', '2017-07-17 09:04:31'),
(620, 'vivek rathod', 'vmrathod008@gmail.com', 'requirement : job as civil engineer  fresher.', '620-Resume Vivek rathod 2017 updated.pdf', '8347936527', '2017-07-26 10:21:29'),
(621, 'Sandeep Baban Jadhav', 'sandipjadhav629@gmail.com', 'Dear Sir / Mam \r\nI have Apportunity to me in your company', '621-Sandeep  BE resume.doc', '08626011357', '2017-07-27 09:45:48'),
(622, 'RAFUSEEYA DALSUKH VIRSINGBHAI', 'dalsukh156@yahoo.com', 'Dear HR sir/mem\r\nI have apply thise opportunity please dear I hardly waiting your best reply..\r\nThank you', '622-Wing Fin.2017.pdf', '9638100156', '2017-07-30 13:17:59'),
(623, 'Nilesh Dhorajiya', 'neeljdhorajiya@gmail.com', '', '', '', '2017-08-09 08:41:37'),
(624, 'Dilip Gopal vekhande', 'dilipvekhande26@gmail.com', 'I am BE civil engineer Mumbai university having experience 5 year in construction field..', '624-civil engineer cv.pdf', '7066006744', '2017-08-14 07:05:06'),
(625, 'ketangondaliya', 'ketangondaliya7@gmail.com', 'marketing job apply', '625-RESUME.docx', '8866265823', '2017-08-30 13:43:50'),
(626, '', '', '', '', '', '2017-10-10 11:31:50'),
(627, 'Shobhit Johari', 'shobhit.dap@gmail.com', 'Looking forward to exploring in real-estate industry I believe I can be very helpful to you in our ', '627-cv shobhit.pdf', '9601965645', '2017-10-10 11:31:50'),
(628, 'Dilip Gopal vekhande', 'dilipvekhande26@gmail.com', 'I am civil engineer', '628-civil engineer cv.pdf', '8805555432', '2017-10-24 10:27:51'),
(629, 'Deepak Kumar Singh', 'thakur212@gmail.com', 'I can work anywhere in india.', '629-Resume  (1).pdf', '8447178137', '2017-11-28 09:18:01'),
(630, 'H r sureshkumar ', 'hrsureshsuresh@gmail.com', 'Sir here I sending my resume for post of any construction engineer job. I handing heigh rise buildings. ', '630-new resume including rainbow creat on 03-12-2017.docx', '9901534062 ', '2017-12-12 07:34:51'),
(631, 'H R Suresh ', 'hrsureshsuresh@gmail.com', 'Sir here I sending my resume for post of project engineer ', '631-new resume including rainbow creat on 03-12-2017.docx', '9901534062 ', '2017-12-12 07:53:57'),
(632, 'Sameer Shetti', 'sameershetti@yahoo.com', 'Hi,\r\n\r\nGood Morning!\r\n\r\nI am sending you my updated CV that outlines my overall experience till date. I am actively in search of a relevant position in your highly esteemed company.\r\nIn all, I have approximately 17 years of work experience, in which I hav', '632-SAMEER A SHETTI - RESUME.pdf', '9619879585', '2018-01-12 02:47:16'),
(633, 'Shrikrishna pal', 'crishnapal@gmail.com', 'Sir, i want to join job', '633-crishna resume for sample.pdf', '8347270707', '2018-01-17 05:36:57'),
(634, 'Gurwinder Singh', 'enggcivil2014@gmail.com', 'I am searching for civil engineer job. I had a strong expierence in Structure work &  project management.', '634-Gurwinder Resume.doc', '9501303132,8194959627', '2018-02-06 09:53:48'),
(635, 'chaudhari Naitikbhai R.', 'chaudhari.naitik.93@gmail.com', 'i find job for civil engineer. ', '635-DOC-20180307-WA0026.docx', '9726563836', '2018-03-09 05:34:45'),
(636, 'MAHESH G PATIL', 'maheshgpatil969@gmail.com', 'Dear Precious Sir / Mam,\r\n\r\nIts regarding that, I request you to consider my application for the position of SALES & BUSINESS DEVELOPMENT STRATEGY, in your Well Reputed Company. I have done B. A. from Shivaji University, Maharashtra, specializing in Engli', '636-Resume_Mahesh.pdf', '9663326327', '2018-04-03 09:54:24'),
(637, 'MAHESH PATIL', 'maheshgpatil969@gmail.com', 'â€¢	KEY SKILLS\r\n\r\nâ€¢	Ability to learn quickly\r\nâ€¢	Well-developed communication skills, Ability to work with a wide variety of people\r\nâ€¢	Experience in sales or marketing job\r\nâ€¢	Ability to take independent decisions based on criteria explained beforeh', '637-Resume_Mahesh.pdf', '9663326327', '2018-04-06 09:57:19'),
(638, 'MAHESH PATIL', 'maheshgpatil969@gmail.com', 'Dear Precious Sir / Mam,\r\n\r\nIts regarding that, I request you to consider my application for the position of SALES & BUSINESS DEVELOPMENT STRATEGY, in your Well Reputed Company. I have done B. A. from Shivaji University, Maharashtra, specializing in Engli', '638-Resume_Mahesh.doc', '7975535611', '2018-04-12 14:09:43'),
(639, 'deepu parimoo', 'deepuparimoo77@gmail.com', 'Apply for the post of Estate Manager. Having 15yr`s Experience in this Field.', '639-Resume For The Post Of Estate Manager.doc', '9876451927', '2018-04-16 09:34:35'),
(640, 'Kush dhiman', 'dhimankush2@gmail.com', 'Please find here attached copy of my Updated CV that details my past experience and other necessary documents and reply me back if you find any vacancy within your company. ', '640-KUSH C.V.docx', '8168715373', '2018-04-17 19:22:36'),
(641, 'Shahrukh Khan', 'shahrukh.mit@gmail.com', 'Disciplined mechanical engineer looking forward to propel a career in the field of mechanical engineering with my technical and managerial skills and make a significant contribution  in exponential growth of the industry.', '641-srk c.v.pdf', '7376102795', '2018-04-22 14:30:03');

-- --------------------------------------------------------

--
-- Table structure for table `module_master`
--

CREATE TABLE `module_master` (
  `module_id` int(5) NOT NULL,
  `module_title` varchar(50) NOT NULL,
  `module_name` varchar(100) NOT NULL,
  `module_file` varchar(25) NOT NULL,
  `module_parent` varchar(20) NOT NULL,
  `module_seo_slug` varchar(25) NOT NULL,
  `sortorder` int(3) NOT NULL,
  `status` char(1) NOT NULL DEFAULT 'D',
  `username` varchar(30) NOT NULL,
  `createdate` date NOT NULL,
  `modifieddate` date NOT NULL,
  `remote_ip` varchar(50) NOT NULL,
  `module_controller` varchar(70) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `module_master`
--

INSERT INTO `module_master` (`module_id`, `module_title`, `module_name`, `module_file`, `module_parent`, `module_seo_slug`, `sortorder`, `status`, `username`, `createdate`, `modifieddate`, `remote_ip`, `module_controller`) VALUES
(1, 'Contact', 'Contact', 'contact', '', '', 0, 'E', '', '0000-00-00', '0000-00-00', '', 'contact'),
(2, 'User Management', 'User', 'user', '', '', 0, 'E', '', '0000-00-00', '0000-00-00', '', 'user'),
(3, 'User Group Management', 'User Group', 'usergroup', '', '', 0, 'E', '', '0000-00-00', '0000-00-00', '', 'user'),
(4, 'Slider Management', 'Slider', 'slider', '', '', 0, 'E', '', '0000-00-00', '0000-00-00', '', 'slider'),
(5, 'Permission Management', 'Permission', 'permission', '', '', 0, 'E', '', '0000-00-00', '0000-00-00', '', 'permission'),
(6, 'Pages Management', 'Pages', 'pages', '', '', 0, 'E', '', '0000-00-00', '0000-00-00', '', 'pages'),
(7, 'Page Image Management', 'Page Images', 'pageimages', '', '', 0, 'E', '', '0000-00-00', '0000-00-00', '', 'pages'),
(8, 'Homecontent Management', 'Homecontent', 'homecontent', '', '', 0, 'E', '', '0000-00-00', '0000-00-00', '', 'pages'),
(9, 'Feedback List', 'Feedback List', 'feedback', '', '', 0, 'E', '', '0000-00-00', '0000-00-00', '', 'feedback'),
(11, 'epanel', 'epanel', 'epanel', '', '', 0, 'E', '', '0000-00-00', '0000-00-00', '', 'settings'),
(12, 'website', 'website', 'website', '', '', 0, 'E', '', '0000-00-00', '0000-00-00', '', 'settings'),
(13, 'News', 'News', 'news', '', '', 0, 'E', '', '0000-00-00', '0000-00-00', '', 'news'),
(14, 'News Type', 'News Type', 'newsmaster', '', '', 0, 'E', '', '0000-00-00', '0000-00-00', '', 'news'),
(15, 'Testimonial', 'Testimonial', 'testimonial', '', '', 0, 'E', '', '0000-00-00', '0000-00-00', '', 'testimonial'),
(16, 'Testimonial Type', 'Testimonial Type', 'testimonialtype', '', '', 0, 'E', '', '0000-00-00', '0000-00-00', '', 'testimonial'),
(17, 'Album Type', 'Album Type', 'albumtype', '', '', 0, 'E', '', '0000-00-00', '0000-00-00', '', 'gallery'),
(18, 'Album', 'Album', 'album', '', '', 0, 'E', '', '0000-00-00', '0000-00-00', '', 'gallery'),
(19, 'Gallery', 'Gallery', 'gallery', '', '', 0, 'E', '', '0000-00-00', '0000-00-00', '', 'gallery'),
(20, 'Subscription', 'Subscription', 'subscription', '', '', 0, 'E', '', '0000-00-00', '0000-00-00', '', 'subscription'),
(21, 'Job', 'Job', 'job', '', '', 0, 'E', '', '0000-00-00', '0000-00-00', '', 'job'),
(22, 'Product', 'Product', 'product', '', '', 0, 'E', '', '0000-00-00', '0000-00-00', '', 'products');

-- --------------------------------------------------------

--
-- Table structure for table `news`
--

CREATE TABLE `news` (
  `news_id` int(11) NOT NULL,
  `username` varchar(200) NOT NULL,
  `createdate` date NOT NULL,
  `modifieddate` date NOT NULL,
  `news_type` int(1) NOT NULL,
  `news_title` varchar(150) NOT NULL DEFAULT '',
  `news_desc` text NOT NULL,
  `eve_date` date NOT NULL,
  `remote_ip` varchar(50) NOT NULL,
  `status` char(1) NOT NULL DEFAULT 'E',
  `sortorder` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `news`
--

INSERT INTO `news` (`news_id`, `username`, `createdate`, `modifieddate`, `news_type`, `news_title`, `news_desc`, `eve_date`, `remote_ip`, `status`, `sortorder`) VALUES
(5, 'Keyur', '2015-10-27', '2016-07-30', 1, 'Ambika Pinnacle', 'Ambika Pinnacle Coming  Soon in Surat ! Which is completely out standing.', '2016-01-01', '127.0.0.1', 'E', 1),
(6, 'Keyur', '2016-03-11', '2016-07-30', 1, 'Ambika Dreams', 'Ambika Dreams , A Residencial Project is  , Coming Soon !', '2016-01-01', '127.0.0.1', 'E', 2),
(7, 'Keyur', '2016-04-03', '2016-07-30', 1, 'Ambika Heights', '1st Mordern Residency in Adajan , thats beyond incredible.', '2016-01-01', '127.0.0.1', 'E', 3),
(8, 'Keyur', '2016-07-29', '2016-07-30', 1, 'Ambika Solitaire', 'New project is coming soon Which is completely out standing.', '2016-08-01', '127.0.0.1', 'E', 4);

-- --------------------------------------------------------

--
-- Table structure for table `news_type`
--

CREATE TABLE `news_type` (
  `news_type_id` int(11) NOT NULL,
  `news_type` varchar(50) NOT NULL,
  `createdate` date NOT NULL,
  `modifieddate` date NOT NULL,
  `username` varchar(30) NOT NULL,
  `status` char(1) NOT NULL DEFAULT 'E',
  `sortorder` int(3) NOT NULL,
  `remote_ip` varchar(15) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `news_type`
--

INSERT INTO `news_type` (`news_type_id`, `news_type`, `createdate`, `modifieddate`, `username`, `status`, `sortorder`, `remote_ip`) VALUES
(1, 'News', '2014-07-21', '2015-10-27', 'Keyur', 'E', 1, '192.168.2.106');

-- --------------------------------------------------------

--
-- Table structure for table `page_master`
--

CREATE TABLE `page_master` (
  `page_id` bigint(11) NOT NULL,
  `parent_id` bigint(11) NOT NULL,
  `page_template` varchar(200) NOT NULL,
  `page_slug` varchar(255) NOT NULL,
  `page_title` varchar(255) NOT NULL,
  `page_content` text NOT NULL,
  `page_image` varchar(100) DEFAULT NULL,
  `meta_title` varchar(255) NOT NULL,
  `meta_desc` text NOT NULL,
  `meta_keyword` text NOT NULL,
  `sortorder` int(8) DEFAULT NULL,
  `status` char(1) NOT NULL COMMENT 'E-Enable D- Disable',
  `user_id` int(8) NOT NULL,
  `createdate` datetime DEFAULT NULL,
  `modifieddate` datetime DEFAULT NULL,
  `remote_ip` varchar(100) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `page_master`
--

INSERT INTO `page_master` (`page_id`, `parent_id`, `page_template`, `page_slug`, `page_title`, `page_content`, `page_image`, `meta_title`, `meta_desc`, `meta_keyword`, `sortorder`, `status`, `user_id`, `createdate`, `modifieddate`, `remote_ip`) VALUES
(10, 0, 'inner_page.tpl.php', 'about-us', 'About Us', '<div class=\"col-md-6 col-sm-6 col-xs-12\">\n<div class=\"about-post\"><img class=\"img-responsive\" src=\"/images/about_us1.jpg\" alt=\"\" />\n<p><!--						<?php echo stripslashes($page_res[\'page_content\']); ?>                                                       --></p>\n<p style=\"text-align: justify;\">We as Ambika group aim to be expert in developing projects that are technologically complex and exhibit quality infrastructure. We are one of the highly respected reactors in the region. The designs and architecture of their residential projects are of international standards. We are known for ourcontemporary designs, innovation and completion of projects within the most stringent time schedule.</p>\n<p>Our clients come first. Our reliability gives us the confidence of offering an ostentatious service that involves: Constant communication with our customer.</p>\n<ul class=\"aboutul\">\n<li><i class=\"fa fa-angle-right\"></i>&nbsp;&nbsp;&nbsp;Efficient management</li>\n<li><i class=\"fa fa-angle-right\"></i>&nbsp;&nbsp;&nbsp;Attention to detail</li>\n<li><i class=\"fa fa-angle-right\"></i>&nbsp;&nbsp;&nbsp;Meticulous construction</li>\n<li><i class=\"fa fa-angle-right\"></i>&nbsp;&nbsp;&nbsp;Comfortable experience</li>\n<li><i class=\"fa fa-angle-right\"></i>&nbsp;&nbsp;&nbsp;Great financial value and increased property value</li>\n</ul>\n</div>\n</div>\n<div class=\"col-md-6 col-sm-6 col-xs-12\">\n<div class=\"about-post mobaboutus\"><img class=\"img-responsive\" src=\"/images/about_us2.jpg\" alt=\"\" />\n<h2>Why you should choose us</h2>\n<!--							<?php echo stripslashes($page_res[\'page_content\']); ?>  -->\n<ul class=\"aboutul\">\n<li><i class=\"fa fa-angle-right\"></i>&nbsp;&nbsp;&nbsp;Experience and expertise</li>\n<li><i class=\"fa fa-angle-right\"></i>&nbsp;&nbsp;&nbsp;On Time. On Budget. Guaranteed.</li>\n<li><i class=\"fa fa-angle-right\"></i>&nbsp;&nbsp;&nbsp;100% Workmanship Guarantee</li>\n<li><i class=\"fa fa-angle-right\"></i>&nbsp;&nbsp;&nbsp;Satisfied Customers</li>\n</ul>\n<p style=\"text-align: justify;\">We are extremely proud to have been involved in many building projects. We believe you are only as good as your last job. Our work history shows that 90% of our work comes from referrals of satisfied customers. This is the best compliment Ambika Group could receive and the results can be easily verified from our history. <br /><br /> Our mission is to be Surat&rsquo;s most recommended building company. This means you can expect the best to ensure we earn your valued recommendation.</p>\n</div>\n</div>', NULL, 'About Us', 'About Us', 'About Us', 1, 'E', 3, '2016-02-22 12:21:34', '2016-08-02 07:34:41', '123.201.2.173');

-- --------------------------------------------------------

--
-- Table structure for table `permission_master`
--

CREATE TABLE `permission_master` (
  `permission_id` int(20) NOT NULL,
  `username` varchar(100) NOT NULL,
  `createdate` date NOT NULL,
  `modifieddate` date NOT NULL,
  `group_id` varchar(20) NOT NULL,
  `module` varchar(20) NOT NULL,
  `permissions` varchar(7) NOT NULL DEFAULT 'a,e,d,v',
  `module_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `permission_master`
--

INSERT INTO `permission_master` (`permission_id`, `username`, `createdate`, `modifieddate`, `group_id`, `module`, `permissions`, `module_id`) VALUES
(228, 'Keyur', '2016-04-03', '2016-04-03', '1', 'gallery', 'a,d,e,v', 19),
(227, 'Keyur', '2016-04-03', '2016-04-03', '1', 'album', 'a,d,e,v', 18),
(226, 'Keyur', '2016-04-03', '2016-04-03', '1', 'albumtype', 'a,d,e,v', 17),
(225, 'Keyur', '2016-04-03', '2016-04-03', '1', 'testimonialtype', 'a,d,e,v', 16),
(221, 'Keyur', '2016-04-03', '2016-04-03', '1', 'website', 'a,d,e,v', 12),
(222, 'Keyur', '2016-04-03', '2016-04-03', '1', 'news', 'a,d,e,v', 13),
(223, 'Keyur', '2016-04-03', '2016-04-03', '1', 'newsmaster', 'a,d,e,v', 14),
(224, 'Keyur', '2016-04-03', '2016-04-03', '1', 'testimonial', 'a,d,e,v', 15),
(220, 'Keyur', '2016-04-03', '2016-04-03', '1', 'epanel', 'a,d,e,v', 11),
(219, 'Keyur', '2016-04-03', '2016-04-03', '1', 'feedback', 'a,d,e,v', 9),
(218, 'Keyur', '2016-04-03', '2016-04-03', '1', 'homecontent', 'a,d,e,v', 8),
(217, 'Keyur', '2016-04-03', '2016-04-03', '1', 'pageimages', 'a,d,e,v', 7),
(216, 'Keyur', '2016-04-03', '2016-04-03', '1', 'pages', 'a,d,e,v', 6),
(215, 'Keyur', '2016-04-03', '2016-04-03', '1', 'permission', 'a,d,e,v', 5),
(213, 'Keyur', '2016-04-03', '2016-04-03', '1', 'usergroup', 'a,d,e,v', 3),
(214, 'Keyur', '2016-04-03', '2016-04-03', '1', 'slider', 'a,d,e,v', 4),
(212, 'Keyur', '2016-04-03', '2016-04-03', '1', 'user', 'a,d,e,v', 2),
(211, 'Keyur', '2016-04-03', '2016-04-03', '1', 'contact', 'a,d,e,v', 1),
(229, 'Keyur', '2016-04-03', '2016-04-03', '1', 'subscription', 'a,d,e,v', 20),
(230, 'Keyur', '2016-04-03', '2016-04-03', '1', 'job', 'a,d,e,v', 21),
(231, 'Keyur', '2016-04-03', '2016-04-03', '1', 'product', 'a,d,e,v', 22);

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE `product` (
  `product_id` int(11) NOT NULL,
  `username` varchar(200) NOT NULL,
  `createdate` date NOT NULL,
  `modifieddate` date NOT NULL,
  `product_type` int(1) NOT NULL,
  `product_title` varchar(100) NOT NULL DEFAULT '',
  `product_tagline` text NOT NULL,
  `product_desc` text NOT NULL,
  `thumbnail_image` text NOT NULL,
  `image` text NOT NULL,
  `position` char(1) NOT NULL DEFAULT 'I',
  `eve_date` date NOT NULL,
  `remote_ip` varchar(50) NOT NULL,
  `status` char(1) NOT NULL DEFAULT 'E',
  `sortorder` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `productID` int(11) NOT NULL,
  `pTypeID` int(11) NOT NULL,
  `productTitle` varchar(100) NOT NULL,
  `productTypeTitle` varchar(50) DEFAULT NULL,
  `productDescr` text NOT NULL,
  `productStatus` varchar(100) NOT NULL,
  `productBS` varchar(100) NOT NULL,
  `productArea` varchar(200) DEFAULT NULL,
  `productUnits` varchar(100) NOT NULL,
  `ContactPerson` text NOT NULL,
  `productAbout` text,
  `productSpecification` text NOT NULL,
  `productPlans` text,
  `productBrochure` varchar(200) DEFAULT NULL,
  `productLocation` varchar(200) DEFAULT NULL,
  `productMap` varchar(255) DEFAULT NULL,
  `productThumbnail` varchar(100) DEFAULT NULL,
  `productSpeciality` text,
  `productVideo` text,
  `productVideoUrl` varchar(150) DEFAULT NULL,
  `productsUrl` varchar(200) DEFAULT NULL,
  `homepage` char(1) NOT NULL DEFAULT 'N',
  `createdate` date NOT NULL,
  `modifieddate` date NOT NULL,
  `username` varchar(20) NOT NULL,
  `sortorder` int(5) NOT NULL,
  `homesortorder` int(2) DEFAULT '0',
  `status` char(1) NOT NULL DEFAULT 'E',
  `remote_ip` varchar(20) NOT NULL,
  `hasForm` varchar(6) DEFAULT NULL,
  `productLocationThumbnail` varchar(256) NOT NULL,
  `menu` char(1) NOT NULL,
  `parallaximage` varchar(256) NOT NULL,
  `parallaxdesc` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`productID`, `pTypeID`, `productTitle`, `productTypeTitle`, `productDescr`, `productStatus`, `productBS`, `productArea`, `productUnits`, `ContactPerson`, `productAbout`, `productSpecification`, `productPlans`, `productBrochure`, `productLocation`, `productMap`, `productThumbnail`, `productSpeciality`, `productVideo`, `productVideoUrl`, `productsUrl`, `homepage`, `createdate`, `modifieddate`, `username`, `sortorder`, `homesortorder`, `status`, `remote_ip`, `hasForm`, `productLocationThumbnail`, `menu`, `parallaximage`, `parallaxdesc`) VALUES
(15, 6, 'Ambika Pinnacle Business Hub', 'Commercial', '<p style=\"text-align: justify;\">Ambika Pinnacle is an embodiment of extravagance life and style. The wonderful perspective of the city through the condo is a fortune to treasure for. The project is detached from the hassle and tensions of the external world. Carefully woven inside planning extras venerate the house, making it resemble a castle from inside. Everything is picked to meet the fabulous configuration prerequisite of the house, changing it from straightforward block and bond dividers to an asylum of extravagance and solace. World class installations are fitted in the inside and also outsides to suit the need of the customers. Ambika Pinnacle is similar to dwelling in the paradise.</p>', 'Ongoing', 'Open', 'Varachha, Surat', '1690 sq ft - 2260 sq ft', '', '<div class=\"row\">\r\n<div class=\"col-sm-12\">\r\n<div class=\"row\">\r\n<div class=\"col-sm-4\"><i class=\"fa fa-angle-double-right\"> </i>Children\'s play area<br /> <i class=\"fa fa-angle-double-right\"> </i>Skating Rink<br /> <i class=\"fa fa-angle-double-right\"> </i>Basket ball and tennis court to entertained and fit at the same time<br /> <i class=\"fa fa-angle-double-right\"> </i>Indoor Game Facility to give you enjoyment<br /> <i class=\"fa fa-angle-double-right\"> </i>Community Hall<br /> <i class=\"fa fa-angle-double-right\"> </i>Internal Roads<br /> <i class=\"fa fa-angle-double-right\"> </i>Spa<br /> <i class=\"fa fa-angle-double-right\"> </i>Power Backup<br /> <i class=\"fa fa-angle-double-right\"> </i>Gymnasium<br /> <i class=\"fa fa-angle-double-right\"> </i> Swimming Pool</div>\r\n</div>\r\n</div>\r\n</div>', '', 'Yes', 'brochure.pdf', '', 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3718.833779003439!2d72.88564431442036!3d21.23843898596413!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x0!2zMjHCsDE0JzE4LjQiTiA3MsKwNTMnMTYuMiJF!5e0!3m2!1sen!2sin!4v1470722485057', 'Project_thumb_inner_page.jpg', NULL, '', NULL, '', 'N', '2016-10-15', '2016-01-01', '', 0, 0, 'E', '123.201.2.173', 'Yes', 'Pinnacle_map_thumb.jpg', 'N', 'Ambika_Pinnacle.jpg', '<div class=\"font18-s\">Ambika Pinnacle is an embodiment of extravagance life and style.</div>\r\n<div class=\"maintit-s\">Ambika Pinnacle Business Hub</div>\r\n<p>Belong to Surat&rsquo;s most sought after locale, \'Ambika Pinnacle\' captures the essence of Surat.</p>'),
(10, 10, 'Ambika Dreams', 'Residential', '<p style=\"text-align: justify;\">Dreams by Ambika Group, located in Dindoli, Surat offers apartments, with the price being on request. Situated in suburb Surat South, this is one of the popular localities of the city.</p>', 'Completed', 'Closed', 'Dindoli, Surat', '655 sq ft -1270 sq ft', '', '<div class=\"row\">\r\n<div class=\"col-sm-12\">\r\n<div class=\"row\">\r\n<div class=\"col-sm-4\"><i class=\"fa fa-angle-double-right\"> </i>Pleasant seat-outs<br /> <i class=\"fa fa-angle-double-right\"> </i>Multipurpose Hall<br /> <i class=\"fa fa-angle-double-right\"> </i>Children Play Area<br /> <i class=\"fa fa-angle-double-right\"> </i>Landscape Garden<br /> <i class=\"fa fa-angle-double-right\"> </i>Sports Facilities</div>\r\n</div>\r\n</div>\r\n</div>', '', 'Yes', '', '', 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3720.9696521332858!2d72.88147031441902!3d21.153605988881537!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x0!2zMjHCsDA5JzEzLjAiTiA3MsKwNTMnMDEuMiJF!5e0!3m2!1sen!2sin!4v1470722357915', 'innerpagethumb.jpg', NULL, '', NULL, '', 'Y', '2016-08-09', '2016-01-01', '', 1, 3, 'E', '::1', 'No', 'map_thumb.jpg', 'N', 'Ambika_Dreams.jpg', '<div class=\"font18-s\">Ambika Dreams is a prime residential initiative by Ambika Group.</div>\r\n<div class=\"maintit-s\">Ambika Dreams</div>\r\n<p>Built to the standards of quality, comfort and refinement, \'Ambika Dreams\' offers luxurious living spaces.</p>'),
(12, 4, 'Ambika Heaven', 'Residential', '<p style=\"text-align: justify;\">Heaven, Dindoli Near Sagrampura, Surat is Precisely planned for the Modern day city dweller, A pollution free living environment and wide open areas that offer not just the luxury of space, but peace of mind as well. The complex is in close vicinity to Schools, Shopping, Hospitals and Entertainment , making it an ideal place to live .It offers 2BHK &amp; 3BHK Luxurious Apartments And Surrounded with most beautiful places.</p>', 'Ongoing', '', 'Dindoli , Surat', '2 BHK - 1,186 / 1,176 sq. ft , 3 BHK - 1,398 / 1,371sq. ft', '', '<div class=\"row\">\r\n<div class=\"col-sm-12\">\r\n<div class=\"row\">\r\n<div class=\"col-sm-4\"><i class=\"fa fa-angle-double-right\"> </i>Children\'s play area<br /> <i class=\"fa fa-angle-double-right\"> </i>Club House<br /> <i class=\"fa fa-angle-double-right\"> </i>Children Play Area<br /> <i class=\"fa fa-angle-double-right\"> </i>24 X 7 Security<br /> <i class=\"fa fa-angle-double-right\"> </i>Jogging Track<br /> <i class=\"fa fa-angle-double-right\"> </i>Landscaped Gardens<br /> <i class=\"fa fa-angle-double-right\"> </i>Swimming Pool<br /> <i class=\"fa fa-angle-double-right\"> </i>Power Backup<br /> <i class=\"fa fa-angle-double-right\"> </i>Sports Facility<br /> <i class=\"fa fa-angle-double-right\"> </i>Indoor Games</div>\r\n</div>\r\n</div>\r\n</div>', '', 'No', '', '', 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3720.9696521332858!2d72.88147031441902!3d21.153605988881537!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x0!2zMjHCsDA5JzEzLjAiTiA3MsKwNTMnMDEuMiJF!5e0!3m2!1sen!2sin!4v1470722357915', 'ambika-Heaven.jpg', NULL, '', NULL, '', 'Y', '2016-08-09', '2016-01-01', '', 2, 4, 'E', '::1', 'No', 'ambikaheaven_mapthumb.jpg', 'N', 'Ambika_Heaven.jpg', '<div class=\"font18-s\">When you reside at the Ambika Heaven, you surely live life large</div>\r\n<div class=\"maintit-s\">Ambika Heaven</div>\r\n<p>It is a dream that is being put together of an inward gated township.</p>'),
(16, 4, 'Ambika Solitaire', 'Residential', '<p style=\"text-align: justify;\">Ambika Solitaire is Precisely planned for the Modern day city dweller, A pollution free living environment and wide open areas that offer not just the luxury of space, but peace of mind as well.</p>', 'Ongoing', 'Open', 'Mota Varachha, Surat', '3 BHK - 2,765 sq. ft , 4 BHK - 3,880 sq. ft ', '', '', '', 'Yes', 'ambikasolitaire_brochure.pdf', '', 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3718.9942706196925!2d72.8615006144203!3d21.232075786183394!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3be04f3b5181e5bb%3A0xaf14e45e85b3d5a4!2sAmbika+Solitaire!5e0!3m2!1sen!2sin!4v1470397110026', 'ambika-solitaire.jpg', NULL, '<iframe width=\"640\" height=\"360\" src=\"https://www.youtube.com/embed/5lQxZ1-kLdI\" frameborder=\"0\" allowfullscreen></iframe>', NULL, '', 'Y', '2016-08-08', '2016-01-01', '', 1, 6, 'E', '::1', 'No', 'solitaire_map_thumb.jpg', 'Y', 'Ambika_Solitarie.jpg', '<div class=\"font18-s\">Ambika Solitaire is an Ideal space to harmonize and revitalise minds.</div>\r\n<div class=\"maintit-s\">Ambika Solitaire</div>\r\n<p>Expecting and getting nothing short of world-class conveniences and lifestyle your traditions.</p>'),
(17, 3, 'Ambika Township', 'Residential', '<p style=\"text-align: justify;\">Ambika Township is one of the popular residential developments in Dindoli, neighborhood of Surat. It is among the completed projects of its Builder. It has lavish yet thoughtfully designed residences.</p>', 'Completed', 'Closed', 'Dindoli, Surat', '600 sq.ft', '', '', '', 'No', '', '', NULL, 'innerpagethumb.jpg', NULL, '', NULL, '', 'N', '2016-08-05', '2016-01-01', '', 4, 0, 'E', '123.201.2.173', 'Yes', '', 'N', 'Ambika_township.jpg', '<div class=\"font18-s\">A township that follows the walk to work concept of residential spaces.</div>\r\n<div class=\"maintit-s\">Ambika Township</div>\r\n<p>Ambika Township, A world where quality amenities meet international work spaces.</p>'),
(13, 3, 'Ambika Heights', 'Residential', '<p style=\"text-align: justify;\">Ambika Heights is one of the popular residential developments in Sagrampura, neighborhood of Surat. It is among the completed projects of its Builder. It has lavish yet thoughtfully designed residences.</p>', 'Completed', 'Closed', 'Ring Road , Surat', '2 BHK - 1,200 sq. ft , 3 BHK - 1,500 sq. ft', '', '', '', 'No', '', '', 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3720.4066220184263!2d72.87722131441937!3d21.175999988112427!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x0!2zMjHCsDEwJzMzLjYiTiA3MsKwNTInNDUuOSJF!5e0!3m2!1sen!2sin!4v1470722753409', 'innerpagethumb.jpg', NULL, '', NULL, '', 'Y', '2016-08-09', '2016-01-01', '', 3, 1, 'E', '::1', 'No', '', 'N', 'Ambika_Heights.jpg', '<div class=\"font18-s\">Ambika Heights, Blessed with modern amenities and spectacular architecture</div>\r\n<div class=\"maintit-s\">Ambika Heights</div>\r\n<p>Ambika Heights ensures that all spaces are utilized and serve a livable benefit to the household.</p>'),
(14, 4, 'Ambika Pinnacle Residency', 'Residential', '<p style=\"text-align: justify;\">Ambika Pinnacle is an embodiment of extravagance life and style. The wonderful perspective of the city through the condo is a fortune to treasure for. The project is detached from the hassle and tensions of the external world. Carefully woven inside planning extras venerate the house, making it resemble a castle from inside. Everything is picked to meet the fabulous configuration prerequisite of the house, changing it from straightforward block and bond dividers to an asylum of extravagance and solace. World class installations are fitted in the inside and also outsides to suit the need of the customers. Ambika Pinnacle is similar to dwelling in the paradise.</p>', 'Ongoing', 'Open', 'Varachha, Surat', '3 BHK - 2,150 sq. ft , 4 BHK - 2,600 sq. ft', '', '<div class=\"row\">\r\n<div class=\"col-sm-12\">\r\n<div class=\"row\">\r\n<div class=\"col-sm-4\"><i class=\"fa fa-angle-double-right\"> </i>Children\'s play area<br /> <i class=\"fa fa-angle-double-right\"> </i>Skating Rink<br /> <i class=\"fa fa-angle-double-right\"> </i>Basket ball and tennis court to entertained and fit at the same time<br /> <i class=\"fa fa-angle-double-right\"> </i>Indoor Game Facility to give you enjoyment<br /> <i class=\"fa fa-angle-double-right\"> </i>Community Hall<br /> <i class=\"fa fa-angle-double-right\"> </i>Internal Roads<br /> <i class=\"fa fa-angle-double-right\"> </i>Spa<br /> <i class=\"fa fa-angle-double-right\"> </i>Power Backup<br /> <i class=\"fa fa-angle-double-right\"> </i>Gymnasium<br /> <i class=\"fa fa-angle-double-right\"> </i> Swimming Pool</div>\r\n</div>\r\n</div>\r\n</div>', '', 'Yes', 'brochure.pdf', '', 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3718.833779003439!2d72.88564431442036!3d21.23843898596413!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x0!2zMjHCsDE0JzE4LjQiTiA3MsKwNTMnMTYuMiJF!5e0!3m2!1sen!2sin!4v1470722485057', 'innerpagethumb.jpg', NULL, '<iframe width=\"640\" height=\"360\" src=\"https://www.youtube.com/embed/PJggJCgVyLo\" frameborder=\"0\" allowfullscreen></iframe>', NULL, '', 'Y', '2016-08-20', '2016-01-01', '', 2, 5, 'E', '123.201.2.173', 'No', 'Pinnacle_map_thumb.jpg', 'N', 'parallax.jpg', '<div class=\"font18-s\">Ambika Pinnacle is an embodiment of extravagance life and style.</div>\r\n<div class=\"maintit-s\">Ambika Pinnacle Residency</div>\r\n<p>Built to the standards of quality, comfort and refinement, Ambika Pinnacle offers luxurious living spaces.</p>'),
(18, 3, 'Ambika Residency', 'Residential', '<p>Ambika Residency</p>', 'Completed', 'Closed', '', '', '', '', '', 'No', '', '', '', '', NULL, '', NULL, '', 'N', '2016-08-09', '2016-01-01', '', 3, 0, 'D', '::1', 'Yes', '', 'N', 'p-2.jpg', ''),
(19, 3, 'Ambika Row House', 'Residential', '', 'Completed', 'Closed', '', '', '', '', '', 'Yes', '', '', '', '', NULL, '', NULL, '', 'N', '2016-08-09', '2016-01-01', '', 3, 0, 'D', '::1', 'Yes', '', 'N', 'p-1.jpg', '');

-- --------------------------------------------------------

--
-- Table structure for table `producttype`
--

CREATE TABLE `producttype` (
  `pTypeID` int(11) NOT NULL,
  `pTypeTitle` varchar(50) NOT NULL,
  `pTypeDescr` text,
  `pTypeParent` int(11) NOT NULL DEFAULT '0',
  `createdate` date NOT NULL,
  `modifieddate` date NOT NULL,
  `username` varchar(20) NOT NULL,
  `sortorder` int(5) NOT NULL,
  `status` char(1) NOT NULL DEFAULT 'E',
  `HasProject` char(1) NOT NULL DEFAULT 'N',
  `remote_ip` varchar(20) NOT NULL,
  `projectFile` varchar(150) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `producttype`
--

INSERT INTO `producttype` (`pTypeID`, `pTypeTitle`, `pTypeDescr`, `pTypeParent`, `createdate`, `modifieddate`, `username`, `sortorder`, `status`, `HasProject`, `remote_ip`, `projectFile`) VALUES
(1, 'Residential', '', 0, '0000-00-00', '0000-00-00', 'Keyur', 1, 'E', 'N', '::1', ''),
(2, 'Commercial', '', 0, '2016-04-06', '0000-00-00', '', 2, 'E', 'N', '::1', ''),
(3, 'Completed', '', 1, '2016-04-06', '0000-00-00', '', 3, 'E', 'Y', '::1', ''),
(4, 'OnGoing', '', 1, '0000-00-00', '0000-00-00', 'Keyur', 2, 'E', 'Y', '127.0.0.1', ''),
(5, 'Completed', '', 2, '2016-04-06', '0000-00-00', '', 3, 'D', 'Y', '::1', ''),
(6, 'OnGoing', '', 2, '0000-00-00', '0000-00-00', 'Keyur', 2, 'E', 'Y', '127.0.0.1', ''),
(7, 'Upcoming', 'Upcoming', 2, '0000-00-00', '0000-00-00', 'Keyur', 1, 'D', 'Y', '127.0.0.1', ''),
(8, 'Hospitality', '', 0, '0000-00-00', '0000-00-00', 'Keyur', 3, 'D', 'N', '127.0.0.1', ''),
(9, 'Ongoing', 'Ongoing', 8, '0000-00-00', '0000-00-00', 'Keyur', 1, 'D', 'Y', '192.168.1.122', ''),
(10, 'Upcoming', '', 1, '2016-08-08', '2016-08-08', '', 1, 'E', 'Y', '::1', '');

-- --------------------------------------------------------

--
-- Table structure for table `seo_link_master`
--

CREATE TABLE `seo_link_master` (
  `seo_link_id` int(8) NOT NULL,
  `module_name` varchar(100) NOT NULL,
  `module_id` int(8) NOT NULL,
  `seo_slug` varchar(255) NOT NULL,
  `user_id` int(8) NOT NULL,
  `createdate` datetime NOT NULL,
  `modifieddate` datetime NOT NULL,
  `remote_ip` varchar(100) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `seo_link_master`
--

INSERT INTO `seo_link_master` (`seo_link_id`, `module_name`, `module_id`, `seo_slug`, `user_id`, `createdate`, `modifieddate`, `remote_ip`) VALUES
(1, 'pages', 10, 'about-us', 3, '2016-04-01 07:26:27', '2016-08-02 10:27:37', '127.0.0.1'),
(2, 'pages', 11, 'vision', 3, '2016-04-01 07:34:35', '2016-04-11 18:18:53', '127.0.0.1'),
(3, 'pages', 12, 'mission', 3, '2016-04-01 07:35:29', '2016-04-11 18:20:38', '127.0.0.1'),
(4, 'News/news', 6, 'ambika-dreams', 3, '2016-04-03 13:55:44', '2016-07-30 17:50:05', '127.0.0.1'),
(5, 'News/news', 7, 'ambika-heights', 3, '2016-04-03 13:56:03', '2016-07-30 17:48:53', '127.0.0.1'),
(19, 'projects/commercial', 5, 'completed', 3, '2016-04-06 06:07:20', '2016-04-06 06:07:20', '::1'),
(18, 'projects/residential', 4, 'ongoing', 3, '2016-04-06 06:07:03', '2016-08-01 11:42:03', '127.0.0.1'),
(17, 'projects/residential', 3, 'completed', 3, '2016-04-06 06:06:48', '2016-04-06 06:06:48', '::1'),
(16, 'projects', 2, 'commercial', 3, '2016-04-06 06:06:26', '2016-04-06 06:06:26', '::1'),
(15, 'projects', 1, 'residential', 3, '2016-04-06 05:59:41', '2016-04-06 05:59:47', '::1'),
(20, 'projects/commercial', 6, 'ongoing', 3, '2016-04-06 06:07:36', '2016-08-01 11:42:15', '127.0.0.1'),
(26, 'projects/commercial/on-going', 5, 'valentina-business-hub', 3, '2016-04-11 15:11:13', '2016-04-11 15:27:43', '127.0.0.1'),
(27, 'projects/commercial/on-going', 6, 'velly-belly', 3, '2016-04-11 15:33:48', '2016-04-11 15:34:39', '127.0.0.1'),
(28, 'projects/commercial', 7, 'upcoming', 3, '2016-04-11 15:56:37', '2016-04-11 16:17:17', '127.0.0.1'),
(24, 'News/news', 5, 'ambika-pinnacle', 3, '2016-04-09 12:31:54', '2016-07-30 17:50:47', '127.0.0.1'),
(41, 'projects/residential/completed', 10, 'ambika-dreams', 3, '2016-07-29 17:24:03', '2016-08-05 12:46:03', '123.201.2.173'),
(29, 'projects/commercial/upcoming', 7, 'v-square', 3, '2016-04-11 15:58:16', '2016-04-12 19:39:13', '127.0.0.1'),
(30, 'projects/commercial/upcoming', 8, 'v3-corner', 3, '2016-04-11 16:05:52', '2016-04-12 13:47:04', '127.0.0.1'),
(31, 'projects/commercial/ongoing', 6, 'velly-belly', 3, '2016-04-11 16:55:39', '2016-04-11 16:58:05', '127.0.0.1'),
(32, 'projects', 8, 'hospitality', 3, '2016-04-11 16:59:05', '2016-04-11 17:13:17', '127.0.0.1'),
(33, 'projects/hospitality', 9, 'ongoing', 3, '2016-04-11 17:01:07', '2016-07-29 16:56:46', '192.168.1.122'),
(34, 'projects/hospitality/ongoing', 6, 'velly-belly', 3, '2016-04-11 17:02:01', '2016-04-12 15:08:53', '127.0.0.1'),
(47, 'projects/residential/on-going', 16, 'ambika-solitaire', 3, '2016-07-29 19:21:13', '2016-07-30 17:30:50', '127.0.0.1'),
(36, 'gallery', 8, 'ambika-group-insight', 3, '2016-04-12 11:54:10', '2016-07-30 16:24:41', '127.0.0.1'),
(37, 'gallery', 9, 'festival-gallery', 3, '2016-04-12 11:55:09', '2016-10-01 07:41:34', '103.69.200.47'),
(38, 'gallery', 10, 'site-updates', 3, '2016-04-12 11:55:25', '2016-04-12 11:55:25', '127.0.0.1'),
(46, 'projects/commercial/ongoing', 15, 'ambika-pinnacle-business-hub', 3, '2016-07-29 19:03:39', '2016-10-15 09:12:05', '123.201.2.173'),
(40, 'News/news', 8, 'ambika-solitaire', 3, '2016-07-29 15:58:13', '2016-07-30 17:49:26', '127.0.0.1'),
(43, 'projects/residential/completed', 12, 'ambika-heaven', 3, '2016-07-29 17:46:42', '2016-08-06 08:14:38', '123.201.2.173'),
(44, 'projects/residential/completed', 13, 'ambika-heights', 3, '2016-07-29 18:30:32', '2016-08-09 11:36:27', '::1'),
(45, 'projects/residential/on-going', 14, 'ambika-pinnacle', 3, '2016-07-29 18:37:20', '2016-07-30 17:45:12', '127.0.0.1'),
(48, 'projects/residential/completed', 17, 'ambika-township', 3, '2016-07-29 19:31:04', '2016-08-05 12:46:18', '123.201.2.173'),
(49, 'projects/residential/completed', 18, 'ambika-residency', 3, '2016-08-01 16:20:36', '2016-08-09 11:26:33', '::1'),
(50, 'projects/residential/completed', 19, 'ambika-row-house', 3, '2016-08-01 16:23:56', '2016-08-09 11:26:48', '::1'),
(51, 'projects/residential/completed', 20, 'dssd', 3, '2016-08-01 16:24:46', '2016-08-01 16:25:19', '127.0.0.1'),
(52, 'projects/residential/completed', 22, 'xzzx', 3, '2016-08-01 16:34:22', '2016-08-01 16:36:20', '127.0.0.1'),
(53, 'projects/residential/completed', 23, 'aesha', 3, '2016-08-01 16:43:29', '2016-08-01 16:54:34', '127.0.0.1'),
(54, 'projects/residential/ongoing', 14, 'ambika-pinnacle-residency', 3, '2016-08-01 18:23:17', '2016-08-20 07:06:05', '123.201.2.173'),
(55, 'projects/residential/ongoing', 16, 'ambika-solitaire', 3, '2016-08-01 18:23:29', '2016-08-08 18:12:03', '::1'),
(56, 'projects/residential', 10, 'upcoming', 3, '2016-08-08 17:25:27', '2016-08-08 17:25:27', '::1'),
(57, 'projects/residential/upcoming', 10, 'ambika-dreams', 3, '2016-08-08 17:26:08', '2016-08-09 13:45:24', '::1'),
(61, 'gallery', 11, 'press-ad-amp-hoarding', 3, '2016-08-09 10:36:49', '2016-08-09 10:36:49', '::1'),
(58, 'projects/residential/ongoing', 12, 'ambika-heaven', 3, '2016-08-08 17:28:51', '2016-08-09 11:29:53', '::1'),
(59, 'projects/residential/completed', 18, 'ambika-residency', 3, '2016-08-08 17:33:08', '2016-08-09 11:26:33', '::1'),
(60, 'projects/residential/completed', 19, 'ambika-row-house', 3, '2016-08-08 17:33:48', '2016-08-09 11:26:48', '::1');

-- --------------------------------------------------------

--
-- Table structure for table `session_log_master`
--

CREATE TABLE `session_log_master` (
  `session_log_id` bigint(11) NOT NULL,
  `user_id` bigint(11) NOT NULL,
  `loginID` varchar(100) NOT NULL,
  `remote_ip` varchar(100) NOT NULL,
  `last_access` datetime NOT NULL,
  `status` char(1) NOT NULL COMMENT 'LogiIn(I) / Logout (O)'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `session_log_master`
--

INSERT INTO `session_log_master` (`session_log_id`, `user_id`, `loginID`, `remote_ip`, `last_access`, `status`) VALUES
(1, 3, 'admin-ambikagroup', '::1', '2016-07-09 23:18:46', 'I'),
(2, 3, 'admin-ambikagroup', '::1', '2016-07-17 16:05:58', 'I'),
(3, 3, 'admin-ambikagroup', '127.0.0.1', '2016-07-29 13:37:45', 'I'),
(4, 3, 'admin-ambikagroup', '127.0.0.1', '2016-07-29 15:18:49', 'I'),
(5, 3, 'admin-ambikagroup', '192.168.1.122', '2016-07-29 16:51:57', 'I'),
(6, 3, 'admin-ambikagroup', '127.0.0.1', '2016-07-29 17:25:47', 'I'),
(7, 3, 'admin-ambikagroup', '192.168.1.122', '2016-07-29 17:26:05', 'I'),
(8, 3, 'admin-ambikagroup', '127.0.0.1', '2016-07-30 11:00:22', 'I'),
(9, 3, 'admin-ambikagroup', '127.0.0.1', '2016-07-30 14:13:25', 'I'),
(10, 3, 'admin-ambikagroup', '127.0.0.1', '2016-08-01 11:41:47', 'I'),
(11, 3, 'admin-ambikagroup', '127.0.0.1', '2016-08-01 14:51:07', 'I'),
(12, 3, 'admin-ambikagroup', '127.0.0.1', '2016-08-01 23:00:02', 'I'),
(13, 3, 'admin-ambikagroup', '127.0.0.1', '2016-08-02 10:27:05', 'I'),
(14, 3, 'admin-ambikagroup', '123.201.2.173', '2016-08-05 12:21:29', 'I'),
(15, 3, 'admin-ambikagroup', '123.201.2.173', '2016-08-06 08:10:39', 'I'),
(16, 3, 'admin-ambikagroup', '::1', '2016-08-09 10:20:06', 'I'),
(17, 3, 'admin-ambikagroup', '123.201.2.173', '2016-08-20 07:00:43', 'I'),
(18, 3, 'admin-ambikagroup', '123.201.2.173', '2016-08-20 07:02:20', 'I'),
(19, 3, 'admin-ambikagroup', '123.201.2.173', '2016-08-30 12:15:48', 'I'),
(20, 3, 'admin-ambikagroup', '123.201.2.173', '2016-08-31 15:35:43', 'I'),
(21, 3, 'admin-ambikagroup', '103.69.200.1', '2016-09-01 12:44:41', 'I'),
(22, 3, 'admin-ambikagroup', '103.69.200.1', '2016-09-01 13:06:10', 'O'),
(23, 3, 'admin-ambikagroup', '103.69.200.1', '2016-09-01 13:12:49', 'I'),
(24, 3, 'admin-ambikagroup', '103.69.200.1', '2016-09-01 13:14:21', 'O'),
(25, 3, 'admin-ambikagroup', '123.201.2.173', '2016-09-01 14:53:38', 'I'),
(26, 3, 'admin-ambikagroup', '123.201.2.173', '2016-09-01 15:45:40', 'I'),
(27, 3, 'admin-ambikagroup', '103.69.200.1', '2016-09-02 14:07:25', 'I'),
(28, 3, 'admin-ambikagroup', '103.69.200.1', '2016-09-02 14:10:27', 'O'),
(29, 3, 'admin-ambikagroup', '103.69.200.1', '2016-09-03 10:27:04', 'I'),
(30, 3, 'admin-ambikagroup', '103.69.200.1', '2016-09-03 10:40:39', 'O'),
(31, 3, 'admin-ambikagroup', '103.69.200.47', '2016-10-01 07:37:48', 'I'),
(32, 3, 'admin-ambikagroup', '103.69.200.47', '2016-10-01 07:58:32', 'O'),
(33, 3, 'admin-ambikagroup', '123.201.2.173', '2016-10-03 07:13:43', 'I'),
(34, 3, 'admin-ambikagroup', '123.201.2.173', '2016-10-11 14:36:27', 'I'),
(35, 3, 'admin-ambikagroup', '123.201.2.173', '2016-10-15 08:08:40', 'I'),
(36, 3, 'admin-ambikagroup', '123.201.2.173', '2016-10-16 07:29:39', 'I'),
(37, 3, 'admin-ambikagroup', '123.201.2.173', '2017-05-06 10:56:42', 'I'),
(38, 3, 'admin-ambikagroup', '223.196.70.6', '2017-07-13 12:50:50', 'I'),
(39, 3, 'admin-ambikagroup', '223.196.70.6', '2017-07-13 13:13:10', 'O'),
(40, 3, 'admin-ambikagroup', '49.34.127.28', '2017-07-14 17:57:55', 'I'),
(41, 3, 'admin-ambikagroup', '49.34.127.28', '2017-07-14 17:58:13', 'I'),
(42, 3, 'admin-ambikagroup', '123.201.2.173', '2017-07-17 06:24:43', 'I'),
(43, 3, 'admin-ambikagroup', '123.201.2.173', '2017-07-17 09:05:59', 'I'),
(44, 3, 'admin-ambikagroup', '123.201.2.173', '2017-07-17 12:46:26', 'I'),
(45, 3, 'admin-ambikagroup', '123.201.2.173', '2017-07-17 13:53:33', 'I'),
(46, 3, 'admin-ambikagroup', '49.34.180.237', '2017-11-16 04:22:47', 'I'),
(47, 3, 'admin-ambikagroup', '49.34.180.237', '2017-11-16 04:25:48', 'O');

-- --------------------------------------------------------

--
-- Table structure for table `sliderimage`
--

CREATE TABLE `sliderimage` (
  `sliderID` int(11) NOT NULL,
  `image` varchar(255) NOT NULL,
  `sliderTitle` varchar(255) NOT NULL,
  `username` varchar(30) NOT NULL,
  `createdate` date NOT NULL,
  `modifieddate` date NOT NULL,
  `sortorder` int(8) NOT NULL,
  `status` char(1) NOT NULL DEFAULT 'E',
  `sliderPosition` char(1) NOT NULL DEFAULT 'H',
  `remote_ip` varchar(100) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sliderimage`
--

INSERT INTO `sliderimage` (`sliderID`, `image`, `sliderTitle`, `username`, `createdate`, `modifieddate`, `sortorder`, `status`, `sliderPosition`, `remote_ip`) VALUES
(15, '15-slider1.jpg', 'Ambika Group Slider 2', 'Keyur', '2016-07-29', '2016-08-09', 2, 'E', 'H', '::1'),
(14, '14-slider2.jpg', 'Ambika Group Slider 1', 'Keyur', '2016-07-29', '2016-08-09', 3, 'E', 'H', '::1'),
(16, '16-slider5.jpg', 'Ambika Group Slider 3', 'Keyur', '2016-07-29', '2016-08-09', 1, 'E', 'H', '::1'),
(17, '17-slider3.jpg', 'Ambika Group Slider 4', 'Keyur', '2016-08-09', '2016-08-09', 4, 'E', 'H', '::1'),
(18, '18-slider4.jpg', 'Ambika Group Slider 5', 'Keyur', '2016-08-09', '2016-08-09', 5, 'E', 'H', '::1');

-- --------------------------------------------------------

--
-- Table structure for table `subscription_master`
--

CREATE TABLE `subscription_master` (
  `s_id` int(11) NOT NULL,
  `s_name` varchar(100) DEFAULT NULL,
  `s_email` varchar(200) DEFAULT NULL,
  `s_mobile` varchar(20) DEFAULT NULL,
  `s_date` date NOT NULL,
  `new` int(1) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `subscription_master`
--

INSERT INTO `subscription_master` (`s_id`, `s_name`, `s_email`, `s_mobile`, `s_date`, `new`) VALUES
(25, 'Zil Mehta', 'zilmehta07@gmail.com', '4345345', '2016-04-09', 1);

-- --------------------------------------------------------

--
-- Table structure for table `template_master`
--

CREATE TABLE `template_master` (
  `template_id` int(11) NOT NULL,
  `template_name` varchar(100) NOT NULL,
  `template_title` varchar(100) NOT NULL,
  `sortorder` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `template_master`
--

INSERT INTO `template_master` (`template_id`, `template_name`, `template_title`, `sortorder`) VALUES
(1, 'inner_page.tpl.php', 'Inner Page Template', 1),
(3, 'full_width_page.tpl.php', 'full_width_page', 0);

-- --------------------------------------------------------

--
-- Table structure for table `testimonial`
--

CREATE TABLE `testimonial` (
  `testimonial_Id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `designation` varchar(255) DEFAULT NULL,
  `photo` text,
  `review` text NOT NULL,
  `username` varchar(30) DEFAULT NULL,
  `createdate` date DEFAULT NULL,
  `modifieddate` date DEFAULT NULL,
  `sortorder` int(8) DEFAULT NULL,
  `status` char(1) NOT NULL DEFAULT 'E',
  `remote_ip` varchar(100) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `testimonial`
--

INSERT INTO `testimonial` (`testimonial_Id`, `name`, `designation`, `photo`, `review`, `username`, `createdate`, `modifieddate`, `sortorder`, `status`, `remote_ip`) VALUES
(1, 'Ambika Heaven', 'Indian Film Actress', '1-Bipasha-Basu_3.jpg', '<p>Ambika Group completed Ambika Heaven , a residential project in Surat </p>', 'Keyur', '2015-10-27', '2016-07-30', 2, 'E', '127.0.0.1'),
(11, 'Ambika Heights', 'Indian Film Actress', '1-Bipasha-Basu_3.jpg', '<p>Ambika Group completed Ambika Heights , a residential project in Surat </p>', 'Keyur', '2015-10-27', '2016-07-30', 3, 'E', '127.0.0.1'),
(12, 'Ambika Dreams', 'Indian Film Actress', '1-Bipasha-Basu_3.jpg', '<p>Ambika Group completed Ambika Dreams , a residential project in Surat </p>', 'Keyur', '2015-10-27', '2016-07-30', 1, 'E', '127.0.0.1');

-- --------------------------------------------------------

--
-- Table structure for table `testimonial_master`
--

CREATE TABLE `testimonial_master` (
  `testimonial_master_id` int(11) NOT NULL,
  `testimonial_type` varchar(50) NOT NULL,
  `username` varchar(30) NOT NULL,
  `createdate` date NOT NULL,
  `modifieddate` date NOT NULL,
  `sortorder` int(11) NOT NULL,
  `status` char(1) NOT NULL DEFAULT 'E',
  `remote_ip` varchar(100) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `testimonial_master`
--

INSERT INTO `testimonial_master` (`testimonial_master_id`, `testimonial_type`, `username`, `createdate`, `modifieddate`, `sortorder`, `status`, `remote_ip`) VALUES
(1, 'Testimonial', 'Keyur', '2015-10-27', '2015-10-27', 1, 'E', '192.168.2.106');

-- --------------------------------------------------------

--
-- Table structure for table `user_master`
--

CREATE TABLE `user_master` (
  `userID` int(11) NOT NULL,
  `group_id` int(11) NOT NULL DEFAULT '0',
  `loginID` varchar(100) NOT NULL,
  `email` varchar(50) NOT NULL DEFAULT '',
  `password` varchar(255) NOT NULL DEFAULT '',
  `firstname` varchar(50) NOT NULL DEFAULT '',
  `lastname` varchar(50) NOT NULL DEFAULT '',
  `address` text NOT NULL,
  `contacts` varchar(255) NOT NULL DEFAULT ''
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_master`
--

INSERT INTO `user_master` (`userID`, `group_id`, `loginID`, `email`, `password`, `firstname`, `lastname`, `address`, `contacts`) VALUES
(3, 1, 'admin-ambikagroup', 'contact@easternts.com', 'c974d68e08fba6aef21cbb052588a80ced02c012423305e785a745d5e45cd6b6', 'Keyur', 'Mehta', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `website_master`
--

CREATE TABLE `website_master` (
  `id` int(11) NOT NULL,
  `email1` varchar(255) NOT NULL,
  `email2` varchar(255) NOT NULL,
  `tel1` varchar(25) NOT NULL,
  `tel2` varchar(100) NOT NULL,
  `address` text NOT NULL,
  `fax` varchar(25) NOT NULL,
  `work1` text NOT NULL,
  `work2` text NOT NULL,
  `map_code` text NOT NULL,
  `logo` text NOT NULL,
  `social` text NOT NULL,
  `copyright` varchar(70) NOT NULL,
  `rera` text NOT NULL,
  `modal_rera` varchar(2) NOT NULL,
  `powered_by` text NOT NULL,
  `coming_soon` varchar(1) NOT NULL DEFAULT 'N',
  `username` varchar(70) NOT NULL,
  `remote_ip` varchar(70) NOT NULL,
  `create_date` date NOT NULL,
  `modified_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `website_master`
--

INSERT INTO `website_master` (`id`, `email1`, `email2`, `tel1`, `tel2`, `address`, `fax`, `work1`, `work2`, `map_code`, `logo`, `social`, `copyright`, `rera`, `modal_rera`, `powered_by`, `coming_soon`, `username`, `remote_ip`, `create_date`, `modified_date`) VALUES
(1, 'info@ambikagroup.com', '', '+91 7096963000', '+91 9879618227', '<p>Ambika Group - VIP circle,<br /> Near Kapodara Bridge,<br /> Utran, Mota Varachha.</p>', '', '10:00 am to 11:00 pm', '11:00 am to 11:30 pm', '', 'logo.png', 'a:6:{s:8:\"facebook\";s:23:\"http://www.facebook.com\";s:7:\"twitter\";s:22:\"http://www.twitter.com\";s:6:\"google\";s:21:\"http://www.google.com\";s:9:\"instagram\";s:0:\"\";s:7:\"youtube\";s:22:\"http://www.youtube.com\";s:9:\"pinterest\";s:0:\"\";}', 'Ambika Group', '<p class=\"text-justify\">This disclaimer (\"Disclaimer\") will be applicable to the Website. By using or accessing the Website you agree with the Disclaimer without any qualification or limitation. The Company reserves the right to add, alter or delete material from the Website at any time and may, at any time, revise these Terms without notifying you. You are bound by any such amendments and the Company therefore advise that you periodically visit this page to review the current Terms. The Websites and all its content are provided with all faults on an \"as is\" and \"as available\" basis. No information given under this Website creates a warranty or expand the scope of any warranty that cannot be disclaimed under applicable law. Your use of the Website is solely at your own risk. This website is for guidance only. It does not constitute part of an offer or contract. Design &amp; specifications are subject to change without prior notice. Computer generated images are the artist\'s impression and are an indicative of the actual designs.</p>\r\n<p class=\"text-justify\">The particulars contained on the mentions details of the Projects/developments undertaken by the Company including depicting banners/posters of the Project. The contents are being modified in terms of the stipulations / recommendations under the Real Estate Regulation Act, 2016 and Rules made thereunder (\"RERA\") and accordingly may not be fully in line thereof as of date. You are therefore required to verify all the details, including area, amenities, services, terms of sales and payments and other relevant terms independently with the sales team/ company prior to concluding any decision for buying any unit(s) in any of the said projects. Till such time the details are fully updated, the said information will not be construed as an advertisement. To find out more about a project / development, please telephone our sales centres or visit our sales office during opening hours and speak to one of our sales staff. In no event will the Company be liable for claim made by the users including seeking any cancellation for any of the inaccuracies in the information provided in this Website, though all efforts have to be made to ensure accuracy. The Company will no circumstance will be liable for any expense, loss or damage including, without limitation, indirect or consequential loss or damage, or any expense, loss or damage whatsoever arising from use, or loss of use, of data, arising out of or in connection with the use of this website.</p>', 'E', 'a:2:{s:5:\"title\";s:24:\"Eastern Techno Solutions\";s:4:\"link\";s:25:\"http://www.easternts.com/\";}', '', 'Keyur', '123.201.2.173', '2015-10-26', '2017-07-17');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `album`
--
ALTER TABLE `album`
  ADD PRIMARY KEY (`a_id`);

--
-- Indexes for table `album_type`
--
ALTER TABLE `album_type`
  ADD PRIMARY KEY (`type_id`);

--
-- Indexes for table `contact_master`
--
ALTER TABLE `contact_master`
  ADD PRIMARY KEY (`cid`);

--
-- Indexes for table `download_brochure`
--
ALTER TABLE `download_brochure`
  ADD PRIMARY KEY (`b_id`);

--
-- Indexes for table `gallery`
--
ALTER TABLE `gallery`
  ADD PRIMARY KEY (`galleryID`);

--
-- Indexes for table `gallery_master`
--
ALTER TABLE `gallery_master`
  ADD PRIMARY KEY (`image_id`);

--
-- Indexes for table `group_master`
--
ALTER TABLE `group_master`
  ADD PRIMARY KEY (`group_id`);

--
-- Indexes for table `homecontent`
--
ALTER TABLE `homecontent`
  ADD PRIMARY KEY (`content_id`);

--
-- Indexes for table `item`
--
ALTER TABLE `item`
  ADD PRIMARY KEY (`item_id`);

--
-- Indexes for table `job_master`
--
ALTER TABLE `job_master`
  ADD PRIMARY KEY (`job_id`);

--
-- Indexes for table `module_master`
--
ALTER TABLE `module_master`
  ADD PRIMARY KEY (`module_id`);

--
-- Indexes for table `news`
--
ALTER TABLE `news`
  ADD PRIMARY KEY (`news_id`),
  ADD KEY `eve_date` (`eve_date`);

--
-- Indexes for table `news_type`
--
ALTER TABLE `news_type`
  ADD PRIMARY KEY (`news_type_id`);

--
-- Indexes for table `page_master`
--
ALTER TABLE `page_master`
  ADD PRIMARY KEY (`page_id`);

--
-- Indexes for table `permission_master`
--
ALTER TABLE `permission_master`
  ADD PRIMARY KEY (`permission_id`);

--
-- Indexes for table `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`product_id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`productID`),
  ADD KEY `pTypeID` (`pTypeID`),
  ADD KEY `pTypeID_2` (`pTypeID`);

--
-- Indexes for table `producttype`
--
ALTER TABLE `producttype`
  ADD PRIMARY KEY (`pTypeID`);

--
-- Indexes for table `seo_link_master`
--
ALTER TABLE `seo_link_master`
  ADD PRIMARY KEY (`seo_link_id`);

--
-- Indexes for table `session_log_master`
--
ALTER TABLE `session_log_master`
  ADD PRIMARY KEY (`session_log_id`);

--
-- Indexes for table `sliderimage`
--
ALTER TABLE `sliderimage`
  ADD PRIMARY KEY (`sliderID`);

--
-- Indexes for table `subscription_master`
--
ALTER TABLE `subscription_master`
  ADD PRIMARY KEY (`s_id`);

--
-- Indexes for table `template_master`
--
ALTER TABLE `template_master`
  ADD PRIMARY KEY (`template_id`);

--
-- Indexes for table `testimonial`
--
ALTER TABLE `testimonial`
  ADD PRIMARY KEY (`testimonial_Id`);

--
-- Indexes for table `testimonial_master`
--
ALTER TABLE `testimonial_master`
  ADD PRIMARY KEY (`testimonial_master_id`);

--
-- Indexes for table `user_master`
--
ALTER TABLE `user_master`
  ADD PRIMARY KEY (`userID`);

--
-- Indexes for table `website_master`
--
ALTER TABLE `website_master`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `album`
--
ALTER TABLE `album`
  MODIFY `a_id` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `album_type`
--
ALTER TABLE `album_type`
  MODIFY `type_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `contact_master`
--
ALTER TABLE `contact_master`
  MODIFY `cid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=65;

--
-- AUTO_INCREMENT for table `download_brochure`
--
ALTER TABLE `download_brochure`
  MODIFY `b_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=332;

--
-- AUTO_INCREMENT for table `gallery`
--
ALTER TABLE `gallery`
  MODIFY `galleryID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=362;

--
-- AUTO_INCREMENT for table `gallery_master`
--
ALTER TABLE `gallery_master`
  MODIFY `image_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT for table `group_master`
--
ALTER TABLE `group_master`
  MODIFY `group_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `homecontent`
--
ALTER TABLE `homecontent`
  MODIFY `content_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `item`
--
ALTER TABLE `item`
  MODIFY `item_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `job_master`
--
ALTER TABLE `job_master`
  MODIFY `job_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=642;

--
-- AUTO_INCREMENT for table `module_master`
--
ALTER TABLE `module_master`
  MODIFY `module_id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `news`
--
ALTER TABLE `news`
  MODIFY `news_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `news_type`
--
ALTER TABLE `news_type`
  MODIFY `news_type_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `page_master`
--
ALTER TABLE `page_master`
  MODIFY `page_id` bigint(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `permission_master`
--
ALTER TABLE `permission_master`
  MODIFY `permission_id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=232;

--
-- AUTO_INCREMENT for table `product`
--
ALTER TABLE `product`
  MODIFY `product_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `productID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `producttype`
--
ALTER TABLE `producttype`
  MODIFY `pTypeID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `seo_link_master`
--
ALTER TABLE `seo_link_master`
  MODIFY `seo_link_id` int(8) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=62;

--
-- AUTO_INCREMENT for table `session_log_master`
--
ALTER TABLE `session_log_master`
  MODIFY `session_log_id` bigint(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=48;

--
-- AUTO_INCREMENT for table `sliderimage`
--
ALTER TABLE `sliderimage`
  MODIFY `sliderID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `subscription_master`
--
ALTER TABLE `subscription_master`
  MODIFY `s_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT for table `template_master`
--
ALTER TABLE `template_master`
  MODIFY `template_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `testimonial`
--
ALTER TABLE `testimonial`
  MODIFY `testimonial_Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `testimonial_master`
--
ALTER TABLE `testimonial_master`
  MODIFY `testimonial_master_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `user_master`
--
ALTER TABLE `user_master`
  MODIFY `userID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `website_master`
--
ALTER TABLE `website_master`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
