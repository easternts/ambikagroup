<?php
	include "inc/mainapp.php";
	include "inc/content_function.php" ;
	switch($module){
		case 'success-stories':
			$pageTitle="Success Stories";
			$title="Success Stories".SITENAME;
			$keyword = $title;
			$description = $title;
			$content_include = "content/success-stories.php";
			include DIR_WS_TEMPLATE."testimonial_page.tpl.php";
			break;
		case 'testimonials':
			$pageTitle="Testimonials";
			$title= "Testimonials".SITENAME ;
			$keyword = $title;
			$description = $title;
			$content_include = "content/testimonials.php";
			include DIR_WS_TEMPLATE."testimonial_page.tpl.php";
			break;					
		case 'gallery':
			$pageTitle = "Photo Gallery";
			$title = "Gallery".SITENAME;
			$keyword = $title;
			$description = $title;
			$content_include = "content/gallery.php";
			include DIR_WS_TEMPLATE."gallery_page.tpl.php";
			break;
		case 'allprojects':
			$pageTitle="All Projects";
			$title="All Projects".SITENAME;
			$keyword = $title;
			$description = $title;
			$content_include = "content/all_projects.php";
			include DIR_WS_TEMPLATE."all_projects_page.tpl.php";
			break;
        case 'site-gallery':
                $title="Site Gallery ".SITENAME;
                $template = "project_site.tpl.php";
				$content_include = "content/site-gallery.php";
                $keyword = $title;
			    $description = $title;
			    include DIR_WS_TEMPLATE.$template;
            break;
        case 'campaign-gallery':
                $title="Brand Campaign Gallery ".SITENAME;
                $template = "project_site.tpl.php";
				$content_include = "content/campaign-gallery.php";
                $keyword = $title;
			    $description = $title;
			    include DIR_WS_TEMPLATE.$template;
            break;
		case 'projects':
			 
			if(isset($module_id) && $module_id > 0){
				
				if($pathcnt > 3){
					$page_res = get_record_from_db('products','productID',$module_id);
					$productType = getProductType($page_res['pTypeID']);
					
					$ptype_res = get_record_from_db('producttype','pTypeID',$page_res['pTypeID']);
					$ParentType = getProductType($ptype_res['pTypeParent']);
					
					$pageTitle = $page_res['productTitle'];
					$burl = HTTP_SERVER.WS_ROOT.get_projectseo_url($page_res['pTypeID'],pro_SeoSlug($productType));
					$bTitle = $ParentType." - ".$productType;
					$title = "Projects - ".$productType." - ".$page_res['productTitle'].SITENAME;
					$template = "project_detail_page.tpl.php";
					$content_include = "content/project_detail.php";
				}else{
					
					$page_res = get_record_from_db('producttype','pTypeID',$module_id);
					$ParentType = getProductType($page_res['pTypeParent']);
					$pageTitle = "Projects - ".$ParentType." - ".$page_res['pTypeTitle'];
					$title = "Projects - ".$ParentType." - ".$page_res['pTypeTitle'].SITENAME;
					$template = "projects_page.tpl.php";
					$content_include = "content/projects.php";
				}
			}else{
				$pageTitle = "Our Projects";
				$title = "Our Projects - ".SITENAME;
				$template = "projects_page.tpl.php";
				$content_include = "content/projects.php";
			}
			$keyword = $title;
			$description = $title;
			$news_type = $module."/".$url_slug;
			
			include DIR_WS_TEMPLATE.$template;
			break;
		
				
		case 'News':
			
			$pageTitle = "Latest News";
			$title = "News".SITENAME;
			$keyword = $title;
			$description = $title;
			$news_type = $module."/".$url_slug;
			$content_include = "content/news.php";
			include DIR_WS_TEMPLATE."news_page.tpl.php";
			break;
			
		case 'project':
			$pageTitle="Project";
			$title="Project".SITENAME;
			$keyword = $title;
			$description = $title;
			$content_include = "content/productList.php";
			include DIR_WS_TEMPLATE."project_page.tpl.php";
			break;
		case 'projectdetails':
			$pageTitle="project Details";
			$title="project Details".SITENAME;
			$keyword = $title;
			$description = $title;
			$content_include = "content/productDetails.php";
			include DIR_WS_TEMPLATE."project_page.tpl.php";
			break;
		case 'contact-us':
			$pageTitle="Contact Us". ' - Ambika Group | Real Estate | Construction |  Surat | Gujarat | India';
			$title="Contact Us".SITENAME;
			$keyword = $title;
			$description = $title;
			$bg = "images/contactus.jpg";
			$content_include = "content/contact_us.php";
			include DIR_WS_TEMPLATE."form_page.tpl.php";
			break;
		
		case 'career':
			$pageTitle="Career";
			$title="Career With Us". ' - Ambika Group | Real Estate | Construction |  Surat | Gujarat | India';
			$keyword = $title . '- Ambika Group | Real Estate | Construction |  Surat | Gujarat | India';
			$description = $title . '- Ambika Group | Real Estate | Construction |  Surat | Gujarat | India';
			$bg = "images/career.jpg";
			$content_include = "content/career.php";
			include DIR_WS_TEMPLATE."career_page.tpl.php";
			break;
		case 'pages':
		
			$page_res = get_record_from_db('page_master','page_id',$module_id);
			
			$title = $page_res['meta_title'].SITENAME;
			$keyword = $page_res['meta_keyword'];
			$description = $page_res['meta_desc'];
			$pageTitle = $page_res['page_title'];
			$pageContent = $page_res['page_content'];
			
			if($page_res['parent_id'] == 1){
				
				include DIR_WS_TEMPLATE.$page_res['page_template'];
				break;
			}else{
				$content_include = "content/about.php";
				include DIR_WS_TEMPLATE.'about_page.tpl.php';
				break;				
			}
			break;						
		case 'home':
		default:
			$page = "Ambika Group | Real Estate | Construction | Surat | Gujarat | India";
			$keyword = "Ambika Group | Real Estate | Construction | Surat | Gujarat | India";
			$description = "Ambika Group is One of the Top Builders and Real estate Developers in Surat. Ambika Group is Surat's premier real estate developer having a range of commericial and residential real estate projects.";
			$content_include = "content/index.php";
			$pageTitle = "Ambika Group | Real Estate | Construction | Surat | Gujarat | India";
			$title = "Ambika Group is One of the Top Builders and Real estate Developers in Surat. Ambika Group is Surat's premier real estate developer having a range of commericial and residential real estate projects.";
			include DIR_WS_TEMPLATE."main_page.tpl.php";	
			break;
	}	
	
?>
