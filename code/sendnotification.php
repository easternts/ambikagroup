<?php
include 'inc/config.php';

$apiKey = "AIzaSyCjEU12Y2et7v4d-f4Scxz2J68dvdmDCDE";

$message=array();
$fields = array();
$i=0;
$cnt = 0;
$devices = array('APA91bFjDMREDJhx2T5VVkFUrIC6naCf1Yi-0FbVrDMB7HBpCgs7FmVhIbiMUkofGhiTQm90qv_5ntEk82CdJerpoB12hOzc3fyGWXRrRyl-M59IM_5QH3DdWlc4RwvSvsPvbdmRQWQdKs2beM5hI8MnmJ8JacsjRQ');

$message = 'Hello';

$gcpm = new GCMPushMessage($apiKey);
$gcpm->setDevices($devices);

$response = $gcpm->send($message, array('notId' => 1, 'Title' => $message,'notification_url'=>'#/notification'));
print_r($response);
	

/*
	Class to send push notifications using Google Cloud Messaging for Android

	Example usage
	-----------------------
	$an = new GCMPushMessage($apiKey);
	$an->setDevices($devices);
	$response = $an->send($message);
	-----------------------
	
	$apiKey Your GCM api key
	$devices An array or string of registered device tokens
	$message The mesasge you want to push out

	@author Matt Grundy

	Adapted from the code available at:
	http://stackoverflow.com/questions/11242743/gcm-with-php-google-cloud-messaging

*/
class GCMPushMessage {

	var $url = 'https://android.googleapis.com/gcm/send';
	var $serverApiKey = "";
	var $devices = array();
	
	/*
		Constructor
		@param $apiKeyIn the server API key
	*/
	function GCMPushMessage($apiKeyIn){
		$this->serverApiKey = $apiKeyIn;
	}

	/*
		Set the devices to send to
		@param $deviceIds array of device tokens to send to
	*/
	function setDevices($deviceIds){
	
		if(is_array($deviceIds)){
			$this->devices = $deviceIds;
		} else {
			$this->devices = array($deviceIds);
		}
	
	}

	/*
		Send the message to the device
		@param $message The message to send
		@param $data Array of data to accompany the message
	*/
	function send($message, $data = false){
		
		if(!is_array($this->devices) || count($this->devices) == 0){
			$this->error("No devices set");
		}
		
		if(strlen($this->serverApiKey) < 8){
			$this->error("Server API Key not set");
		}
		
		$fields = array(
			'registration_ids'  => $this->devices,
			'data'              => array( "message" => $message ,"title" => "Hindva Latest Updates" ),
			'notification_key'  => time()
		);
		
		if(is_array($data)){
			foreach ($data as $key => $value) {
				$fields['data'][$key] = $value;
			}
		}
		print_r($fields);
		$headers = array( 
			'Authorization: key=' . $this->serverApiKey,
			'Content-Type: application/json'
		);
		
			
		$ch = curl_init();
		
		curl_setopt( $ch, CURLOPT_URL, $this->url );
		curl_setopt( $ch, CURLOPT_VERBOSE, true);
		curl_setopt( $ch, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4); 
		curl_setopt( $ch, CURLOPT_POST, true );
		curl_setopt( $ch, CURLOPT_HTTPHEADER, $headers);
		curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
		
		curl_setopt( $ch, CURLOPT_POSTFIELDS, json_encode( $fields ) );
		curl_setopt( $ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt( $ch, CURLOPT_SSL_VERIFYHOST, false);
		
		
		
		$result = curl_exec($ch);

		if ($result === FALSE) {
                die('Curl failed: ' . curl_error($ch));
          }
		curl_close($ch);
		
		return $result;

	}
	
	function error($msg){
		echo "Android send notification failed with error:";
		echo "\t" . $msg;
		exit(1);
	}
}


?>

