 <link rel="shortcut icon" href="favicon.ico">
<div class="top-line">
	<div class="container">
		<div class="row">
			<div class="col-md-8 col-sm-9">
				<ul class="info-list">
					<li>
						<i class="fa fa-phone"></i>
						Call us:
						<span><a href="tel:+917096963000" class="inherit">+91 7096963000</a> | <a href="tel:+919879618227" class="inherit">+91 9879618227</a></span>
					</li>
					<li>
						<i class="fa fa-envelope-o"></i>
						Email us:
                        <span><a href="mailto:info@ambikagroupsurat.com" class="inherit">info@ambikagroupsurat.com</a></span>
					</li>
				</ul>
			</div>	
			<div class="col-md-4 col-sm-3">
				<ul class="social-icons">
					<li><a class="facebook" href="https://www.facebook.com/ambikagroupsurat/?fref=ts" target="_blank"><i class="fa fa-facebook"></i></a></li>
                    <li><a class="instagram" href="https://www.instagram.com/ambikagroup/?hl=en" target="_blank"><i class="fa fa-instagram"></i></a></li>
					<li><a class="twitter" href="https://twitter.com/AmbikaInfo" target="_blank"><i class="fa fa-twitter"></i></a></li>
					<!--<li><a class="google" href="#" target="_blank"><i class="fa fa-google-plus"></i></a></li>-->
					<li><a class="linkedin" href="https://www.linkedin.com/nhome/" target="_blank"><i class="fa fa-linkedin"></i></a></li>
					<li><a class="pinterest" href="https://www.pinterest.com/ambika_group/" target="_blank"><i class="fa fa-pinterest"></i></a></li>
				</ul>
			</div>	
		</div>
	</div>
</div>