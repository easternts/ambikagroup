<?php
	$web_arr = get_website_details();
?>

<div id="modal-rera" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="true" >
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Disclaimer</h4>
      </div>
      <div class="modal-body">
        <?php 
             echo stripslashes($web_arr['rera']);
        ?>
      </div>
      <div class="modal-footer" style="text-align:center;">
        <button type="button" class="btn btn-default" id="rera_agree">I Agree</button>
      </div>
    </div>

  </div>
</div>
<script type="text/javascript">
	$(document).ready(function(e){
	     if(typeof(Cookies.get('rera')) == 'undefined'){
			$('#modal-rera').modal('show');
		 }
	});

    $('#rera_agree').on('click', function () {
        Cookies.set('rera', '1');
		$('#modal-rera').modal('hide');
    })

</script>