
<div class="footer-last">
	<div class="container">
		<p class="copyright text-center">
            <?php
           $powered  =  $web_arr['powered_by']; 
            ?>
			&copy; <?php echo  date('Y')  . "&nbsp;"  .  $web_arr['copyright']; ?>. All Rights Reserved. | Powered By : <a target="_blank" class="w" title="Eastern Techno Solutions" href="<?php echo $powered['link']; ?>"><?php echo $powered['title']; ?></a>
		</p>
	</div>
</div>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-33999805-63', 'auto');
  ga('send', 'pageview');

</script>