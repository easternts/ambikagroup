<?php
$web_arr = get_website_details();
?>
<div class="container">
	<div class="navbar-header">
		<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
			<span class="sr-only">Toggle navigation</span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
		</button>
		<a class="navbar-brand" href="<?php echo HTTP_SERVER.WS_ROOT ;?>"><img src="<?php echo $web_arr['logo']; ?>" alt=""></a>
	</div>
	<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
		<ul class="nav mega navbar-nav pull-right">
			<li><a href="<?php echo HTTP_SERVER.WS_ROOT ;?>">Home</a></li>
			<li><a href="<?php echo get_pageseo_url('10','pages');?>">About</a></li>
			<li class="dropdown main mega-fw-s"><a href="#" data-toggle="dropdown" class="dropdown-toggle js-activated">PROJECTS</a>
              <ul class="dropdown-menu">
                <li>
                  <div class="mega-content">
                  <div class="row">
									<div class="nav-column col-lg-4 col-md-4 col-sm-4 col-xs-12">
										<?php 
										$ptSql1 = mysql_query("select * from products where status = 'E' and menu = 'Y' order by productID desc LIMIT 1") or die(mysql_error());
										$ptrs1 = mysql_fetch_array($ptSql1);
										$url2 = HTTP_SERVER.WS_ROOT.get_projectseo_url($ptrs1['productID'],pro_SeoSlug($ptrs1['productTitle']));
										$image = "timthumb.php?src=".DIR_WS_PROJECT_PATH.$ptrs1['productID']."/".$ptrs1 ['productThumbnail']."&w=200&h=120&zc=0";
										?>	
										<a><span class="menutitle"><?php echo $ptrs1['productTitle'];?></span></a>
											<img src="<?php echo $image;?>" class="img-responsive" alt="<?php echo $ptrs1['productTitle'];?>">
											<a class="imgmenu" href="<?php echo  $url2 ;?>" class=""> - More Details</a>
										</div>
								<?php
									$ptSql = mysql_query("select * from producttype where pTypeParent = 0 and status = 'E' order by sortorder") or die(mysql_error());
									while($ptrs = mysql_fetch_array($ptSql)) {
									$url1 = HTTP_SERVER.WS_ROOT.get_projectseo_url($ptrs['pTypeID'],pro_SeoSlug($ptrs['pTypeTitle']));
									
									echo '<div class="pj nav-column col-lg-4 col-md-4 col-sm-4 col-xs-6">
											<span class="menutitle">'.$ptrs['pTypeTitle'].'</span>
										<ul class="menu">
									';
									$menusql = mysql_query("Select * from producttype where pTypeParent = '".$ptrs['pTypeID']."' and status = 'E' order by sortorder") or die();
										while($rs = mysql_fetch_array($menusql)){
										$url = HTTP_SERVER.WS_ROOT.get_projectseo_url($rs['pTypeID'],pro_SeoSlug($rs['pTypeTitle']));
										echo '<li><a class="subtit" href="'.$url.'" title="'.$rs['pTypeTitle'].'"><i class="fa fa-angle-right"></i> '.$rs['pTypeTitle'].'</a></li>';
										}
									echo '</ul></div>';
									}
									 ?>
                                </div>
                  </div>
                </li>
              </ul>
            </li>
			<li class="dropdown"><a role="button">Media</a>
				<ul class="dropdown-menu">
					<li><a href="<?php echo HTTP_SERVER.WS_ROOT ;?>News/" class="transition"><i class="fa fa-angle-right"></i> Latest News</a></li>
					<li><a href="<?php echo HTTP_SERVER.WS_ROOT ;?>success-stories/" class="transition"><i class="fa fa-angle-right"></i> Success Stories</a></li>
					<li> <a href="<?php echo HTTP_SERVER.WS_ROOT ;?>gallery/all/"><i class="fa fa-angle-right"></i> Photo Gallery</a></li>
				</ul>
			</li>
			<li><a href="<?php echo HTTP_SERVER.WS_ROOT ;?>career/" class="transition">CAREER</a></li>
			<li><a href="<?php echo HTTP_SERVER.WS_ROOT ;?>contact-us/" class="transition">CONTACT US</a></li>	
		</ul>
	</div>
</div>