<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
	<div class="contact-box widget">
	   <h3>More Questions?</h3>
	   <i class="fa fa-envelope"></i>
	   <p>Let us know your question via the contact form.</p>
	</div>
	<div class="contact-box widget">
	   <h3>Business Hour</h3>
	   <i class="fa fa-clock-o"> </i>
	   <ul>
		  <li>Monday - Friday 9am to 5pm </li>
		  <li>Saturday - 9am to 2pm</li>
		  <li>Sunday - Closed</li>
	   </ul>
	</div>
 </div>
 