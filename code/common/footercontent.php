<div class="footer-top">
	<div class="container">
		<div class="row">
			<div class="col-sm-12 col-xs-12 col-sm-12">
				<img src="images/footerlogo.png" class="img-responsive center-block">										
					<div class="social">
						<a class="facebook" href="https://www.facebook.com/ambikagroupsurat/?fref=ts" target="_blank"><i class="fa fa-facebook"></i></a>
						<a class="instagram" href="https://www.instagram.com/ambikagroup/?hl=en" target="_blank"><i class="fa fa-instagram"></i></a>
						<a class="twitter" href="https://twitter.com/AmbikaInfo" target="_blank"><i class="fa fa-twitter"></i></a>
						<!--<a class="google" href="#"><i class="fa fa-google-plus"></i></a>-->
						<a class="linkedin" href="https://www.linkedin.com/in/ambika-group-119b93126" target="_blank"><i class="fa fa-linkedin"></i></a>
                        <a class="pinterest" href="https://www.pinterest.com/ambika_group/" target="_blank"><i class="fa fa-pinterest"></i></a>
					</div>
			</div>
		</div>
	</div>
</div>