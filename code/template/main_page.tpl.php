<!doctype html>
<html lang="en" class="no-js">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title><?php echo $title; ?></title>
	<meta name="keywords" content="<?php echo $keyword; ?>">
	<meta name="description" content="<?php echo $description; ?>">
	<base href="<?php echo HTTP_SERVER.WS_ROOT ;?>">
	<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">	
	<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">	
	<link rel="stylesheet" type="text/css" href="css/jquery.bxslider.css">
	<link rel="stylesheet" type="text/css" href="css/owl.carousel.css">
    <link rel="stylesheet" type="text/css" href="css/owl.theme.css">
	<link rel="stylesheet" type="text/css" href="css/font-awesome.css">
    <!-- REVOLUTION BANNER CSS SETTINGS -->
	<link rel="stylesheet" type="text/css" href="css/settings.css"/>
	<link rel="stylesheet" type="text/css" href="css/style.css">
	<link rel="stylesheet" type="text/css" href="css/nivo-slider.css" rel="stylesheet">
	<script src="js/modernizr.custom.js"></script>
</head>
<body>
      <div class="se-pre-con">
    </div>

	<!-- Container -->
	<div id="container">
		<!-- Header
		    ================================================== -->
		<header class="clearfix hidden-xs">
			<nav class="navbar navbar-default navbar-fixed-top" role="navigation">
				<?php include "common/top_bar.php"; ?>	
				<?php include "common/header.php"; ?>	
			</nav>
		</header>
		<div class="navbar visible-xs">
			<!-- offcanvas-trigger-effects -->
			<div id="home">
				<?php include "common/mobilemenu.php"; ?>	
				<a class="mobile-logo visible-xs" href="<?php echo HTTP_SERVER.WS_ROOT ;?>"><img src="images/logo.png" height="50"></a>
			</div>
			<!-- offcanvas-trigger-effects -->
		</div>
		<!-- End Header -->

		<!-- home-section 
			================================================== -->
		<section id="home-section" class="slider1">
			<!--Main Slider-->
			<div class="">    	
				<?php include "common/slider.php"; ?>	
			</div>
		</section>
		<!-- End home section -->

		<!-- banner-section 
			================================================== -->
		<section class="banner-section">
			<div class="container">
                
				<h2 class="font18">AMBIKA SOLITAIRE - The 1st Tallest 18 Storey Residential Project of Mota Varachha<a href="<?php echo get_pageseo_url('16','projects/residential/on-going')?>" class="button-one">Read More</a></h2>
			</div>
		</section>
		<!-- End banner section -->

		<!-- portfolio-section 
			================================================== -->
		<section class="portfolio-section">
			<div class="container">
				<div class="col-sm-12">
					<h2 class="title-bod">Latest&nbsp;&nbsp;Projects</h2>
				</div>	
				<div class="single_p_w">
					<?php
					$ptSql1 = mysql_query("select * from products where homepage ='Y' order by homesortorder desc LIMIT 6") or die(mysql_error());
						while($ptrs1 = mysql_fetch_array($ptSql1)){
						$url2 = HTTP_SERVER.WS_ROOT.get_projectseo_url($ptrs1['productID'],pro_SeoSlug($ptrs1['productTitle']));

						$image= "timthumb.php?src=".DIR_WS_PROJECT_PATH.$ptrs1['productID']."/".$ptrs1['productThumbnail']."&w=400&h=250&a=tl";
					?>
					<div class="col-sm-6 col-md-4 col-xs-12">
						<div class="single_p_img">
							<img src="<?php echo $image;?>" alt="" class="img-responsive">
							<div class="single_p_hov_c">
										<div class="clearfix"></div>
										<div class="single_p_info">
											<!--<h6>2, 2.5, 3 &amp; 3.5 BHK Apartments </h6>-->
											<h6 class="units"><?php echo $ptrs1['productUnits']; ?></h6>
										</div>
										<div class="single_btm">
											<div class="pull-left">
                                                <!--
												<a class="btn_norm single_enq popmake-popup-property-list formidable_active" title="Enquiry Now" href="#" type="button" data-toggle="modal" data-target="#myModal"><i class="fa fa-envelope-"></i></a>
                                                -->
											</div>
											<div class="pull-right">
												<a title="Know More" class="btn_norm single_know" href="<?php echo  $url2 ;?>">Know More</a>
											</div>
										</div>
								</div>
						</div>
						<div class="projecttitle home-projecttitle">
							<span><?php echo $ptrs1['productTitle']; ?></span> | &nbsp;<?php echo $ptrs1['productArea']; ?>
						</div>
					</div>
					<?php } ?>
				</div>
			</div>
		</section>
		<!-- End portfolio section -->

		<!-- tabs-section 
			================================================== -->
		<section class="tabs-section">
			<div class="container">
				<div class="row">
					<div class="col-md-7 col-sm-6">
						<div class="about-us-box">
							<h1>about us and our priorities</h1>
							<p style="color:#fff;">We as a group aim to be expert in developing projects that are technologically complex and exhibit quality infrastructure. We are one of the highly respected reactors in the region. </p>
							<div class="row">
								<div class="col-md-6">
									<div class="about-us-post">
										<a href="#"><i class="fa fa-building-o"></i></a>
										<h2>Construction</h2>
										<span>Build Homes</span>
									</div>
									<div class="about-us-post">
										<a href="#"><i class="fa fa-cogs"></i></a>
										<h2>Maintanance</h2>
										<span>Energy Repair</span>
									</div>
									<div class="about-us-post">
										<a href="#"><i class="fa fa-desktop"></i></a>
										<h2>Good Planning</h2>
										<span>Architecture</span>
									</div>
								</div>
								<div class="col-md-6">
									<div class="about-us-post">
										<a href="#"><i class="fa fa-desktop"></i></a>
										<h2>Good Planning</h2>
										<span>Architecture</span>
									</div>
									<div class="about-us-post">
										<a href="#"><i class="fa fa-users"></i></a>
										<h2>Awesome Stuff</h2>
										<span>1000+ workers</span>
									</div>
									<div class="about-us-post">
										<a href="#"><i class="fa fa-building-o"></i></a>
										<h2>Construction</h2>
										<span>Build homes</span>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-5 col-sm-6">
						<div class="about-box mobabout">
							
				            <h1 class="homeabout">Who we are</h1>
							<p style="text-align:justify;">The designs and architecture of our residential projects are of international standards. We are known for our contemporary designs, innovation and completion of projects within the most stringent time schedule. </p>
							<p style="text-align:justify;">For over last three decades we remain committed in delivering the highest quality living space solutions, and are creating the most efficient facilities for our clients, from the conception of a project through construction, operations and procurement.</p>
                            <p style="text-align:justify;">Our aim is to provide premium housing and commercials to our clients at par with international standards so as to provide benchmark quality standards and a luxurious lifestyle at an affordable rate.</p>
                            <img src="images/credai.jpg" alt="">
						</div>
					</div>
				</div>
			</div>
		</section>
		<!-- End tabs section -->

		<!-- news-section 
			================================================== -->
		<section class="news-section">
			<div class="container">
				<div class="news-box">	
                    <h1>Latest News</h1>
                    <?php
		$selectsql = "select * from news where news_type = '1' and status = 'E' order by sortorder desc" ;
		$selectqry = mysql_query($selectsql) or die(mysql_error());
		if(mysql_num_rows($selectqry) > 0){
			while($result = mysql_fetch_assoc($selectqry)) {
			?>
					<div class="col-md-3 col-sm-6 col-xs-12 padleft0">
						<div class="news-post">
					
							<div class="news-content">
								<h2><a href="<?php echo HTTP_SERVER.WS_ROOT ;?>News/"><?php echo $result['news_title']; ?> - <?php echo date('d-m-Y',strtotime($result['eve_date'])); ?></a></h2>
								<p><?php echo stripslashes($result['news_desc']); ?></p>
								<a href="<?php echo HTTP_SERVER.WS_ROOT ;?>News/">Read More <i class="fa fa-angle-right"></i></a>
							</div>
						</div>
					</div>
                    <?php }
        }
                    ?>

				</div>
			</div>
		</section>
		<!-- End news section -->

		<!-- testimonial-section 
			================================================== -->
		<section class="testimonial-section">
			<div class="container">
				<div class="testimonial-box">
					<ul class="bxslider">
                        <?php
	$sql ="select * from testimonial where status = 'E' order by sortorder desc";
	$sqlquery = mysql_query($sql);         
	while($rs = mysql_fetch_array($sqlquery)) {
?>
						<li>
							<h2>Success Stories</h2>
						  <span><?php echo $rs['name']; ?></span>
							<p><?php echo $rs['review'];?></p>
						</li>
<!--
						<li>
							<h2>Besim Dauti</h2>
							<span>Project Menager</span>
							<p>Donec nec justo eget felis facilisis fermentum. Aliquam porttitor mauris sit amet orci. Aenean dignissim pellentesque felis. Morbi in sem quis dui placerat ornare. Pellentesque odio nisi, euismod in, pharetra a, ultricies in, diam. Sed arcu. Cras consequat.</p>
						</li>
						<li>
							<h2>Quan Ngyen</h2>
							<span>Electricity Engineer</span>
							<p>Donec nec justo eget felis facilisis fermentum. Aliquam porttitor mauris sit amet orci. Aenean dignissim pellentesque felis. Morbi in sem quis dui placerat ornare. Pellentesque odio nisi, euismod in, pharetra a, ultricies in, diam. Sed arcu. Cras consequat.</p>
						</li>
-->
                        <?php } ?>
					</ul>
				</div>

			</div>
		</section>
		<!-- End testimonial section -->

		<!-- footer 
			================================================== -->
		<footer>
			<?php include "common/footercontent.php"; ?>	
			<?php include "common/footer.php"; ?>
		</footer>
	</div>
	<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		  <div class="modal-dialog" role="document">
			<div class="modal-content">
			  <div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel">Enquire Now</h4>
			  </div>
			  <div class="modal-body">
				<form id="contact-form">
					<div class="row">
						<div class="col-md-6">
							<input name="name" id="name" type="text" placeholder="Name">
						</div>
						<div class="col-md-6">
							<input name="mail" id="mail" type="text" placeholder="Email">
						</div>
					</div>
					<div class="row">
						<div class="col-md-6">
							<input name="tel-number" id="tel-number" type="text" placeholder="Phone">
						</div>
						<div class="col-md-6">
							<input name="tel-number" id="tel-number" type="text" placeholder="Subject">
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<select data-reqmsg="This field cannot be blank." data-frmval="Looking For" id="field_qlsd8x" name="item_meta[254]">
								<option selected="selected" value="Looking For">Looking For</option>
									<option value="Purchase">Purchase</option>
									<option value="Lease">Lease</option>
							</select>
						</div>
					</div>
					<textarea name="comment" id="comment" placeholder="Message"></textarea>
					<input type="submit" id="submit_contact" value="Send Message">
				</form>
			  </div>
			</div>
		  </div>
	</div>
	<!-- End Container -->
	<script type="text/javascript" src="js/jquery.min.js"></script>
	<script type="text/javascript" src="js/jquery.migrate.js"></script>
	<script type="text/javascript" src="js/jquery.bxslider.min.js"></script>
	<script type="text/javascript" src="js/owl.carousel.min.js"></script>
	<script type="text/javascript" src="js/bootstrap.min.js"></script>
	<script type="text/javascript" src="js/jquery.imagesloaded.min.js"></script>
	<script type="text/javascript" src="js/retina-1.1.0.min.js"></script>
	<script type="text/javascript" src="js/script.js"></script>
	<script type="text/javascript" src="js/menu.js"></script>
	<script type="text/javascript" src="js/jquery.nivo.slider.js"></script> 
	<script type="text/javascript">
		$('#slider').nivoSlider({
			effect: 'fade',               // Specify sets like: 'fold,fade,sliceDown'
			slices: 15,                     // For slice animations
			boxCols: 8,                     // For box animations
			boxRows: 4,                     // For box animations
			controlNav:false,
			pauseOnHover: false     
		});
	</script>
	<script src="js/jquery.dlmenu.js"></script>
	
	<!-- Cookie for home popup -->
	<script src="js/js.cookie.js"></script>
	<!-- Cookie for home popup end -->
	
	<script>
		$(function() {
			$( '#dl-menu' ).dlmenu();
		});
	</script>
	
	<?php
		if($web_arr['modal_rera'] == 'E'){
			include "common/home_rera.php";
		} 
	?>
	
	
</body>
</html>