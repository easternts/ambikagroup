<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<!-- Responsive -->
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
	<title><?php echo $title; ?></title>
	<meta name="keywords" content="<?php echo $keyword; ?>">
	<meta name="description" content="<?php echo $description; ?>">
	<base href="<?php echo HTTP_SERVER.WS_ROOT ;?>">
	<!-- Stylesheets -->
	<link href="css/bootstrap.css" rel="stylesheet">
	<link href="css/revolution-slider.css" rel="stylesheet">
	<link href="css/style.css" rel="stylesheet">
	<link href="css/responsive.css" rel="stylesheet">
	<script src="js/modernizr.custom.js"></script>
	<!--[if lt IE 9]><script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
	<!--[if lt IE 9]><script src="js/respond.js"></script><![endif]-->
</head>
<body>
      <div class="se-pre-con">
    </div>
<div class="page-wrapper">
 	
    <!-- Preloader -->
    <div class="preloader"></div>
 	<div class="top-bar hidden-xs">
		<?php include "common/top_bar.php"; ?>	
	</div>
    <!-- Main Header-->
    <header class="main-header hidden-xs">
        <!-- Header Lower -->
			<?php include "common/header.php"; ?>	
		<!--End Main Header -->
    </header>
	
	<div class="navbar visible-xs">
		<!-- offcanvas-trigger-effects -->
		<div id="home">
			<?php include "common/mobilemenu.php"; ?>	
			<a class="mobile-logo visible-xs" href="<?php echo HTTP_SERVER.WS_ROOT ;?>"><img src="images/logo.png" height="50"></a>
		</div>
		<!-- offcanvas-trigger-effects -->
	</div>

    <!--Page Title-->
    <section class="page-title" style="background-image:url(images/gallery.jpg);">
    	<div class="auto-container">
        	<div class="sec-title">
                <h1><?php echo $pageTitle; ?></h1>
                <div class="bread-crumb"><a href="<?php echo HTTP_SERVER.WS_ROOT ;?>">Home</a> / <a href="#" class="current"><?php echo $pageTitle; ?></a></div>
            </div>
        </div>
    </section>
	<section class="gallery-section bg-light-grey outside-hidden">
                	 <?php 	
						if(isset($content_include)) {
							include $content_include; 
						} else {
							echo do_shortcode(stripslashes($pageContent));
						} 
					?>
	</section>
	<div class="clearfix"></div>
	
    <!--Main Footer-->
		<?php include "common/footercontent.php"; ?>	
		<?php include "common/footer.php"; ?>
	<!--END Footer-->
    
</div>
<!--End pagewrapper-->
<!--Scroll to top-->
<div class="scroll-to-top"><span class="icon flaticon-arrows-14"></span></div>
<script src="js/jquery.js"></script> 
<script src="js/bootstrap.min.js"></script>
<script src="js/jquery.mCustomScrollbar.concat.min.js"></script>
<script src="js/jquery.fancybox.pack.js"></script>
<script src="js/wow.js"></script>
<script src="js/mixitup.js"></script>
<script src="js/bxslider.js"></script>
<script src="js/owl.js"></script>
<script src="js/script.js"></script>
<script src="js/jquery.dlmenu.js"></script>
<script src="js/jqueryvalidation/jquery.validate.js"></script>
<script src="js/jqueryvalidation/additional-methods.js"></script>
<script src="js/subscribe.js"></script>
<script>
	$(function() {
		$( '#dl-menu' ).dlmenu();
	});
</script>
</body>
</html>
