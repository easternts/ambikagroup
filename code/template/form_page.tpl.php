<!doctype html>
<html lang="en" class="no-js">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title><?php echo $title; ?></title>
	<meta name="keywords" content="<?php echo $keyword; ?>">
	<meta name="description" content="<?php echo $description; ?>">
	<base href="<?php echo HTTP_SERVER.WS_ROOT ;?>">
	<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">	
	<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">	
	<link rel="stylesheet" type="text/css" href="css/jquery.bxslider.css">
	<link rel="stylesheet" type="text/css" href="css/owl.carousel.css">
    <link rel="stylesheet" type="text/css" href="css/owl.theme.css">
	<link rel="stylesheet" type="text/css" href="css/font-awesome.css">
    <!-- REVOLUTION BANNER CSS SETTINGS -->
	<link rel="stylesheet" type="text/css" href="css/settings.css"/>
	<link rel="stylesheet" type="text/css" href="css/style.css">
	<link rel="stylesheet" type="text/css" href="css/nivo-slider.css" rel="stylesheet">
	<script src="js/modernizr.custom.js"></script>
</head>
<body class="cont">
  <div class="se-pre-con">
    </div>
	<!-- Container -->
	<div id="container">
		<!-- Header
		    ================================================== -->
		<header class="clearfix hidden-xs">
			<nav class="navbar navbar-default navbar-fixed-top" role="navigation">
				<?php include "common/top_bar.php"; ?>	
				<?php include "common/header.php"; ?>	
			</nav>
		</header>
		<div class="navbar visible-xs">
			<!-- offcanvas-trigger-effects -->
			<div id="home">
				<?php include "common/mobilemenu.php"; ?>	
				<a class="mobile-logo visible-xs" href="<?php echo HTTP_SERVER.WS_ROOT ;?>"><img src="images/logo.png" height="50"></a>
			</div>
			<!-- offcanvas-trigger-effects -->
		</div>
		<!-- End Header -->

		<!-- page-banner-section 
			================================================== -->
		<section class="brd" style="background-image:url(images/contact-us.jpg);">	
			<div class="contaDeatils">
				<div class="container">
					<div class="row">
						<div class="col-sm-4 col-md-4">
							<div class="box">
								<div class="col-sm-2 col-md-3 col-xs-3">
									<div class="cicon">
										<i class="fa fa-map-marker"></i>
									</div>
								</div>
                                <?php
				$sql="select * from website_master";
				$query = mysql_query($sql);
				$result = mysql_fetch_assoc($query);
				?>
								<div class="col-sm-10 col-md-9 col-xs-9">
									<div class="cdeatils">
										<h3>OUR ADDRESS</h3>
										<address>										
											<?php echo $result['address']; ?>
										</address>
									</div>
								</div>
							</div>
						</div>
						<div class="col-sm-4 col-md-4">
							<div class="box">
								<div class="col-sm-2 col-md-3 col-xs-3">
									<div class="cicon">
										<i class="fa fa-phone r"></i>
									</div>
								</div>
								<div class="col-sm-10 col-md-9 col-xs-9">
									<div class="cdeatils">
										<h3>GIVE US A CALL</h3>
										<address>	
                                                          <span><a href="tel:+917096963000" class="inherit"><?php echo $result['tel1']; ?></a></span>
                                            <br><span><a href="tel:+919879618227" class="inherit"><?php echo $result['tel2']; ?></a></span><br>
            <span style="color: transparent;">abc</span>
										</address>
									</div>
								</div>
							</div>
						</div>
						<div class="col-sm-4 col-md-4">
							<div class="box">
								<div class="col-sm-2 col-md-3 col-xs-3">
									<div class="cicon">
										<i class="fa fa-clock-o"></i>
									</div>
								</div>
								<div class="col-sm-10 col-md-9 col-xs-9">
									<div class="cdeatils">
										<h3>OPENING HOURS</h3>
										<address>										
                                            <span>Mon – Sat: 9:00 AM – 8:00 PM</span><br>
										     <span>Sun: Open Full Day</span><br>
                                            <span style="color: transparent;">abc</span>
										</address>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>	
		<!-- End page-banner section -->
		<section class="contact-section">
			<div class="container">
				<div class="row">					
						 <?php 	
							if(isset($content_include)) {
								include $content_include; 
							} else {
								echo do_shortcode(stripslashes($pageContent));
							} 
						?>
				</div>
			</div>    
		</section>
		<!-- footer 
			================================================== -->
			<footer>
				<?php include "common/footercontent.php"; ?>	
				<?php include "common/footer.php"; ?>
			</footer>
		<!-- End footer -->
	</div>
	<!-- End Container -->
	<script type="text/javascript" src="js/jquery.min.js"></script>
	<script type="text/javascript" src="js/jquery.migrate.js"></script>
	<script type="text/javascript" src="js/jquery.bxslider.min.js"></script>
	<script type="text/javascript" src="js/owl.carousel.min.js"></script>
	<script type="text/javascript" src="js/bootstrap.min.js"></script>
	<script type="text/javascript" src="js/jquery.imagesloaded.min.js"></script>
	<script type="text/javascript" src="js/retina-1.1.0.min.js"></script>
	<script type="text/javascript" src="js/script.js"></script>
	<script type="text/javascript" src="js/menu.js"></script>
    <script src="js/jqueryvalidation/jquery.validate.js"></script>
<script src="js/jqueryvalidation/additional-methods.js"></script>
	<script src="js/jquery.dlmenu.js"></script>
	<script>
		$(function() {
			$( '#dl-menu' ).dlmenu();
		});
	</script>
    <script type="text/javascript">
	 
	  $("#contact-form").validate();
      
       $('#submit').click(function(e){
            e.preventDefault();
			var data =  $('#contact-form').serialize();
			
			if ($("#contact-form").valid()) {
				
				$('#submit').val('PLEASE WAIT..');
				$("#submit").prop("disabled", true);
				$.ajax({
					'url' : './sendmail.php',
					'method' : 'POST',
					'data' : data,
					'success' : function(response){
						$('#submit').val('SUBMIT');
						$("#submit").prop("disabled",false);
						$('#contact-form input[type=text]').val('');
						$('#contact-form input[type=email]').val('');
						$('#contact-form textarea').val('');
						$('#alert_div').removeClass("display-none");
						$('#alert_div').html(response);
							var $elm = $('#alert_div');
							$elm.fadeIn();
							setTimeout(function() {
								$elm.fadeOut();
							}, 2000);
					}
				 })
			}
	 });
      
	</script>
	
</body>
</html>