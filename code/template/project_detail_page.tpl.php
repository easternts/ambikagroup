<!doctype html>
<html lang="en" class="no-js">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title><?php echo $title; ?></title>
	<meta name="keywords" content="<?php echo $keyword; ?>">
	<meta name="description" content="<?php echo $description; ?>">
	<base href="<?php echo HTTP_SERVER.WS_ROOT ;?>">
	<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">	
	<link rel="stylesheet" type="text/css" href="css/jquery.bxslider.css">
	<link rel="stylesheet" type="text/css" href="css/owl.carousel.css">
    <link rel="stylesheet" type="text/css" href="css/owl.theme.css">
	<link rel="stylesheet" type="text/css" href="css/font-awesome.css">
    <!-- REVOLUTION BANNER CSS SETTINGS -->
	<link rel="stylesheet" type="text/css" href="css/settings.css"/>
	<link rel="stylesheet" type="text/css" href="css/style.css">
	<link rel="stylesheet" type="text/css" href="css/nivo-slider.css" rel="stylesheet">
	<script src="js/modernizr.custom.js"></script>
</head>
<body>
  <div class="se-pre-con">
    </div>
	<!-- Container -->
	<div id="container">
		<!-- Header
		    ================================================== -->
		<header class="clearfix hidden-xs">
			<nav class="navbar navbar-default navbar-fixed-top" role="navigation">
				<?php include "common/top_bar.php"; ?>	
				<?php include "common/header.php"; ?>	
			</nav>
		</header>
		<div class="navbar visible-xs">
			<!-- offcanvas-trigger-effects -->
			<div id="home">
				<?php include "common/mobilemenu.php"; ?>	
				<a class="mobile-logo visible-xs" href="<?php echo HTTP_SERVER.WS_ROOT ;?>"><img src="images/logo.png" height="50"></a>
			</div>
			<!-- offcanvas-trigger-effects -->
		</div>
		<!-- End Header -->
		<section id="home-section" class="slider1">
			<!--Main Slider-->
			<div class="">    	
				<?php include "common/projectslider.php"; ?>	
			</div>
		</section>
		<!-- page-banner-section 
			================================================== -->
		<!-- End page-banner section -->
		<section class="mobsinglepagesection projrctsection" style="padding-top:0px;padding-bottom:0px;">
			
						 <?php 	
							if(isset($content_include)) {
								include $content_include; 
							} else {
								echo do_shortcode(stripslashes($pageContent));
							} 
						?>
				</div>
			</div>    
		</section>

		<!-- footer 
			================================================== -->
			<footer>
				<?php include "common/footercontent.php"; ?>	
				<?php include "common/footer.php"; ?>
			</footer>
		<!-- End footer -->
	</div>
	<div class="modal fade" id="downloadForm">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header" style="background-color:#2D3563;color:#fff;">
       
        <h4 class="modal-title" id="myModalLabel">Download Request</h4>
      </div>
      <div class="modal-body">
      	<div>
			
		         <div class="modal-body">
		         <form class="downloadfrm"  id="downloadrequest" name="downloadrequest" method="post" novalidate>
	
                        	<div class="row form-group">
			<div class="col-md-6">
				<input name="dName" id="dName" type="text" placeholder="Name" class="form-control" required>
			</div>
                        </div>
                        <div class="row form-group">
			<div class="col-md-6">
				<input name="dEmail" id="dEmail" type="email" placeholder="Email"  class="form-control" required>
			</div>
        </div>
                          <div class="row form-group">
			<div class="col-md-6">
				<input name="dMobile" id="dMobile" type="text" placeholder="Mobile"  class="form-control" required>
			</div>
        </div>
		        		
					<br>
					<button class="btn btn-success" id="submitdownload" type="submit">Submit</button>
	     			<a href="#" class="btn btn-danger" data-dismiss="modal">Close</a>
				</form>
		        	</div>
			
		</div>
      </div>
      <div class="modal-footer">
       	
      </div>
    </div>
  </div>
</div>
	<!-- End Container -->
	<script type="text/javascript" src="js/jquery.min.js"></script>
	<script type="text/javascript" src="js/jquery.migrate.js"></script>
	<script type="text/javascript" src="js/jquery.bxslider.min.js"></script>
	<script type="text/javascript" src="js/owl.carousel.min.js"></script>
	<script type="text/javascript" src="js/bootstrap.min.js"></script>
	<script type="text/javascript" src="js/jquery.imagesloaded.min.js"></script>
	<script type="text/javascript" src="js/retina-1.1.0.min.js"></script>
	<script type="text/javascript" src="js/script.js"></script>
	<script type="text/javascript" src="js/menu.js"></script>
	<script src="js/jquery.nivo.slider.js"></script> 
  <script src="js/jqueryvalidation/jquery.validate.js"></script>
<script src="js/jqueryvalidation/additional-methods.js"></script>
	<script src="js/jquery.dlmenu.js"></script>
	<script>
		$(function() {
			$( '#dl-menu' ).dlmenu();
		});
	</script>
	<script>
		$('#slider').nivoSlider({
			effect: 'fade',               // Specify sets like: 'fold,fade,sliceDown'
			slices: 15,                     // For slice animations
			boxCols: 8,                     // For box animations
			boxRows: 4,                     // For box animations
			controlNav:false
		});
	</script>

<script type="text/javascript">
	 
	  $("#downloadrequest").validate();
     
       $('#submitdownload').click(function(e){
            e.preventDefault();
			if ($("#downloadrequest").valid()) {
				var name = $("#dName").val();
				var email = $("#dEmail").val();
				var mobile = $("#dMobile").val();
				var brochure_nm=$("#brochurename").val();
	
				var projectname=$("#projectname").val();
				
				$('#submitdownload').val('PLEASE WAIT..');
				$("#submitdownload").prop("disabled", true);
				var dataString = 'Name='+ name + '&Email='+ email + '&Mobile='+ mobile + '&Project='+ projectname ;
				$.ajax({
					'url' : 'process.php',
					'method' : 'POST',
					'data' : dataString,
					'success' : function(response){
						$('#submitdownload').val('SUBMIT');
						$("#submitdownload").prop("disabled",false);
						$('#downloadrequest input[type=text]').val('');
						$('#downloadrequest input[type=email]').val('');
						
						$('.modal-body').html('<h4>Thank you requesting eBrochure</h4><a href="download_file.php?download='+ brochure_nm + '" class="btn btn-success" target="_blank">Download Brochure</a>');
	                    $('.modal-footer').html('<a href="#" class="btn btn-danger" data-dismiss="modal">Close</a>');  
					}
				 })
				
			}
	 });
      
      
</script>
	
<script src="js/jquery-migrate-1.1.1.js"></script>
		<link rel="stylesheet" href="js/prettyphoto/prettyPhoto.css" type="text/css" media="screen" charset="utf-8" />
	<script src="js/prettyphoto/jquery.prettyPhoto.js" type="text/javascript" charset="utf-8"></script>
			<script type="text/javascript">
                
			 jQuery("a[rel^='prettyPhoto']").prettyPhoto({
			         overlay_gallery: false, 
                     show_title:true ,
                     social_tools: false
             });
                
           
			</script>
<script>
    $( document ).ready(function() {
       for(var i=0 ; i<=4;i++){
           if($("#" + i).length != 0) {
               if(i%2 == 0){
                   //white
                   $("#" + i).css("background-color", "#fff"); 

               }else{
                   //grey
                   $("#" + i).css("background-color", "#f7f7f7");

               }
           }else{
               break;
           }
           
       }
    });
</script>

</body>
</html>