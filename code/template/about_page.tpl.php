<!doctype html>
<html lang="en" class="no-js">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title><?php echo $title; ?></title>
	<meta name="keywords" content="<?php echo $keyword; ?>">
	<meta name="description" content="<?php echo $description; ?>">
	<base href="<?php echo HTTP_SERVER.WS_ROOT ;?>">
	<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">	
	<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">	
	<link rel="stylesheet" type="text/css" href="css/jquery.bxslider.css">
	<link rel="stylesheet" type="text/css" href="css/owl.carousel.css">
    <link rel="stylesheet" type="text/css" href="css/owl.theme.css">
	<link rel="stylesheet" type="text/css" href="css/font-awesome.css">
    <!-- REVOLUTION BANNER CSS SETTINGS -->
	<link rel="stylesheet" type="text/css" href="css/settings.css"/>
	<link rel="stylesheet" type="text/css" href="css/style.css">
	<link rel="stylesheet" type="text/css" href="css/nivo-slider.css" rel="stylesheet">
	<script src="js/modernizr.custom.js"></script>
    
</head>
<body>
  <div class="se-pre-con">
    </div>
	<!-- Container -->
	<div id="container">
		<!-- Header
		    ================================================== -->
		<header class="clearfix hidden-xs">
			<nav class="navbar navbar-default navbar-fixed-top" role="navigation">
				<?php include "common/top_bar.php"; ?>	
				<?php include "common/header.php"; ?>	
			</nav>
		</header>
		<div class="navbar visible-xs">
			<!-- offcanvas-trigger-effects -->
			<div id="home">
				<?php include "common/mobilemenu.php"; ?>	
				<a class="mobile-logo visible-xs" href="<?php echo HTTP_SERVER.WS_ROOT ;?>"><img src="images/logo.png" height="50"></a>
			</div>
			<!-- offcanvas-trigger-effects -->
		</div>
		<!-- End Header -->

		<!-- page-banner-section 
			================================================== -->
		<section class="brd" style="background-image:url(images/About_Us.jpg);">	
			<div class="container-fluid">
				<div class="row-fluid">
                    <div class="titleupper">
				<!--<h1 class="imgtitle"><?php echo $pageTitle; ?></h1>-->
                        </div>
				</div>
			</div>
		</section>
        <section class="about-section">
			<div class="container">
				<div class="row">
                    <?php echo stripslashes($page_res['page_content']); ?>
					</div>
			</div>
		</section>
		<!-- End page-banner section -->

		<!-- about section 
			================================================== -->
	
		<!-- End about section -->

		<!-- tabs-section 
			================================================== -->
		<section class="tabs-section">
			<div class="container">
				<div class="row">
					<div class="col-md-7 col-sm-6">
						<div class="about-us-box">
							<h1>about us and our priorities</h1>
							<p style="color:#fff;">We as a group aim to be expert in developing projects that are technologically complex and exhibit quality infrastructure. We are one of the highly respected reactors in the region. </p>
							<div class="row">
								<div class="col-md-6">
									<div class="about-us-post">
										<a href="#"><i class="fa fa-building-o"></i></a>
										<h2>Construction</h2>
										<span>Build Homes</span>
									</div>
									<div class="about-us-post">
										<a href="#"><i class="fa fa-cogs"></i></a>
										<h2>Maintanance</h2>
										<span>Energy Repair</span>
									</div>
									<div class="about-us-post">
										<a href="#"><i class="fa fa-desktop"></i></a>
										<h2>Good Planning</h2>
										<span>Architecture</span>
									</div>
								</div>
								<div class="col-md-6">
									<div class="about-us-post">
										<a href="#"><i class="fa fa-desktop"></i></a>
										<h2>Good Planning</h2>
										<span>Architecture</span>
									</div>
									<div class="about-us-post">
										<a href="#"><i class="fa fa-users"></i></a>
										<h2>Awesome Stuff</h2>
										<span>1000+ workers</span>
									</div>
									<div class="about-us-post">
										<a href="#"><i class="fa fa-building-o"></i></a>
										<h2>Construction</h2>
										<span>Build homes</span>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-5 col-sm-6">
						<div class="about-box mobabout">
							<h1 class="homeabout">Who we are</h1>
							<p style="text-align:justify;">The designs and architecture of our residential projects are of international standards. We are known for our contemporary designs, innovation and completion of projects within the most stringent time schedule. </p>
							<p style="text-align:justify;">For over last three decades we remain committed in delivering the highest quality living space solutions, and are creating the most efficient facilities for our clients, from the conception of a project through construction, operations and procurement.</p>
                            <p style="text-align:justify;">Our aim is to provide premium housing and commercials to our clients at par with international standards so as to provide benchmark quality standards and a luxurious lifestyle at an affordable rate.</p>
                            <img src="images/credai.jpg" alt="">
						</div>
					</div>
				</div>
			</div>
		</section>
		<!-- End tabs section -->

		<!-- team-section 
			================================================== -->
		<section class="testimonial-section">
			<div class="container">
				<div class="row">
					<div class="col-sm-2"></div>
					<div class="col-sm-8">
						<div class="aboutpart">
							<h2>Our Vision and Mission </h2>
							<p>Our aim is to provide premium housing and commercials to our clients at par with international standards so as to provide benchmark quality standards and a luxurious lifestyle at an affordable rate.</p>
						</div>
					</div>
				</div>
			</div>
		</section>
		<!-- End team section -->

		<!-- footer 
			================================================== -->
			<footer>
				<?php include "common/footercontent.php"; ?>	
				<?php include "common/footer.php"; ?>
			</footer>
		<!-- End footer -->
	</div>
	<!-- End Container -->
	<script type="text/javascript" src="js/jquery.min.js"></script>
	<script type="text/javascript" src="js/jquery.migrate.js"></script>
	<script type="text/javascript" src="js/jquery.bxslider.min.js"></script>
	<script type="text/javascript" src="js/owl.carousel.min.js"></script>
	<script type="text/javascript" src="js/bootstrap.min.js"></script>
	<script type="text/javascript" src="js/jquery.imagesloaded.min.js"></script>
	<script type="text/javascript" src="js/retina-1.1.0.min.js"></script>
	<script type="text/javascript" src="js/script.js"></script>
	<script type="text/javascript" src="js/menu.js"></script>
	<script type="text/javascript" src="js/jquery.nivo.slider.js"></script> 
	<script type="text/javascript">
		$('#slider').nivoSlider({
			effect: 'fade',               // Specify sets like: 'fold,fade,sliceDown'
			slices: 15,                     // For slice animations
			boxCols: 8,                     // For box animations
			boxRows: 4,                     // For box animations
			controlNav:false
		});
	</script>
		<script src="js/jquery.dlmenu.js"></script>
	<script>
		$(function() {
			$( '#dl-menu' ).dlmenu();
		});
	</script>
</body>
</html>