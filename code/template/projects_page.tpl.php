<!doctype html>
<html lang="en" class="no-js">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title><?php echo $title; ?></title>
	<meta name="keywords" content="<?php echo $keyword; ?>">
	<meta name="description" content="<?php echo $description; ?>">
	<base href="<?php echo HTTP_SERVER.WS_ROOT ;?>">
	<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">	
	<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">	
	<link rel="stylesheet" type="text/css" href="css/jquery.bxslider.css">
	<link rel="stylesheet" type="text/css" href="css/owl.carousel.css">
    <link rel="stylesheet" type="text/css" href="css/owl.theme.css">
	<link rel="stylesheet" type="text/css" href="css/font-awesome.css">
    <!-- REVOLUTION BANNER CSS SETTINGS -->
	<link rel="stylesheet" type="text/css" href="css/settings.css"/>
	<link rel="stylesheet" type="text/css" href="css/style.css">
	<link rel="stylesheet" type="text/css" href="css/nivo-slider.css" rel="stylesheet">
	<script src="js/modernizr.custom.js"></script>
</head>
<body>
  <div class="se-pre-con">
    </div>
	<!-- Container -->
	<div id="container">
		<!-- Header
		    ================================================== -->
		<header class="clearfix hidden-xs">
			<nav class="navbar navbar-default navbar-fixed-top" role="navigation">
				<?php include "common/top_bar.php"; ?>	
				<?php include "common/header.php"; ?>	
			</nav>
		</header>
		<div class="navbar visible-xs">
			<!-- offcanvas-trigger-effects -->
			<div id="home">
				<?php include "common/mobilemenu.php"; ?>	
				<a class="mobile-logo visible-xs" href="<?php echo HTTP_SERVER.WS_ROOT ;?>"><img src="images/logo.png" height="50"></a>
			</div>
			<!-- offcanvas-trigger-effects -->
		</div>
		<!-- End Header -->

		<!-- page-banner-section 
			================================================== -->
		<section class="brd" style="background-image:url(images/project-listing.jpg);">	
			<div class="container-fluid">
				<div class="row-fluid">
				 <div class="titleupper">
				<!--<h1 class="imgtitle"><?php echo $pageTitle; ?></h1>-->
                        </div>
				</div>
			</div>
		</section>	
		<!-- End page-banner section -->
		<section class="portfolio-section">
			<div class="container">
				<div class="row">
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
						 <?php 	
							if(isset($content_include)) {
								include $content_include; 
							} else {
								echo do_shortcode(stripslashes($pageContent));
							} 
						?>
					</div>
				</div>
			</div>    
		</section>

		<!-- footer 
			================================================== -->
			<footer>
				<?php include "common/footercontent.php"; ?>	
				<?php include "common/footer.php"; ?>
			</footer>
		<!-- End footer -->
	</div>
	<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		  <div class="modal-dialog" role="document">
			<div class="modal-content">
			  <div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel">Enquire Now</h4>
			  </div>
			  <div class="modal-body">
				<form id="contact-form">
					<div class="row">
						<div class="col-md-6">
							<input name="name" id="name" type="text" placeholder="Name">
						</div>
						<div class="col-md-6">
							<input name="mail" id="mail" type="text" placeholder="Email">
						</div>
					</div>
					<div class="row">
						<div class="col-md-6">
							<input name="tel-number" id="tel-number" type="text" placeholder="Phone">
						</div>
						<div class="col-md-6">
							<input name="tel-number" id="tel-number" type="text" placeholder="Subject">
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<select data-reqmsg="This field cannot be blank." data-frmval="Looking For" id="field_qlsd8x" name="item_meta[254]">
								<option selected="selected" value="Looking For">Looking For</option>
									<option value="Purchase">Purchase</option>
									<option value="Lease">Lease</option>
							</select>
						</div>
					</div>
					<textarea name="comment" id="comment" placeholder="Message"></textarea>
					<input type="submit" id="submit_contact" value="Send Message">
				</form>
			  </div>
			</div>
		  </div>
	</div>
	<!-- End Container -->
	<script type="text/javascript" src="js/jquery.min.js"></script>
	<script type="text/javascript" src="js/jquery.migrate.js"></script>
	<script type="text/javascript" src="js/jquery.bxslider.min.js"></script>
	<script type="text/javascript" src="js/owl.carousel.min.js"></script>
	<script type="text/javascript" src="js/bootstrap.min.js"></script>
	<script type="text/javascript" src="js/jquery.imagesloaded.min.js"></script>
	<script type="text/javascript" src="js/retina-1.1.0.min.js"></script>
	<script type="text/javascript" src="js/script.js"></script>
	<script type="text/javascript" src="js/menu.js"></script>
			<script src="js/jquery.dlmenu.js"></script>
	<script>
		$(function() {
			$( '#dl-menu' ).dlmenu();
		});
	</script>
</body>
</html>