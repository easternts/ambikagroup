<div class="col-md-8">
	<form method="post" name="career-form" id="career-form" novalidate class="submit-message"> 
		<h2>Career With Us</h2>
		<div class="row">
			<div class="col-md-6">
				<input name="name" id="name" type="text" placeholder="Name" required>
			</div>
			<div class="col-md-6">
				<input name="email" id="email" type="email" placeholder="Email" required>
			</div>			
		</div>
		<div class="row">
			<div class="col-md-6 marbot20"> 
				<input type="file" name="resume" id="resume" required>
			</div>
			<div class="col-md-6">
				<input name="mobile" id="mobile" type="text" placeholder="Phone" required>
			</div>
		</div>
		<textarea name="msg" id="msg" placeholder="Message" required></textarea>
		<input type="submit" id="career-btn" name="career-btn" value="Send Message">
		<div id="msg" class="message"></div>
	</form>
</div>
<div class="col-md-4">
		<div class="contact-info">
			<h2>Contact Info</h2>
			<p>You can contact or visit us in our office : Mon to Sat from 09:00 AM - 08:00 PM , Sun : Open Full Day</p>
			<ul class="information-list">
                        <?php
				$sql="select * from website_master";
				$query = mysql_query($sql);
				$result = mysql_fetch_assoc($query);
				?>
				<li><i class="fa fa-map-marker"></i><span><?php echo $result['address']; ?></span></li>
                <li><i class="fa fa-phone"></i><span style="margin-bottom: -9px;margin-left:0px;"><a href="tel:+917096963000" class="inherit" style="margin-left: 15px;"><?php echo $result['tel1']; ?></a></span><span style="margin-left:0px;"><a href="tel:+919879618227" class="inherit" style="margin-left: 15px;"><?php echo $result['tel2']; ?></a></span></li>
				<li><i class="fa fa-envelope-o"></i><a href="mailto:info@ambikagroupsurat.com" class="inherit" style="margin-left: 15px;">info@ambikagroupsurat.com</a></li>
			</ul>						
		</div>
</div>