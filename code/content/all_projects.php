<div class="container">
    <div class="row">
		<div class="inner-box padd-bott-20">
			<div class="sec-title padd-left-70 with-style style-left">
				<h2>Project Name</h2>
			</div>                        
		</div>
	</div>
</div>
    	<div class="auto-container">
            <!--Filter-->
            <div class="filters text-center">
            	<ul class="filter-tabs filter-btns clearfix anim-3-all">
                    <li class="filter" data-role="button" data-filter="all">All</li>
                    <li class="filter" data-role="button" data-filter=".interior">Completed</li>
                    <li class="filter" data-role="button" data-filter=".outdoor">Ongoing</li>
                </ul>
            </div>
            
        </div>
        
        <div class="auto-container images-container">
            <div class="filter-list row clearfix">
                <!--Portfolio Item-->
                <article class="portfolio-item mix mix_all outdoor architecture col-md-4 col-sm-6 col-xs-12">
                	<div class="inner-box">
                    	<figure class="image-box"><a href="images/gallery/image-1.jpg" class="lightbox-image" title="Image Caption Here"><img src="images/gallery/image-1.jpg" alt=""></a></figure>
                        <div class="lower-content">
                        	<h3><a href="#">Architecure Villa Design</a></h3>
                            <div class="desc">villa with garden design</div>
                            <a href="images/gallery/image-1.jpg" class="lightbox-image zoom-btn" title="Image Caption Here"><span class="icon flaticon-shapes-5"></span></a>
                        </div>
                    </div>
                </article>
            	
                <!--Portfolio Item-->
                <article class="portfolio-item mix mix_all interior architecture buildings col-md-4 col-sm-6 col-xs-12">
                	<div class="inner-box">
                    	<figure class="image-box"><a href="images/gallery/image-1.jpg" class="lightbox-image" title="Image Caption Here"><img src="images/gallery/image-1.jpg" alt=""></a></figure>
                        <div class="lower-content">
                        	<h3><a href="#">Interior Restroom Design</a></h3>
                            <div class="desc">villa interior design</div>
                            <a href="images/gallery/image-2.jpg" class="lightbox-image zoom-btn" title="Image Caption Here"><span class="icon flaticon-shapes-5"></span></a>
                        </div>
                    </div>
                </article>
                
                <!--Portfolio Item-->
                <article class="portfolio-item mix mix_all interior buildings col-md-4 col-sm-6 col-xs-12">
                	<div class="inner-box">
                    	<figure class="image-box"><a href="images/gallery/image-1.jpg" class="lightbox-image" title="Image Caption Here"><img src="images/gallery/image-1.jpg" alt=""></a></figure>
                        <div class="lower-content">
                        	<h3><a href="#">Modern Kitchen Interior</a></h3>
                            <div class="desc">creative kitchen assets</div>
                            <a href="images/gallery/image-3.jpg" class="lightbox-image zoom-btn" title="Image Caption Here"><span class="icon flaticon-shapes-5"></span></a>
                        </div>
                    </div>
                </article>
            </div>
        </div>
        
            