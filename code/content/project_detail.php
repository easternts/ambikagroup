<div class="container">
	<div class="row">
		<div class="col-sm-12">
			<div class="visible-xs hidden-md hidden-sm hidden-lg" style="margin-top:30px;">
				<div id="accordion" class="panel-group">
		<?php
			if($page_res['productDescr'] != ""){
				$galImg = DIR_WS_PROJECT_PATH.$page_res['productID']."/".$page_res['productThumbnail'];
				echo '<div class="panel panel-default">
						<div class="panel-heading"><h4 class="panel-title"><a href="#overview" data-parent="#accordion" data-toggle="collapse">Overview</a></h4></div>
						<div class="panel-collapse collapse in" id="overview">
							<div class="panel-body">		
								'.stripslashes($page_res['productDescr']).'<br>
								<img src="'.$galImg.'" alt="'.$page_res['productTitle'].'" class="img-responsive">
							</div>
						</div>
					</div>';
			}
			if($page_res['productAbout'] != ""){
				echo '
					<div class="panel panel-default">
						<div class="panel-heading"><h4 class="panel-title"><a href="#amenities" data-parent="#accordion" data-toggle="collapse">Amenities</a></h4></div>
						<div class="panel-collapse collapse" id="amenities">
							<div class="panel-body">		
								'.stripslashes($page_res['productAbout']).'
							</div>
						</div>
					</div>';
			}
			if($page_res['productSpecification'] != ""){
				
				echo '
					<div class="panel panel-default">
						<div class="panel-heading"><h4 class="panel-title"><a href="#mobspecification" data-parent="#accordion" data-toggle="collapse">Specification</a></h4></div>
						<div class="panel-collapse collapse" id="mobspecification">
							<div class="panel-body">		
								'.stripslashes($page_res['productSpecification']).'
							</div>
						</div>
					</div>';
			}
			
			
			if($page_res['productPlans'] == "Yes"){
				
				echo '
				<div class="panel panel-default">
						<div class="panel-heading"><h4 class="panel-title"><a href="#floorplans" data-parent="#accordion" data-toggle="collapse">Floor Plans</a></h4></div>
						<div class="panel-collapse collapse" id="floorplans">
							<div class="panel-body">
							';
			?>
				<div class="">
					<div class="row">
			<?php
				$sql ="select * from gallery where productID = '".(int)$module_id."' and status = 'E' and isFront = 'D' order by sortorder";
				$sqlquery = mysql_query($sql);         
				$i = 0;
				while($rs = mysql_fetch_array($sqlquery)) {
					$galImg = "timthumb.php?src=".DIR_WS_PROJECT_PATH.$module_id."/".$rs['galleryImage']."&w=1280&h=593&a=tl";
					$large_image = DIR_WS_PROJECT_PATH.$module_id."/".$rs['galleryImage'];	
			?>
				<div class="col-lg-3 col-md-4 col-sm-6 col-xs-12 thumb">
					<a  rel="prettyPhoto[mobilefloorplan]"  class="prettyphoto thumbnail"  href="<?php echo $large_image; ?>" title="<?php echo $rs['galleryTitle'];?>">
					<img class="img-responsive" src="<?php echo $galImg; ?>" alt="<?php echo $rs['galleryTitle'];?>">
					</a>
				</div>
			<?php
				}
			?>
					</div>
				</div>

			<?php 					
				echo '
					</div></div></div>';
			}	
			
				echo '
				<div class="panel panel-default">
						<div class="panel-heading"><h4 class="panel-title"><a href="#gallery" data-parent="#accordion" data-toggle="collapse">Gallery</a></h4></div>
						<div class="panel-collapse collapse" id="gallery">
							<div class="panel-body">
							';
			?>
				<div class="">
					<div class="row">
			<?php
				$sql ="select * from gallery where productID = '".(int)$module_id."' and status = 'E' and isFront = 'G' order by sortorder LIMIT 12";
				$sqlquery = mysql_query($sql);         
				$i = 0;
				while($rs = mysql_fetch_array($sqlquery)) {
					$galImg = "timthumb.php?src=".DIR_WS_PROJECT_PATH.$module_id."/".$rs['galleryImage']."&w=1280&h=593&a=c";
					$large_image = DIR_WS_PROJECT_PATH.$module_id."/".$rs['galleryImage'];	
			?>
				<div class="col-lg-3 col-md-4 col-sm-6 thumb">
					<a rel="prettyPhoto[mobilegallery]" class="prettyphoto thumbnail" href="<?php echo $large_image; ?>" title="<?php echo $rs['galleryTitle'];?>">
					<img class="img-responsive" src="<?php echo $galImg; ?>" alt="<?php echo $rs['galleryTitle'];?>">
					</a>
				</div>
			<?php
				}
			?>
					</div>
				</div>

			<?php 					
				echo '
					</div></div></div>';
                    ?>
                    <?php
					
    	           $sql1 ="select * from gallery where productID = '".(int)$module_id."' and status = 'E' and isFront = 'S' order by sortorder";
				   $sql2 = mysql_query($sql1);    
                    
			  if(mysql_num_rows ($sql2) > 0){
			echo '
				<div class="panel panel-default">
						<div class="panel-heading"><h4 class="panel-title"><a href="#sitegallery" data-parent="#accordion" data-toggle="collapse">Site Gallery</a></h4></div>
						<div class="panel-collapse collapse" id="sitegallery">
							<div class="panel-body">
							';
			?>
				<div class="container">
					<div class="row">
			<?php
				
			while($rs = mysql_fetch_array($sql2)) {
						$galImg = "timthumb.php?src=".DIR_WS_PROJECT_PATH.$module_id."/".$rs['galleryImage']."&w=500&h=350&a=c";
						$large_image = DIR_WS_PROJECT_PATH.$module_id."/".$rs['galleryImage'];	

			?>
				<div class="col-lg-3 col-md-4 col-sm-6 col-xs-12 thumb">
					<a rel="prettyPhoto[mobilesitegallery]" class="prettyphoto thumbnail" href="<?php echo $large_image; ?>" title="<?php echo $rs['galleryTitle'];?>">
					<img class="img-responsive" src="<?php echo $galImg; ?>" alt="<?php echo $rs['galleryTitle'];?>">
					</a>
				</div>
			<?php
				}
			?>
					</div>
				</div>

			<?php 					
				echo '
					</div></div></div>';
			}
                    
            ?>
                    
             <?php
					
    	           $campaignsql1 ="select * from gallery where productID = '".(int)$module_id."' and status = 'E' and isFront = 'C' order by sortorder";
				   $campaignqry1 = mysql_query($campaignsql1);    
                    
			  if(mysql_num_rows ($campaignqry1) > 0){
			echo '
				<div class="panel panel-default">
						<div class="panel-heading"><h4 class="panel-title"><a href="#campaigngallery" data-parent="#accordion" data-toggle="collapse">Brand Campaign</a></h4></div>
						<div class="panel-collapse collapse" id="campaigngallery">
							<div class="panel-body">
							';
			?>
				<div class="container">
					<div class="row">
			<?php
				
			while($rs = mysql_fetch_array($campaignqry1)) {
						$galImg = "timthumb.php?src=".DIR_WS_PROJECT_PATH.$module_id."/campaign/".$rs['galleryImage']."&w=500&h=350&a=c";
						$large_image = DIR_WS_PROJECT_PATH.$module_id."/campaign/".$rs['galleryImage'];	

			?>
				<div class="col-lg-3 col-md-4 col-sm-6 col-xs-12 thumb">
					<a rel="prettyPhoto[mobilecampaigngallery]" class="prettyphoto thumbnail" href="<?php echo $large_image; ?>" title="<?php echo $rs['galleryTitle'];?>">
					<img class="img-responsive" src="<?php echo $galImg; ?>" alt="<?php echo $rs['galleryTitle'];?>">
					</a>
				</div>
			<?php
				}
			?>
					</div>
				</div>

			<?php 					
				echo '
					</div></div></div>';
			}
                    
            ?>
                    
            <?php
				
			if($page_res['productLocation'] != "" || $page_res['productMap'] != ""){
				
				echo '<div class="panel panel-default">
						<div class="panel-heading"><h4 class="panel-title"><a href="#location" data-parent="#accordion" data-toggle="collapse">Location</a></h4></div>
						<div class="panel-collapse collapse" id="location">
							<div class="panel-body">';	
                
                           
                            if($page_res['productMap'] != '') {
						
								echo '<iframe src="'.$page_res['productMap'].'" width="300" height="300" frameborder="0" style="border:0" allowfullscreen></iframe>';
							
				            }else{
                           
                            echo '<a rel="location" class="prettyphoto thumbnail" href="'.DIR_WS_PROJECT_PATH.$module_id."/".$page_res['productLocation'].'">
					
                            <img class="img-responsive" src="'.DIR_WS_PROJECT_PATH.$module_id."/".$page_res[ 'productLocationThumbnail'].'" alt="">
					       </a>';
                            }
                          
                    
							    
					echo '</div>
						</div>
					</div>';
			}
			if($page_res['productBrochure'] != ""){
				echo '
				
				<div class="panel panel-default">
						<div class="panel-heading"><h4 class="panel-title"><a href="#brochure" data-parent="#accordion" data-toggle="collapse">Brochure</a></h4></div>
						<div class="panel-collapse collapse" id="brochure">
							<div class="panel-body">		
							<a href="'.DIR_WS_PROJECT_PATH.$module_id."/".$page_res['productBrochure'].'" target="_blank">Brochure </a>
							</div>
						</div>
					</div>';
			}
			if($page_res['productVideo'] != ""){
				echo '<div class="panel panel-default">
						<div class="panel-heading"><h4 class="panel-title"><a href="#productVideo" data-parent="#accordion" data-toggle="collapse">Video</a></h4></div>
						<div class="panel-collapse collapse" id="productVideo">
							<div class="panel-body">		
							'.stripslashes($page_res['productVideo']).'
							</div>
						</div>
					</div>
					';
			}
		?>
    </div>
<br>
</div>
</div>
</div>
</div>





<div class="hidden-xs">
<section class="home">

	<div class="container">
		<div class="row">
					
				<div class="col-sm-12">
					<div class="" style="overflow:hidden;">
						<div class="col-sm-7">
						<div class="project-content">
							<div class="project-tags">
								<h3 style="margin:0px;"><?php echo $page_res['productTitle']; ?></h3>
								<ul>	
								<li><?php echo $page_res['productDescr']; ?></li>
								 <?php if($page_res['productTypeTitle'] != ''){ ?>
									<li><i class="fa fa-building-o"></i> <span>Type:</span> <?php echo $page_res['productTypeTitle']; ?></li>
									 <?php } ?>
									<li><i class="fa fa-building"></i> <span>Project:</span> <?php echo $page_res['productStatus']; ?></li>
									<?php if($page_res['productBS'] != ""){	?>
									<li><i class="fa fa-line-chart"></i> <span>Booking Status:</span> <?php echo $page_res['productBS']; ?></li>
									<?php } if($page_res['productArea'] != ""){?>
									<li><i class="fa fa-map-marker"></i> <span>Area:</span> <?php echo $page_res['productArea']; ?></li>
									<?php } if($page_res['productUnits'] != ""){?>
									<li><i class="fa fa-underline"></i> <span>Units:</span> <?php echo $page_res['productUnits']; ?></li>
									<?php }?>
								</ul>
							</div>										
						</div>
						</div>
						<div class="col-sm-5">
							<?php $galImg = DIR_WS_PROJECT_PATH.$page_res['productID']."/".$page_res['productThumbnail']; ?>
							<img src="<?php echo $galImg; ?>" alt="">
						</div>
					</div>
				</div>
           	
		</div>
	</div>
    </section>

    <?php 
        $divsection = 0;
    ?>

    		 <?php 	if($page_res['productPlans'] == "Yes"){?>

	<div class="fplan thumb" id="<?php echo $divsection++ ; ?>">
		<div class="container">
           
			
			<div class="row">
				<div class="col-sm-12"><h3 class="center1">Floor Plans</h3></div>	
			</div>
		
			<div class="row">		
			
				<div class="">
					<?php
						$sql ="select * from gallery where productID = '".(int)$module_id."' and status = 'E' and isFront = 'D' order by sortorder";
						$sqlquery = mysql_query($sql);         
						$i = 0;
						while($rs = mysql_fetch_array($sqlquery)) {
							$galImg = "timthumb.php?src=".DIR_WS_PROJECT_PATH.$module_id."/".$rs['galleryImage']."&w=500&h=350&a=tl";
							$large_image = DIR_WS_PROJECT_PATH.$module_id."/".$rs['galleryImage'];	
					?>
					<div class="col-lg-3 col-md-3 col-sm-4 col-xs-12">
									<div class="thumbnail">
										<div class="blog-image">
											<a  href="<?php echo $large_image; ?>" rel="prettyPhoto[floorplan]" class="prettyphoto"><img src="<?php echo $galImg; ?>" alt="<?php echo $rs['galleryTitle'];?>" class="img-responsive"></a>
                                        
										</div>
										<div class="caption">
											<?php echo $rs['galleryTitle'];?>
										</div>
									</div>
								</div>		
					<?php
						}
					?>
				</div>
			</div>
		
			</div>
		</div>
            	<?php 					
				}
    ?>    
                 <?php
                        $locationImg = DIR_WS_PROJECT_PATH.$module_id."/".$page_res['parallaximage'];
   
                                                     ?>
	<div class="p-eff-img-s" style="background-image: url('<?php echo $locationImg; ?>');">
		<div class="container">
			<div class="row">
				<div class="col-sm-12">
<!--                    <?php echo $page_res['parallaxdesc']?>-->
<!--
					<div class="font18-s">Ambika Solitaire is an Ideal space to harmonize and revitalise minds.</div>
					<div class="maintit-s">Ambika Solitaire</div>
-->
                    <?php echo $page_res['parallaxdesc'];?>
				
				</div>
			</div>
		</div>
	</div>

   <?php if($page_res['productVideo'] != ""){?>
	<div class="video"  id="<?php echo $divsection++ ; ?>">
		<div class="container">
           
            	<div class="row">
				<div class="col-sm-12"><h3 class="center">Video</h3></div>	
			</div>
			
			<div class="row">

			
					<div class="col-md-12"><center><p><?php echo stripslashes($page_res['productVideo']); ?></p></center></div>				
			
			</div>
			
        </div>
    </div>
  <?php } ?>
    <div class="gallery" id="<?php echo $divsection++ ; ?>">
		<div class="container">
            
            <div class="row">
				<div class="col-sm-12"><h3 class="center">Gallery</h3></div>	
			</div>
         
			<div class="row">
			<div class="thumb">
				  <?php
					$sql ="select * from gallery where productID = '".(int)$module_id."' and status = 'E' and isFront = 'G' order by sortorder";
					$sqlquery = mysql_query($sql);         
					$i = 0;
					while($rs = mysql_fetch_array($sqlquery)) {
						$galImg = "timthumb.php?src=".DIR_WS_PROJECT_PATH.$module_id."/".$rs['galleryImage']."&w=500&h=350&a=c";
						$large_image = DIR_WS_PROJECT_PATH.$module_id."/".$rs['galleryImage'];	
				?>
				<div class="portfolio-item col-md-3 col-sm-3 col-xs-12">
					<div class="thumbnail">
						<a href="<?php echo $large_image; ?>" rel="prettyPhoto[gallery]" class="prettyphoto" title="<?php echo $rs['galleryTitle'];?>">
                            <img src="<?php echo $galImg; ?>" alt="">
                        </a>
					</div>				
				</div>				
				<?php
					}
				?>
			</div>
			</div>
		</div>
    </div>
	 <?php if($page_res['productLocation'] != "" || $page_res['productMap'] != ""){?>
       <div class="location" id="<?php echo $divsection++ ; ?>">
			<div class="container">
               	<div class="row">
				<div class="col-sm-12"><h3 class="center">Location</h3></div>	
			</div>
	
			<div class="row">
                
				<div class="col-md-12">
                        <?php 
                             if($page_res['productMap'] != "") {
                        ?>
                            <center>
                                <iframe src="<?php echo stripslashes($page_res['productMap']); ?>" width="600" height="400" frameborder="0" style="border:0" allowfullscreen></iframe>
                            </center>
                        <?php }else { ?>
                                                                                   
                    	<center>
                            <?php
                                 $locationImg = DIR_WS_PROJECT_PATH.$module_id."/".$page_res['productLocationThumbnail'];
   
                             ?>
				                <a href="<?php echo DIR_WS_PROJECT_PATH.$module_id."/".$page_res['productLocation']; ?>" class="prettyphoto" ><img src="<?php echo $locationImg; ?>" alt="" class="img-responsive width"></a>
                           
                        </center>
                       <?php } 
                            ?>
                 
                </div>
			</div>
        </div>
    </div>
       <?php } ?>
		<?php
					
    	           $sql1 ="select * from gallery where productID = '".(int)$module_id."' and status = 'E' and isFront = 'S' order by sortorder";
				   $sql2 = mysql_query($sql1);    
    
                   $sql3 ="select * from gallery where productID = '".(int)$module_id."' and status = 'E' and isFront = 'C' order by sortorder";
				   $qry3 = mysql_query($sql3);   
    
             			
    ?>
        
    <?php 
    if($page_res['productBrochure'] != "" || mysql_num_rows ($qry3) > 0  || mysql_num_rows ($sql2) > 0){
   
    ?>
    
     

	<div class="getdetails" id="<?php echo $divsection++ ; ?>">
       
		<div class="container">
            <div class="row">
				<h3 class="center">Get more Information</h3>
			</div>
			<div class="row">
			<div class="clearfix"></div>
				<div class="services-wrapper services-thumbnail">
					<div class="col-md-1"></div>
                    <?php
                    if($page_res['productBrochure'] != ""){
                        ?>
					<div class="col-md-3 col-sm-4 rt">
						<div class="services-box">
							<div class="thumbnail-wrapper">
								<div style="background-image:url(images/bhrochure.jpg)" class="services-img"></div>
                                 
								<div class="services-content">
                                   
									<div class="sc-wraper" id="Brochure">
                                        <input type="hidden" name="brochurename" id="brochurename" value = "<?php echo $module_id."/".$page_res['productBrochure']; ?>">
			<input type="hidden" name="projectname" id="projectname" value = "<?php echo $page_res['productTitle']; ?>">
                                         <?php $brochure = DIR_WS_PROJECT_PATH.$module_id."/".$page_res['productBrochure'] ?>
										<h3><a href="<?php echo DIR_WS_PROJECT_PATH.$module_id."/".$page_res['productBrochure']?>" target="_blank">Download Brochure</a></h3></div>
								</div>
                              
							</div>
						</div>
					</div>
                  
                    <?php } ?>
                    <?php
              
                    if(mysql_num_rows ($qry3) > 0){
                    
                        ?>
					<div class="col-md-3 col-sm-4">
						<div class="services-box">
							<div class="thumbnail-wrapper">
								<div style="background-image:url(images/viewbrand.jpg)" class="services-img"></div>
								<div class="services-content">
									<div class="sc-wraper">
                                        <?php $campaignurl =  HTTP_SERVER.WS_ROOT.'campaign-gallery/'.get_projectseo_url($module_id,pro_SeoSlug($page_res['productTitle'])); ?>
										<h3><a target="_blank" href="<?php echo $campaignurl; ?>">View Brand Campaign</a></h3></div>
								</div>
							</div>
						</div>
					</div>
                 <?php } 
        ?>
                    <?php
        if(mysql_num_rows ($sql2) > 0){
                        ?>
					<div class="col-md-3 col-sm-4">
						<div class="services-box">
							<div class="thumbnail-wrapper">
								<div style="background-image:url(images/sitegallery.jpg)" class="services-img"></div>
								<div class="services-content">

                                    <div class="sc-wraper">
                                        
                                        <?php $url =  HTTP_SERVER.WS_ROOT.'site-gallery/'.get_projectseo_url($module_id,pro_SeoSlug($page_res['productTitle'])); ?>
                                        <h3><a target="_blank" href="<?php echo $url; ?>">Site Pictures</a></h3>
                                    </div>
								</div>
							</div>
						</div>
					</div>
               <?php } ?>
				</div>
			</div>
		</div>
	</div>
        
<?php } ?>

</div>