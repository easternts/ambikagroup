<script>
			
			
$(document).ready(function(){
	
	
	flag = 0;
	          $('input[type=submit]').on('click', function() {       
				if ($("#testiFrm").valid()) {
					$('input[type=submit]').prop('disabled', false);  
				} else {
					$('input[type=submit]').prop('disabled', 'disabled');
				}
			  });
			  
			  $('input[type=file]').bind('change', function() {

				  //this.files[0].size gets the size of your file.
				  var iSize = ($("input[type=file]")[0].files[0].size / 1024); 
					iSize = (Math.round(iSize * 100) / 100)
				var max = '<?php echo (int)ini_get('upload_max_filesize'); ?>' * 1024;	
				  if(iSize > max)
				  {
					  $('input[type=submit]').prop('disabled', 'disabled');
					  flag = 1;
					  alert('Maximum file upload size is : '+'<?php echo (int)ini_get('upload_max_filesize'); ?>'+' MB');
				  }
					  
				  else
				  {
					 $('input[type=submit]').prop('disabled', false); 
					 flag = 0;
				  }
					 

				});

});
</script>
<?php
include WS_PFBC_ROOT."Form.php";
class testimonial
{
	function addForm()
	{
		$form = new Form("addFrm");
		$form->configure(array(
			"prevent" => array("bootstrap","jQuery"),
			"view" => new View_SideBySide,
			"id" => "testiFrm"
		));
		$status = array("E" => "Enabled", "D" => "Disabled");
		//$designation = getdesignationlist('T');
		$form->addElement(new Element_HTML("<legend>New Testimonial</legend>"));
		$form->addElement(new Element_Hidden("controller", "testimonial"));
		$form->addElement(new Element_Hidden("action", "testimonial"));
		$form->addElement(new Element_Hidden("subaction", "add"));
		
		/* Actual Form Fields Started */
		$form->addElement(new Element_HTML('<div id="dtBox"></div>'));	
		$form->addElement(new Element_Textbox("Name:", "name", array(
			"required" => 1, 
			"placeholder" => "Name"
		)));
			
		
		
		
        $form->addElement(new Element_TinyMCE("Review:", "review", array(
			"required" => 1, 
			"placeholder" => "Review"
			)));
		$form->addElement(new Element_HTML('<label class="control-label">Last Sort Order : '.get_last_sortorder('testimonial').'</label><br><br>'));			
		$form->addElement(new Element_Textbox("Sort Order:", "sortorder", array(
			"required" => 1, 
			"placeholder" => "SortOrder"
			)));
		$form->addElement(new Element_Select("Status:", "status", $status, array(
			"required" => 1, 
			"placeholder" => "Status"
			)));		
		$form->addElement(new Element_HTML('<hr>'));
		$form->addElement(new Element_Button);
		$form->addElement(new Element_Button("Cancel", "button", array(
			"onclick" => "history.go(-1);"
		)));
		$form->render();
	}
	function add()
	{
		
		$username=$_SESSION['username'];
		$sql = "Insert into testimonial set 
		name = '".mysql_real_escape_string($_POST['name'])."',
		review = '".mysql_real_escape_string($_POST['review'])."',
		username = '".$username."',
		createdate = now(),
		sortorder='".$_POST['sortorder']."',
		status = '".$_POST['status']."',
		remote_ip ='".$_SERVER['REMOTE_ADDR']."'
		";
		$qry = mysql_query($sql) or die(mysql_error().$sql);
		$id = mysql_insert_id();
		
		if($_FILES['image']['name'] != "")
		{
			
			$path = DIR_FS_TESTIMONIAL_PATH;
			
			if(!file_exists($path))
			{
				mkdir($path);
				exec("chown ".FILEUSER.FILEUSER." ".$path);
				exec("chmod 0777 ".$path);
				
			}
			$photo = $id . '-' .$_FILES['image']['name'];
			if(move_uploaded_file($_FILES['image']['tmp_name'],$path.$photo))
			{
				$qry = "update testimonial set photo = '".$photo."' where testimonial_Id = '".$id."'";
				$res = mysql_query($qry) or die(mysql_error());
			}
		}
		
		return true;
	}
	function editForm()
	{		
		$sql = "select * from testimonial where testimonial_Id ='".$_REQUEST['testimonial_Id']."'";
		$qry = mysql_query($sql) or die(mysql_error().$sql);
		
		if(mysql_num_rows($qry) > 0){
			$rs=mysql_fetch_array($qry);
			$form = new Form("html5");
			$form->configure(array(
			"prevent" => array("bootstrap","jQuery"),
			"view" => new View_SideBySide,
			"id" => "testiFrm"
			));
			$status = array("E" => "Enabled", "D" => "Disabled");
			
			$form->addElement(new Element_HTML("<legend>Edit Testimonial</legend>"));
			$form->addElement(new Element_Hidden("controller", "testimonial"));
			$form->addElement(new Element_Hidden("action", "testimonial"));
			$form->addElement(new Element_Hidden("subaction", "edit"));
			$form->addElement(new Element_Hidden("testimonial_Id", $_REQUEST['testimonial_Id']));
			$form->addElement(new Element_Hidden("prevImage", $rs['photo']));
			/* Actual Form Fields Started */
			$form->addElement(new Element_HTML('<div id="dtBox"></div>'));	
			$form->addElement(new Element_Textbox("Name:", "name", array(
			"value"=> $rs['name'],
			"required" => 1, 
			"placeholder" => "Name"
			)));
				
			
			
			$form->addElement(new Element_TinyMCE("Review:", "review", array(
				"value"=> $rs['review'],
				"required" => 1, 
				"placeholder" => "Review"
				)));
			
			$form->addElement(new Element_HTML('<label class="control-label">Last Sort Order : '.get_last_sortorder('testimonial').'</label><br><br>'));			
			$form->addElement(new Element_Textbox("Sort Order:", "sortorder", array(
			"value"=> $rs['sortorder'],
			"required" => 1, 
			"placeholder" => "Sort Order"
			)));		
			$form->addElement(new Element_Select("Status:", "status", $status, array(
			"value"=> $rs['status'],
			"required" => 1, 
			"placeholder" => "Status"
			)));
			$form->addElement(new Element_HTML('<hr>'));
			$form->addElement(new Element_Button);
			$form->addElement(new Element_Button("Cancel", "button", array(
				"onclick" => "history.go(-1);"
			)));
			$form->render();
		}
		else
		{
			echo "No Tax Found...";
		}
		
	}
	function edit()
	{
		echo $_FILES['image']['name'];
		if($_FILES['image']['name'] != "")
		{
			$path = DIR_FS_TESTIMONIAL_PATH;
			if(!file_exists($path))
			{
				mkdir($path);
				exec("chown ".FILEUSER.FILEUSER." ".$path);
				exec("chmod 0777 ".$path);
				
			}
			
			@unlink($path.$_POST['prevImage']);
			$id = $_POST['testimonial_Id'];
			$photo = $id . '-' .$_FILES['image']['name'];
			if(move_uploaded_file($_FILES['image']['tmp_name'],$path.$photo))
			{
				$qry = "update testimonial set photo = '".$photo."' where testimonial_Id = '".$id."'";
				$res = mysql_query($qry) or die(mysql_error());
				
			}
			
		}
		
		$username = $_SESSION['username'];
		$updqry = mysql_query("update testimonial set 
		name = '".mysql_real_escape_string($_POST['name'])."',
        review = '".mysql_real_escape_string($_POST['review'])."',
		username = '".$username."',	
		sortorder='".$_POST['sortorder']."',
		status = '".$_POST['status']."',
		modifieddate = now(),
		remote_ip ='".$_SERVER['REMOTE_ADDR']."'
		where testimonial_Id='" .$_POST['testimonial_Id']."'") or die ("Updating Testimonial records query failed: ".mysql_error());
			
		return true;
	}
	function listData()
	{
?>
<script>
$(document).ready(function() {
	var listURL = 'helperfunc/testimonialList.php';
	$('#testimoniallist').dataTable({
		"ajax": listURL
	});
});
</script>	
<?php
		$subvar = 'onclick="return confirmSubmit();"';	
		echo '<div id="no-more-tables"><table cellpadding="1" cellspacing="2" border="0" class="table table-striped table-bordered dataTable" id="testimoniallist" width="100%">
		<thead>
		<tr>
			<th align="left">ID</th>
			<th align="left">Name</th>
			<th align="left">Review</th>
			<th align="left">Status</th>
			<th align="left">Sortorder</th>
			<th>Action</th>
		</tr>
		</thead>
		<tbody></tbody>	
		<tfoot>
				<tr>
					<th align="left">ID</th>
					<th align="left">Name</th>
					<th align="left">Review</th>
					<th align="left">Status</th>
					<th align="left">Sortorder</th>
					<th>Action</th>
				</tr>
		</tfoot>
		 </table></div>';		
	?>
<script>
	$('.table').editable({
		selector: 'a.estatus,a.esortorder',
		params:{"tblName":"testimonial"},
		value: 1,
		source: [{value: 'E', text: 'Active'},{value: 'D', text: 'Disabled'}]
	});
</script>
<?php		
	}
	
	function delete()
	{
		$photo = getfldValue('testimonial','testimonial_Id',$_GET['testimonial_Id'],'photo');
		@unlink(DIR_FS_TESTIMONIAL_PATH.$photo);
		
		$delsql = "Delete from testimonial where testimonial_Id='".$_GET['testimonial_Id']."'";
		$delqry = mysql_query($delsql) or die(mysql_error().$delsql);
		return true;		
	}
	
	
	}
?>
