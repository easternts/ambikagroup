<script> 
$(document).ready(function(){

    $('#mainpages').select2({
		placeholder: "Select a Main Page"
	});

 
$("#selectall").click(function(){
    $('.case').not(this).prop('checked', this.checked);
}); 

 $(document).on("click",".case" ,function(){
        if($(".case").length == $(".case:checked").length) {
            $("#selectall").prop("checked", "checked");
        } else {
            $("#selectall").removeAttr("checked");
        }
 
    }); 

$("#deleteMultiple").click(function(){
	
	  var result = confirm("Are You Sure you want to delete the feedback ? ");
		if (result == true) 
		{	
			var delIDarray = [];
			  $('.case').each(function(){
					var ar = this.id;
					var selectval = $('#'+ar).is(':checked');
					if(selectval){
						delIDarray.push(ar);
					}
			  });
			var finalDelArray = delIDarray.join(',');
			var loc = 'index.php?controller=feedback&action=feedback&subaction=delete&fid='+finalDelArray;
			window.location.assign(loc);
		}
		else 
		{
			
		}	  
	  //location.href="index.php?controller=products&action=subscriptionlist&subaction=delete&sid="+finalDelArray;
});     
 
});
</script>
<?php
include WS_PFBC_ROOT."Form.php";
class feedback
{

	function listData(){
	$table = 'feedback';
	update_data($table);
	?>
	
	<?php 
		$form = new Form("addFrm");
		$form->configure(array(
			"prevent" => array("bootstrap","jQuery"),
			"view" => new View_Inline
		));
		$form->addElement(new Element_HTML("<legend>Search Entries</legend>"));
		$form->addElement(new Element_Hidden("controller", "feedback"));
		$form->addElement(new Element_Hidden("action", "feedback"));
		$form->addElement(new Element_Hidden("subaction", "listData"));
        
		/* Actual Form Fields Started */
		$form->addElement(new Element_jQueryUIDate("Date:", "fdate", array(
                    "placeholder" => "Date",
                    "required" => 1
                )));
					
		$form->addElement(new Element_Button);
		/* $form->addElement(new Element_HTML('<br><br>&nbsp;&nbsp;<a href="feedback_report.php">Export To Excel</a>'));*/
		$form->addElement(new Element_HTML('<hr>'));
		$form->render();		
		
		$whr = "";
		$disQry =  "SELECT  * from feedback where 1=1 ";
		
		//if(isset($_POST['posttype'])) $whr .= " and post = '".$_POST['posttype']."'";
		if(isset($_REQUEST['fdate']) && $_REQUEST['fdate'] != "" ) {
			$edate = date('Y-m-d',strtotime($_REQUEST['fdate']));
			$whr .= " and fdate >= '".$edate."'";
		}
			
		
		
		
		$disQry .= $whr;
		$disQry .= ' order by fdate desc';
	
		if(isset($_SESSION['listSQL'])){ unset($_SESSION['listSQL']); }
		$_SESSION['listSQL'] = $disQry;
		
		
?>
	<script>
	$(document).ready(function() {
		var listURL = "helperfunc/feedback.php";
		var oTable = $('#inquirylist').dataTable({
			"ajax": listURL,
			"order": [[ 0, "desc" ]]
		});
		
		$(window).bind('resize', function () {  oTable.fnAdjustColumnSizing();  });
		
	});
	</script>
<?php
	$subvar = 'onclick="return confirmSubmit();"';
	echo '<div id="no-more-tables">
	<table class="table table-striped table-bordered dataTable" id="inquirylist" style="width:100%;">

		

		<thead>
		<tr>
			<th colspan="8"><input type="checkbox" id="selectall" value="false">&nbsp; &nbsp;Select All</input>&nbsp; &nbsp;
			<button class="btn btn-danger" id="deleteMultiple">Delete Selected</th>	
		</tr>
		<tr>
			<th><center>Select</center></th>
			<th>Name</th>
			<th>Phone</th>
			<th>Message</th>
			<th>Date</th>
			<th>Action</th>
		</tr>
		</thead>
		<tbody></tbody>	
		<tfoot>	
		<tr>
			<th>Select</th>
			<th>Name</th>
			<th>Phone</th>
			<th>Message</th>
			<th>Date</th>
			<th>Action</th>
		</tr>
		</tfoot>
	</table></div>';	
?>
<script>
	$('.table').editable({
		selector: 'a.estatus,a.esortorder',
		params:{"tblName":"feedback"},
		value: 1,
		source: [{value: 'E', text: 'Active'},{value: 'D', text: 'Disabled'}]
	});
</script>
<?php
		 
	}
	function delete()
	{		
		
		$values = explode(",", $_GET["fid"]);
		if(count($values) > 1){
			foreach($values as $key => $val){
				$delsql = "Delete from feedback where feedback_id = '".$val."'";
				$delqry = mysql_query($delsql) or die(mysql_error().$delsql);
			}
		}else{
			$delsql = "Delete from feedback where feedback_id = '".$_GET['fid']."'";
			$delqry = mysql_query($delsql) or die(mysql_error().$delsql);
		}
		return true;		
	}
	
}
?>
